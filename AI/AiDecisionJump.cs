﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiDecisionJump : AIDecision
{
    protected CharacterLunge characterLunge;
    private GameObject playerObject;
    public override bool Decide()
    {
        Vector2 playerDirection = transform.InverseTransformPoint(playerObject.transform.position).normalized;
        characterLunge.setTargetVector(playerDirection);
        characterLunge.StartDash();
        return true;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        playerObject = GameObject.FindGameObjectWithTag("Player");
        characterLunge = GetComponent<CharacterLunge>();
    }
}