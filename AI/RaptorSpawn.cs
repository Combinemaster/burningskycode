﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaptorSpawn : MonoBehaviour
{
	public float spawnTime = 1.1f;
	public float intensityDecayDuration = 3f;
	public float startingIntensity = 20f;

	protected Character character;
	protected Health healthRef;
	protected Animator animator;
	protected int spawningAnimationParameter;

	private static string MATERIAL_INTENSITY_PARAM = "_Intensity";
	private static string SLOW_DOWN_EFFECT_ENABLED_PARAM = "_SlowDownEffectEnabled";
	private MeshRenderer modelRenderer;

	// Start is called before the first frame update
	void Start()
    {
		character = GetComponent<Character>();
		healthRef = GetComponent<Health>();
		animator = character._animator;

		MaterialPropertyBlock mpb = new MaterialPropertyBlock();
		mpb.SetFloat(MATERIAL_INTENSITY_PARAM, startingIntensity);

		modelRenderer = character.CharacterModel.gameObject.GetComponent<MeshRenderer>();
		modelRenderer.SetPropertyBlock(mpb);

		RegisterAnimatorParameter("Spawning", AnimatorControllerParameterType.Bool, out spawningAnimationParameter);

		MMAnimatorExtensions.UpdateAnimatorBool(animator, "Spawning", true);

		character.ConditionState.ChangeState(CharacterStates.CharacterConditions.Spawning);

		healthRef.Invulnerable = true;
	}

    // Update is called once per frame
    void Update()
    {
		if (spawnTime < 0) {
			MMAnimatorExtensions.UpdateAnimatorBool(animator, "Spawning", false);
			character.ConditionState.ChangeState(CharacterStates.CharacterConditions.Normal);
			character.MovementState.ChangeState(CharacterStates.MovementStates.Idle);
			healthRef.Invulnerable = false;

			MaterialPropertyBlock mpb = new MaterialPropertyBlock();
			modelRenderer.GetPropertyBlock(mpb);

			mpb.SetFloat(MATERIAL_INTENSITY_PARAM, 0);
			mpb.SetFloat(SLOW_DOWN_EFFECT_ENABLED_PARAM, 1);

			modelRenderer.SetPropertyBlock(mpb);

			Destroy(this);
		} else {
			spawnTime -= Time.deltaTime;

			MaterialPropertyBlock retreivedPropertyBlock = new MaterialPropertyBlock();
			modelRenderer.GetPropertyBlock(retreivedPropertyBlock);

			if (retreivedPropertyBlock.GetFloat(MATERIAL_INTENSITY_PARAM) > 0) {
				float resultingIntensity = retreivedPropertyBlock.GetFloat(MATERIAL_INTENSITY_PARAM) - intensityDecayDuration;

				MaterialPropertyBlock mpb = new MaterialPropertyBlock();
				mpb.SetFloat(MATERIAL_INTENSITY_PARAM, resultingIntensity);

				modelRenderer.SetPropertyBlock(mpb);
			}
		}
	}

	protected virtual void RegisterAnimatorParameter(string parameterName, AnimatorControllerParameterType parameterType, out int parameter) {
		parameter = Animator.StringToHash(parameterName);

		if (animator == null) {
			return;
		}
		if (animator.MMHasParameterOfType(parameterName, parameterType)) {
			character._animatorParameters.Add(parameter);
		}
	}
}
