﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiDecisionFindPlayer : AIDecision {
	/// The offset to apply to the shoot origin point (by default the position of the object)
	public Vector2 raycastOriginOffset = new Vector2(0, 0);
	/// The layers the agent will be blocked by
	public LayerMask blockersLayerMask;
	/// The layers the agent will be looking for
	public LayerMask targetLayerMask;
	/// Max distance for searching for the player, keep this low to encourge more patrolling!
	public float maxSightRange;

	private GameObject player;
	protected CharacterHandleWeapon characterShoot;

	// Start is called before the first frame update
	protected override void Start() {
		base.Start();
		player = GameObject.FindGameObjectWithTag("Player");
		characterShoot = GetComponent<CharacterHandleWeapon>();
	}

	public override bool Decide() {
		return findPlayer();
	}

	public bool findPlayer() {
		RaycastHit2D blockingRaycast;
		RaycastHit2D targetRaycast;
		Vector2 _rayCastOrigin = new Vector2(transform.position.x + raycastOriginOffset.x, transform.position.y + raycastOriginOffset.y);
		Vector2 _playerOrigin = transform.InverseTransformPoint(player.transform.position).normalized;
		float distance = Vector2.Distance(player.transform.position, transform.position);

		if (distance > maxSightRange) {
			distance = maxSightRange;
		}

		// we cast a ray in front of the agent to check for a Player
		blockingRaycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, distance, blockersLayerMask, Color.yellow, true);
		targetRaycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, distance, targetLayerMask, Color.yellow, true);

		if (!blockingRaycast && targetRaycast) {
			_brain.Target = player.transform;
			MMGameEvent.Trigger(DynamicCombatMusic.eventPlayerDetectedId);
			return true;
		}
		else {
			characterShoot.ShootStop();
			return false;
		}
	}
}
