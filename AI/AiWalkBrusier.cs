﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiWalkBrusier : AIWalk {
	//Keep track of player location to "hone in" on their location
	private GameObject playerObject;

	//Boolean value to stop following the player if a obstacle is presented.
	[Information("Boolean value that controls if the brusier should stop following the player if they can't see them. This is mostly used for static enemies where there is a clear open arena in place. Defaulted to false to perserve the 'personality' of the brusiers aggressive" +
		" nature.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	public bool stopFollowing;

	//Reference to the lunge script
	protected CharacterLunge characterLunge;

	//Reference to the Brusier Jump script
	protected BrusierJump brusierJump;

	protected override void Start() {
		playerObject = GameObject.FindGameObjectWithTag("Player");
		characterLunge = GetComponent<CharacterLunge>();
		brusierJump = GetComponent<BrusierJump>();
		base.Start();
	}

	protected override void CheckForTarget() {
		if (WalkBehaviour != WalkBehaviours.MoveOnSight) {
			return;
		}

		// we set up the origin (being the brusier) and the endpoint, being the player
		Vector2 raycastOrigin = transform.position + MoveOnSightRayOffset;
		Vector2 playerDirection = transform.InverseTransformPoint(playerObject.transform.position).normalized;
		RaycastHit2D raycastObstacle = MMDebug.RayCast(raycastOrigin, playerDirection, 1, MoveOnSightObstaclesLayer, Color.green, true);

		if (stopFollowing) {
			RaycastHit2D raycastObstacleStopMoving = MMDebug.RayCast(raycastOrigin, playerDirection, Vector2.Distance(playerObject.transform.position, transform.position), MoveOnSightObstaclesLayer, Color.red, true);
			if (raycastObstacleStopMoving) {
				_direction = Vector2.zero;
				return;
			}
		}

		if (playerDirection.x > 0) {
			_direction = Vector2.right;
		}
		else {
			_direction = Vector2.left;
		}

		Renderer playerRenderer = playerObject.GetComponent<Renderer>();
		if (playerRenderer == null) {
			playerRenderer = playerObject.GetComponentInChildren<MeshRenderer>();
		}

		//normalizes the position of the brusier and player to be on the lowest point of their model
		double playerYPostionNormalized = System.Math.Round(playerObject.transform.position.y, 1);
		double brusierYPostionNormalized = System.Math.Round(transform.position.y, 1);

		//then it compares if the brusiers position is higher or lower than the players. If it is, we know the user is below the brusier on the Y axis.
		bool targetBelow = brusierYPostionNormalized > playerYPostionNormalized;

		_distanceToTarget = Vector2.Distance(transform.position, playerObject.transform.position);

		// if the ray hit the player and they are within lunge distance, then execute the lunge
		if (!targetBelow &&
			_controller.State.IsGrounded &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Falling &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Dashing) {

			if (!raycastObstacle) {
				characterLunge.setTargetVector(playerDirection);
				characterLunge.StartDash();
			}
		} 

		if (targetBelow && _controller.State.IsGrounded &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Falling &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Dashing) {
			brusierJump.jumpDownFromOneWayPlatform();
		}

		// if the ray has hit nothing, or if we've reached our target, we prevent our character from moving further.
		if (_distanceToTarget <= StopDistance) {
			_direction = Vector2.zero;
		}
		else {
			// if we've hit something, we make sure there's no obstacle between us and our target
			if (raycastObstacle && _distanceToTarget > raycastObstacle.distance) {
				_direction = Vector2.zero;
			}
		}
	}
}
