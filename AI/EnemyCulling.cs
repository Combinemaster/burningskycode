﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCulling : MonoBehaviour
{	
    private CorgiController corgiController;

    // Start is called before the first frame update
    void Start()
    {
        corgiController = gameObject.GetComponentInParent<CorgiController>();
    }

    private void OnBecameInvisible()
    {
        corgiController.enabled = false;
    }

    private void OnBecameVisible()
    {
        corgiController.enabled = true;
    }
}
