﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPetro : MonoBehaviour, ResetGameObject {

	private Health healthScriptRef;
	public bool respawned = true;

	bool ResetGameObject.respawned {
		get => respawned; set {
			respawned = value;
		}
	}

	private void Start() {
		healthScriptRef = gameObject.GetComponent<Health>();
	}

	public void resetGameObject() {
		healthScriptRef.ResetHealthToMaxHealth();
		healthScriptRef.Revive();
		gameObject.GetComponent<CorgiController>().GravityActive(false);
	}
}
