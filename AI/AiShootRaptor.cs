﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiShootRaptor : AIShootOnSight
{
	/// if true, the Character will face the target (left/right) when shooting
	public bool faceTarget = true;

	/// The layers the agent will be blocked by
	public LayerMask blockersLayerMask;

	///Y offset for the obstacle raycast
	public float obstacleRaycastYOffset;

	private GameObject player;
	protected Animator animator;

	protected int firingAnimationParameter;
	protected int playerPositionXAnimationParameter;
	protected int playerPositionYAnimationParameter;

	protected override void Start()
    {
        base.Start();
		animator = _character._animator;
		player = GameObject.FindGameObjectWithTag("Player");

		RegisterAnimatorParameter("Firing", AnimatorControllerParameterType.Bool, out firingAnimationParameter);
		RegisterAnimatorParameter("playerPositionX", AnimatorControllerParameterType.Float, out playerPositionXAnimationParameter);
		RegisterAnimatorParameter("playerPositionY", AnimatorControllerParameterType.Float, out playerPositionYAnimationParameter);
	}

	protected virtual void RegisterAnimatorParameter(string parameterName, AnimatorControllerParameterType parameterType, out int parameter) {
		parameter = Animator.StringToHash(parameterName);

		if (animator == null) {
			return;
		}
		if (animator.MMHasParameterOfType(parameterName, parameterType)) {
			_character._animatorParameters.Add(parameter);
		}
	}

	protected override void Update()
	{
		if ((_character == null) || (_characterShoot == null)) { return; }

		if ((_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
					|| (_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Frozen) ||
					(_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Spawning))
		{
			_characterShoot.ShootStop();
			return;
		}

		RaycastHit2D _raycast;
		Vector2 _rayCastOrigin = new Vector2(transform.position.x, transform.position.y + obstacleRaycastYOffset);
		Vector2 _playerOrigin = transform.InverseTransformPoint(player.transform.position).normalized;

		float currentDistance = Vector2.Distance(player.transform.position, transform.position);
		if (currentDistance > ShootDistance) {
			currentDistance = ShootDistance;
		}

		_raycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, currentDistance, blockersLayerMask, Color.yellow, true);

		if (!_raycast && (Vector2.Distance(player.transform.position, transform.position) <= ShootDistance)) {
			facePlayer();

			if (animator != null) {
				MMAnimatorExtensions.UpdateAnimatorBool(animator, "Firing", true);
				MMAnimatorExtensions.UpdateAnimatorFloat(animator, "playerPositionX", _playerOrigin.x);
				MMAnimatorExtensions.UpdateAnimatorFloat(animator, "playerPositionY", _playerOrigin.y);
			}

			_characterShoot.CurrentWeapon.GetComponent<WeaponAim>().SetCurrentAim(_playerOrigin);
			_characterShoot.ShootStart();
			MMGameEvent.Trigger(DynamicCombatMusic.eventPlayerDetectedId);
		} else {
			if (animator != null) {
				MMAnimatorExtensions.UpdateAnimatorBool(animator, "Firing", false);
			}

			_characterShoot.ShootStop();
		}
	}

	protected virtual void facePlayer() {
		if (!faceTarget) {
			return;
		}

		if (transform.position.x > player.transform.position.x) {
			_character.Face(Character.FacingDirections.Left);
		}
		else {
			_character.Face(Character.FacingDirections.Right);
		}
	}

	private void OnBecameInvisible() {
		this.enabled = false;
	}

	private void OnBecameVisible() {
		this.enabled = true;
	}
}
