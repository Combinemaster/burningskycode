﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrusierJump : CharacterJump {
	public void jumpDownFromOneWayPlatform() {
		_movement.ChangeState(CharacterStates.MovementStates.Jumping);
		JumpDownFromOneWayPlatform();
	}
}
