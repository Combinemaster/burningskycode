﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BurningSky;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;

public class HandleEnemyDeath : MonoBehaviour
{
	public List<GameObject> activateObjects;

	private void Start() {
		var health = gameObject.GetComponent<Health>();
		health.OnDeath += enableObjects;
	}

	private void enableObjects() {
		foreach (GameObject activatedObject in activateObjects) {
			activatedObject.SetActive(true);
		}
	}

	private void OnDisable()
    {
		MMEventManager.TriggerEvent(new EnemyEvent(EnemyEventType.EnemyDied, gameObject.GetInstanceID()));
    }
}
