﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiActionRaptorShoot : AIAction {
	/// if true, the Character will face the target (left/right) when shooting
	public bool FaceTarget = true;
	/// The offset to apply to the shoot origin point (by default the position of the object)
	public Vector2 RaycastOriginOffset = new Vector2(0, 0);
	/// The layers the agent will be blocked by
	public LayerMask blockersLayerMask;
	/// The maximum distance at which the AI can shoot at the player
	public float ShootDistance = 10f;

	private GameObject player;
	protected Character character;
	protected CharacterHandleWeapon characterShoot;
	protected Animator animator;

	protected int firingAnimationParameter;
	protected int playerPositionXAnimationParameter;
	protected int playerPositionYAnimationParameter;

	public override void PerformAction() {
		shootAtPlayer();
	}

	public override void OnExitState() {
		if (animator != null) {
			MMAnimatorExtensions.UpdateAnimatorBool(animator, "Firing", false);
		}
		base.OnExitState();
	}

	protected override void Start() {
		base.Start();
		player = GameObject.FindGameObjectWithTag("Player");
		character = GetComponent<Character>();
		animator = character._animator;
		characterShoot = GetComponent<CharacterHandleWeapon>();

		RegisterAnimatorParameter("Firing", AnimatorControllerParameterType.Bool, out firingAnimationParameter);
		RegisterAnimatorParameter("playerPositionX", AnimatorControllerParameterType.Float, out playerPositionXAnimationParameter);
		RegisterAnimatorParameter("playerPositionY", AnimatorControllerParameterType.Float, out playerPositionYAnimationParameter);
	}

	protected virtual void RegisterAnimatorParameter(string parameterName, AnimatorControllerParameterType parameterType, out int parameter) {
		parameter = Animator.StringToHash(parameterName);

		if (animator == null) {
			return;
		}
		if (animator.MMHasParameterOfType(parameterName, parameterType)) {
			character._animatorParameters.Add(parameter);
		}
	}

	protected void shootAtPlayer() {
		if ((character == null) || (characterShoot == null)) { return; }

		if ((character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
					|| (character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Frozen) ||
					(character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Spawning)) {
			characterShoot.ShootStop();
			return;
		}

		RaycastHit2D _raycast;
		Vector2 _rayCastOrigin = new Vector2(transform.position.x + RaycastOriginOffset.x, transform.position.y + RaycastOriginOffset.y);
		Vector2 _playerOrigin = transform.InverseTransformPoint(player.transform.position).normalized;

		float currentDistance = Vector2.Distance(player.transform.position, transform.position);
		if (currentDistance > ShootDistance) {
			currentDistance = ShootDistance;
		}

		// we cast a ray in front of the agent to check for a Player
		_raycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, Vector2.Distance(player.transform.position, transform.position), blockersLayerMask, Color.yellow, true);

		if (!_raycast && (Vector2.Distance(player.transform.position, transform.position) <= ShootDistance)) {
			facePlayer();

			if (animator != null) {
				MMAnimatorExtensions.UpdateAnimatorBool(animator, "Firing", true);
				character.MovementState.ChangeState(CharacterStates.MovementStates.Null);
				MMAnimatorExtensions.UpdateAnimatorFloat(animator, "playerPositionX", _playerOrigin.x);
				MMAnimatorExtensions.UpdateAnimatorFloat(animator, "playerPositionY", _playerOrigin.y);
			}

			characterShoot.CurrentWeapon.GetComponent<WeaponAim>().SetCurrentAim(_playerOrigin);
			characterShoot.ShootStart();
		}
		else {
			characterShoot.ShootStop();
		}
	}

	protected virtual void facePlayer() {
		if (!FaceTarget) {
			return;
		}

		if (this.transform.position.x > player.transform.position.x) {
			character.Face(Character.FacingDirections.Left);
		}
		else {
			character.Face(Character.FacingDirections.Right);
		}
	}
}
