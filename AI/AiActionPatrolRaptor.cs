﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiActionPatrolRaptor : AIActionPatrol
{
	protected Animator animator;
	protected int spawningAnimationParameter;

	// Start is called before the first frame update
	override protected void Start()
    {
		base.Start();

		animator = _character._animator;

		RegisterAnimatorParameter("Patrolling", AnimatorControllerParameterType.Bool, out spawningAnimationParameter);
	}

	protected override void Patrol() {
		MMAnimatorExtensions.UpdateAnimatorBool(animator, "Patrolling", true);
		base.Patrol();
	}

	public override void OnExitState() {
		MMAnimatorExtensions.UpdateAnimatorBool(animator, "Patrolling", false);
		base.OnExitState();
	}

	protected virtual void RegisterAnimatorParameter(string parameterName, AnimatorControllerParameterType parameterType, out int parameter) {
		parameter = Animator.StringToHash(parameterName);

		if (animator == null) {
			return;
		}
		if (animator.MMHasParameterOfType(parameterName, parameterType)) {
			_character._animatorParameters.Add(parameter);
		}
	}
}
