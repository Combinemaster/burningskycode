﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiActionChase : AIAction
{
    public override void PerformAction()
    {
		if (_character == null)
		{
			return;
		}
		if ((_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
			|| (_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Frozen))
		{
			return;
		}
		// moves the agent in its current direction
		CheckForTarget();
		CheckForWalls();
		CheckForHoles();
		_characterHorizontalMovement.SetHorizontalMove(_direction.x);
	}
	/// The agent's possible walk behaviours : patrol will have it walk in random directions until a wall or hole is hit, MoveOnSight will make the agent move only when "seeing" a target
	public enum WalkBehaviours { Patrol, MoveOnSight }

	[Information("Add this component to a Character and it will walk, turn back when it hits a wall, and try and avoid holes if you ask it to.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	/// The agent's walk behaviour
	public WalkBehaviours WalkBehaviour = WalkBehaviours.Patrol;

	[Header("Obstacle Detection")]
	[Information("Decide whether your character should change direction when hitting a wall, and if it should try to avoid holes.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	/// If set to true, the agent will change direction when hitting a wall
	public bool ChangeDirectionOnWall = true;
	/// If set to true, the agent will try and avoid falling
	public bool AvoidFalling = false;
	/// The offset the hole detection should take into account
	public Vector3 HoleDetectionOffset = new Vector3(0, 0, 0);
	/// the length of the ray cast to detect holes
	public float HoleDetectionRaycastLength = 1f;

	[Header("Move on Sight")]
	[Information("If your AI Walk's behaviour is set to Move On Sight, you can define here its 'view' distance (the length of the raycasts used to detect targets), the distance at which it should stop from the target, an offset for the raycast's origin, and the target layer.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	/// The maximum distance at which the AI can see the target
	public float ViewDistance = 10f;
	/// the horizontal distance from its target at which the agent will stop moving. Between that distance and the walk distance, the agent will slow down progressively
	public float StopDistance = 1f;
	/// The offset to apply to the raycast origin point (by default the position of the object)
	public Vector3 MoveOnSightRayOffset = new Vector3(0, 0, 0);
	/// the layer mask to use to detect targets
	public LayerMask MoveOnSightLayer;
	/// the layer mask of the sight obstacles (usually platforms)
	public LayerMask MoveOnSightObstaclesLayer;

	public bool stopFollowing;

	// private stuff
	protected CorgiController _controller;
	protected Character _character;
	protected Health _health;
	protected CharacterHorizontalMovement _characterHorizontalMovement;
	protected Vector2 _direction;
	protected Vector2 _startPosition;
	protected Vector2 _initialDirection;
	protected Vector3 _initialScale;
	protected float _distanceToTarget;
	private GameObject playerObject;
	protected BrusierJump brusierJump;
	protected CharacterLunge lungeRef;

	/// <summary>
	/// Initialization
	/// </summary>
	protected override void Start()
	{
		base.Start();
		Initialization();
		playerObject = GameObject.FindGameObjectWithTag("Player");
		brusierJump = GetComponent<BrusierJump>();
		lungeRef = GetComponent<CharacterLunge>();
	}

	protected override void Initialization()
	{
		// we get the CorgiController2D component
		_controller = GetComponent<CorgiController>();
		_character = GetComponent<Character>();
		_characterHorizontalMovement = GetComponent<CharacterHorizontalMovement>();
		_health = GetComponent<Health>();
		// initialize the start position
		_startPosition = transform.position;
		// initialize the direction
		_direction = _character.IsFacingRight ? Vector2.right : Vector2.left;

		_initialDirection = _direction;
		_initialScale = transform.localScale;
	}

	/// <summary>
	/// Checks for a wall and changes direction if it meets one
	/// </summary>
	protected virtual void CheckForWalls()
	{
		if (!ChangeDirectionOnWall)
		{
			return;
		}

		// if the agent is colliding with something, make it turn around
		if ((_direction.x < 0 && _controller.State.IsCollidingLeft) || (_direction.x > 0 && _controller.State.IsCollidingRight))
		{
			ChangeDirection();
		}
	}

	/// <summary>
	/// Checks for holes 
	/// </summary>
	protected virtual void CheckForHoles()
	{
		// if we're not grounded or if we're not supposed to check for holes, we do nothing and exit
		if (!AvoidFalling || !_controller.State.IsGrounded)
		{
			return;
		}

		// we send a raycast at the extremity of the character in the direction it's facing, and modified by the offset you can set in the inspector.
		Vector2 raycastOrigin = new Vector2(transform.position.x + _direction.x * (HoleDetectionOffset.x + Mathf.Abs(GetComponent<BoxCollider2D>().bounds.size.x) / 2), transform.position.y + HoleDetectionOffset.y - (transform.localScale.y / 2));
		RaycastHit2D raycast = MMDebug.RayCast(raycastOrigin, -transform.up, HoleDetectionRaycastLength, _controller.PlatformMask | _controller.MovingPlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.gray, true);
		// if the raycast doesn't hit anything
		if (!raycast)
		{
			// we change direction
			ChangeDirection();
		}
	}

	/// <summary>
	/// Casts a ray (if needed) to see if a target is in sight. If yes, moves towards it.
	/// </summary>
	protected virtual void CheckForTarget()
	{
		if (WalkBehaviour != WalkBehaviours.MoveOnSight)
		{
			return;
		}

		// we set up the origin (being the brusier) and the endpoint, being the player
		Vector2 raycastOrigin = transform.position + MoveOnSightRayOffset;
		Vector2 playerDirection = transform.InverseTransformPoint(playerObject.transform.position).normalized;
		RaycastHit2D raycastObstacle = MMDebug.RayCast(raycastOrigin, playerDirection, 1, MoveOnSightObstaclesLayer, Color.green, true);

		if (stopFollowing)
		{
			RaycastHit2D raycastObstacleStopMoving = MMDebug.RayCast(raycastOrigin, playerDirection, Vector2.Distance(playerObject.transform.position, transform.position), MoveOnSightObstaclesLayer, Color.red, true);
			if (raycastObstacleStopMoving)
			{
				_direction = Vector2.zero;
				return;
			}
		}

		if (playerDirection.x > 0)
		{
			_direction = Vector2.right;
		}
		else
		{
			_direction = Vector2.left;
		}

		Renderer playerRenderer = playerObject.GetComponent<Renderer>();
		if (playerRenderer == null)
		{
			playerRenderer = playerObject.GetComponentInChildren<MeshRenderer>();
		}

		//normalizes the position of the brusier and player to be on the lowest point of their model
		double playerYPostionNormalized = System.Math.Round(playerObject.transform.position.y, 1);
		double brusierYPostionNormalized = System.Math.Round(transform.position.y, 1);

		//then it compares if the brusiers position is higher or lower than the players. If it is, we know the user is below the brusier on the Y axis.
		bool targetBelow = brusierYPostionNormalized > playerYPostionNormalized;

		_distanceToTarget = Vector2.Distance(transform.position, playerObject.transform.position);

		// if the ray hit the player and they are within lunge distance, then execute the lunge
		if (!targetBelow &&
			_controller.State.IsGrounded &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Falling &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Dashing &&
			lungeRef.canLunge())
		{
			MMGameEvent.Trigger(DynamicCombatMusic.eventPlayerDetectedId);

			if (!raycastObstacle)
			{
				_direction = Vector2.zero;
			}
		}


		if (targetBelow && _controller.State.IsGrounded &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Falling &&
			_character.MovementState.CurrentState != CharacterStates.MovementStates.Dashing)
		{
			brusierJump.jumpDownFromOneWayPlatform();
		}

		// if the ray has hit nothing, or if we've reached our target, we prevent our character from moving further.
		if (_distanceToTarget <= StopDistance)
		{
			_direction = Vector2.zero;
		}
		else
		{
			// if we've hit something, we make sure there's no obstacle between us and our target
			if (raycastObstacle && _distanceToTarget > raycastObstacle.distance)
			{
				_direction = Vector2.zero;
			}
		}
	}

	/// <summary>
	/// Changes the agent's direction and flips its transform
	/// </summary>
	protected virtual void ChangeDirection()
	{
		_direction = -_direction;
	}

	protected virtual void OnRevive()
	{
		_direction = _character.IsFacingRight ? Vector2.right : Vector2.left;
		transform.localScale = _initialScale;
		transform.position = _startPosition;
	}

	/// <summary>
	/// When the player respawns, we reinstate this agent.
	/// </summary>
	/// <param name="checkpoint">Checkpoint.</param>
	/// <param name="player">Player.</param>
	protected virtual void OnEnable()
	{
		if (_health != null)
		{
			_health.OnRevive += OnRevive;
		}
	}

	protected virtual void OnDisable()
	{
		if (_health != null)
		{
			_health.OnRevive -= OnRevive;
		}
	}

}
