﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

/// <summary>
/// This class is responsible for managing the boss state by switching between the various states that the boss might be in.
/// This manage also controls the lights, health bars, change times between the bosses states, and control of which weapon the boss should used.
/// </summary>
public class BossManager : MonoBehaviour, ResetGameObject
{
	//The various boss states that the security system could be in. 
	public enum BossStates { Inactive, DestroyGenerator1, DestroyGenerator2, TransitionToBasicAttacks, BasicAttacks, TransitionToOverload, WeaponsOverload, WaveRegen, BasicAttacksAndSpeedUp, FinalBlow }

	//The current boss state, responsible for managing how the boss acts. 
	public MMStateMachine<BossStates> bossState;

	[Header("Boss Messages")]
	//Block of code that stores references to the messages that the boss would display that would communicate instruction to the user.
	//Instruction that the user will not follow.
	public GameObject accessDeniedMessage;
	public GameObject ceaseAndDesistMessage;

	[Header("Generator Blockers")]
	//After the initial destruction, these values are no longer used. 
	public GameObject generator1;
	public GameObject generatorCap1;
	public GameObject generator2;
	public GameObject generatorCap2;

	[Header("Lights")]
	//Block of code that tracks the lights that are present in the level. Usually turning them on/off depending on the phase. 
	public GameObject[] pointLights;
	public GameObject[] platformLights;
	public GameObject globalLight;
	public GameObject bossLight;

	[Header("Boss move locations")]
	//Block of code that tracks where the boss should teleport too. This can be increased or decreased depending on how much spots
	//we want the boss to move to and fro. 
	public GameObject[] bossMoveLocations;
	public GameObject bossMoveLocationRadialWeapon;

	[Header("Boss variables")]
	//Reference to the main boss
	public GameObject mainBoss;

	//block of code that tracks the weapons that the boss should use.
	//these weapons will be switched between depending on which spot the boss went when it moves. 
	public RadialWeapon radialWeapon;
	public LaserWeapon laserWeapon;
	public Weapon rocketWeapon;

	//Reference to the bosses health bar.
	public GameObject bossHealthBar;

	//Reference to the wave manager that is used in the boss fight.
	//depending on the number of enemies still in the wave manager the boss will regenerate. 
	public GenericWaveManager bossWaveManager;

	[Header("Music")]
	public AudioSource musicForEncounter;

	[Header("Phase Timings")]
	[Information("The following are numbers that represent the various timings that the boss will use. This is measured in seconds, so use wisely!", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	//How often the boss will change position on various non-overloaded phases. 
	public float turretChangePositionInSeconds = 1f;
	//How often the boss will move in it's overloaded state (WeaponsOverload)
	public float turretChangePositionOverloadInSeconds = 1f;
	//How long the overload stage will last
	public float weaponsOverloadOverloadDuration = 1f;
	//How long the player has to damage the boss before it overloads again. 
	public float basicAttacksBeforeSpeedUpDuration = 1f;
	//How long the player has to kill as many enemies as they can before they are sacrificed.
	public float sacrificeInSeconds = 1f;
	//Time, in seconds, that controls the time it takes for the boss to transition from having no lights to emergency lights + basic attacks. 
	public float transitionToBasicAttacksInSeconds = 0f;
	//Time, in seconds, that it takes for the boss to transition from basic attacks to the overload phase of his flowchart.
	public float transitionToOverloadInSeconds = 0f;
	//Time, in seconds, that controls the time it takes for the boss to transition from having no lights to emergency lights + basic attacks. 
	private float transitionToBasicAttacksInSecondsInternal = 0f;

	[Header("Boss State Variables")]
	[Information("The following numbers are what controls the increase in attack speed and frequency of teleportation after enemies are sacrificed, typically there is a max but you can set it to something absurd and never get there.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	//By how much the turret change duration will increase, this will make the boss move around at a quicker pace. 
	public float turretChangePositionInSecondsDecrease = 1f;
	//The min the boss will move at, the speed of the change cannot go below this number
	public float turretChangePositionInSecondsMin = 3f;

	//internal counter for the time for lock on orginial speed
	private float timeForLockOnOriginalSpeed;
	public float timeForLockOnOriginalSpeedIncrease;
	public float timeForLockOnOriginalSpeedLimit;
	//internal counter for the orginial closeness of the bosses laser
	private int laserTrackNumberOriginal;
	public int laserTrackNumberOriginalIncrease;
	public int laserTrackNumberOriginalLimit;
	//internal counter for the orginial number of lasers
	private int laserNumberOfRaysOriginal;
	public int laserNumberOfRaysOriginalDecrease;
	public int laserNumberOfRaysOriginalBottomLimit;

	//internal counter for the original rocket speed
	private float originalRocketInitialSpeed;
	public float originalRocketInitialSpeedIncrease;
	public float originalRocketInitialSpeedLimit;

	//internal counter for the original radial weapon projectile speed
	private int originalRadialProjectileCount;
	public int originalRadialProjectileCountIncrease;
	public int originalRadialProjectileCountLimit;

	//internal counter for the orginial speed that the boss was switching positions at
	private float originalTurretChangePositionTime;

	//internal counter for how long we should wait for before we change position for the boss.
	private float turretChangePositionCounter = 0f;
	//internal counter for how long we should wait for before we change position durning the overload phase.
	private float turretChangePositionOverloadCounter = 0f;
	//internal counter for how long the overload phase should last for
	private float weaponsOverloadInSecondsCounter = 0f;
	//internal counter for how long the basic attacks should go on for before they speed up. 
	private float basicAttacksBeforeSpeedUpCounter = 0f;
	//internal counter for how long the sacrifice should last for, once this counter reaches it's end the boss sacrifices the enemies left over. 
	private float sacrificeCounter = 0f;

	//bool to check if the boss has initilized itself, usually by setting the radialWeaponPool and bossWeaponPool
	private bool initialized = false;
	//bool to check if the boss has already been reset to 1 HP. if they have, 1 more shot kills the boss utterly. 
	private bool lastPhaseDone = false;

	//block of code that stores the weapon pools for each of the weapons the boss is going to use.
	//Mostly used to set individual speeds of projectiles
	private BurningSkySimpleObjectPooler radialWeaponPool;
	private BurningSkySimpleObjectPooler rocketWeaponPool;

	private SoundManager soundManagerRef;

	public bool respawned = true;

	bool ResetGameObject.respawned {
		get => respawned; set {
			respawned = value;
		}
	}

	public void Start() {
		bossState = new MMStateMachine<BossStates>(gameObject, true);
		bossState.ChangeState(BossStates.Inactive);

		weaponsOverloadInSecondsCounter = weaponsOverloadOverloadDuration;
		basicAttacksBeforeSpeedUpCounter = basicAttacksBeforeSpeedUpDuration;

		originalTurretChangePositionTime = turretChangePositionInSeconds;

		timeForLockOnOriginalSpeed = laserWeapon.timeForLockOn;
		laserTrackNumberOriginal = laserWeapon.laserTrackNumber;
		laserNumberOfRaysOriginal = laserWeapon.MagazineSize;

		transitionToBasicAttacksInSecondsInternal = transitionToBasicAttacksInSeconds;

		soundManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundManager>();
	}

	public void FixedUpdate() {
		initializeWeapons();
	}

	//Method call that initializes the bosses weapons. Forces the system to generate seperate pools for each of the weapons projectiles so they can be modified dynamically. 
	private void initializeWeapons() {
		if (!initialized) {
			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(radialWeapon, null);
			radialWeaponPool = mainBoss.GetComponent<CharacterHandleWeapon>().CurrentWeapon.GetComponent<BurningSkySimpleObjectPooler>();

			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(rocketWeapon, null);
			rocketWeaponPool = mainBoss.GetComponent<CharacterHandleWeapon>().CurrentWeapon.GetComponent<BurningSkySimpleObjectPooler>();

			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(laserWeapon, null);

			originalRocketInitialSpeed = rocketWeaponPool.getPooledGameObjects()[0].GetComponent<RocketProjectile>().Speed;
			originalRadialProjectileCount = radialWeapon.projectilesPerShot;

			initialized = true;
		}
	}

	public void Update() {
		if (mainBoss.GetComponent<Health>().CurrentHealth <= 0) {
			if (!lastPhaseDone) {
				resetBossTo1Hp();
				return;
			} else if (lastPhaseDone) {
				resetLights();
				return;
			}
			return;
		}

		if ((mainBoss.GetComponent<Health>().CurrentHealth < mainBoss.GetComponent<Health>().MaximumHealth * 0.75) && bossState.CurrentState.Equals(BossStates.BasicAttacks)) {
			mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
			bossState.ChangeState(BossStates.TransitionToOverload);
			return;
		}
	}

	//Method call that resets the boss to 1 HP so the user can finish him off. Used in the "FinalBlow" phase of the fight. 
	private void resetBossTo1Hp() {
		mainBoss.GetComponent<Health>().Invulnerable = true;
		mainBoss.GetComponent<Health>().Revive();
		mainBoss.SetActive(true);
		mainBoss.GetComponent<Health>().SetHealth(1, this.gameObject);
		bossWaveManager.resetGameObject();
		lastPhaseDone = true;
		weaponsOverloadInSecondsCounter = weaponsOverloadOverloadDuration;
		bossState.ChangeState(BossStates.FinalBlow);
	}

	//Method call that resets the lights back to their normal state. Happens after the boss was killed. 
	private void resetLights() {
		foreach (GameObject light in pointLights) {
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.white;
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 1;
		}

		foreach (GameObject light in platformLights) {
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0;
		}

		globalLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 1;
		bossLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0;
	}

	/// <summary>
	/// On LateUpdate, processes the weapon state
	/// </summary>
	protected virtual void LateUpdate() {
		processBossState();
	}

	/// <summary>
	/// Called every lastUpdate, processes the weapon's state machine
	/// </summary>
	private void processBossState() {
		if (bossState == null) { return; }

		switch (bossState.CurrentState) {
			case BossStates.DestroyGenerator1:
			caseDestroyGenerator1();
			break;

			case BossStates.DestroyGenerator2:
			caseDestroyGenerator2();
			break;

			case BossStates.TransitionToBasicAttacks:
			caseTransitionToBasicAttacks();
			break;

			case BossStates.BasicAttacks:
			caseBasicAttacks();
			break;

			case BossStates.TransitionToOverload:
			caseTransitionToOverload();
			break;

			case BossStates.WeaponsOverload:
			caseWeaponsOverload();
			break;

			case BossStates.WaveRegen:
			caseWaveRegen();
			break;

			case BossStates.BasicAttacksAndSpeedUp:
			caseBasicAttacksAndSpeedUp();
			break;

			case BossStates.FinalBlow:
			caseFinalBlow();
			break;
		}
	}

	//In this state, the bosses first generator gets exposed. 
	private void caseDestroyGenerator1() {
		accessDeniedMessage.SetActive(true);
		generator1.GetComponent<GeneratorScript>().generatorExposed = true;
		generatorCap1.SetActive(false);
		bossState.ChangeState(BossStates.Inactive);
	}

	//In this state, the bosses second generator gets exposed. Destroying the second generator causes the boss fight to start. 
	private void caseDestroyGenerator2() {
		accessDeniedMessage.SetActive(false);
		ceaseAndDesistMessage.SetActive(true);
		generator2.GetComponent<GeneratorScript>().generatorExposed = true;
		generatorCap2.SetActive(false);
		bossState.ChangeState(BossStates.Inactive);
	}

	//In this state, the boss starts transition to using basic attacks towards the player. 
	//This case only turns the lights off and then tu
	private void caseTransitionToBasicAttacks() {
		foreach (GameObject light in pointLights) {
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.red;
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0;
		}
		globalLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0.0f;
		ceaseAndDesistMessage.SetActive(false);
		transitionToBasicAttacksInSecondsInternal -= Time.deltaTime;
		if (transitionToBasicAttacksInSecondsInternal <= 0) {
			foreach (GameObject light in pointLights) {
				light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 1;
			}
			globalLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0.5f;
			bossLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 1f;
			mainBoss.GetComponent<Health>().Invulnerable = false;
			if (musicForEncounter != null) {
				soundManagerRef.PlayBackgroundMusic(musicForEncounter);
			}
			bossHealthBar.SetActive(true);
			bossState.ChangeState(BossStates.BasicAttacks);
		}
	}

	//In this state, the emergency lights turn on for just the platforms while turning off the rest of them. 
	//Then the boss will transition to "WeaponsOverload" phase.  
	private void caseTransitionToOverload() {
		foreach (GameObject light in pointLights) {
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0;
		}
		foreach (GameObject light in platformLights) {
			light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 2;
		}
		globalLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().intensity = 0.2f;
		mainBoss.GetComponent<Health>().Invulnerable = true;
		transitionToOverloadInSeconds -= Time.deltaTime;
		if (transitionToOverloadInSeconds <= 0) {
			bossState.ChangeState(BossStates.WeaponsOverload);
		}
	}

	//In this state, the boss will become immune to the players weapons and aggersively jump around, rapidly shooting it's weapons. 
	private void caseWeaponsOverload() {
		if (weaponsOverloadInSecondsCounter <= 0) {
			mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
			mainBoss.transform.position = bossMoveLocationRadialWeapon.transform.position;
			mainBoss.GetComponent<Health>().Invulnerable = true;
			sacrificeCounter = sacrificeInSeconds;
			bossWaveManager.startWave();
			bossState.ChangeState(BossStates.WaveRegen);
			return;
		} else {
			weaponsOverloadInSecondsCounter -= Time.deltaTime;
		}

		if (turretChangePositionOverloadCounter <= 0) {
			turretChangePositionOverloadCounter = turretChangePositionOverloadInSeconds;
			changePositionAndStartFiring();
		}
		else {
			turretChangePositionOverloadCounter -= Time.deltaTime;
		}
	}

	//In this state, the boss will use it's basic attacks, a machine gun and a radial weapon and attempt to kill the player. This state is used to introduce the player to the boss
	//and get them used to the constant jumping and unpredictableness. 
	private void caseBasicAttacks() {
		if (turretChangePositionCounter <= 0) {
			turretChangePositionCounter = turretChangePositionInSeconds;
			changePositionAndStartFiring();
		}
		else {
			turretChangePositionCounter -= Time.deltaTime;
		}
	}

	//Method call that changes the bosses position, equips a new weapon if required, and begins firing. 
	private void changePositionAndStartFiring() {
		mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
		int chosenLocation = Random.Range(0, bossMoveLocations.Length + 1);

		if (chosenLocation == 4) {
			mainBoss.transform.position = bossMoveLocationRadialWeapon.transform.position;
			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(radialWeapon, null);
			mainBoss.GetComponent<BossTurretShoot>().startFiring = true;
		} else if (chosenLocation == 1 || chosenLocation == 2) {
			mainBoss.transform.position = bossMoveLocations[chosenLocation].transform.position;
			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(rocketWeapon, null);
			mainBoss.GetComponent<BossTurretShoot>().startFiring = true;
		}
		else {
			mainBoss.transform.position = bossMoveLocations[chosenLocation].transform.position;
			mainBoss.GetComponent<CharacterHandleWeapon>().ChangeWeapon(laserWeapon, null);
			mainBoss.GetComponent<BossTurretShoot>().startFiring = true;
		}
	}

	//In this state, a sacrifice timer starts, the boss becomes immune, and does not fire any weapons. At the end of the timer, all remaining enemies are sacrificed, and the boss regenerates health.  
	private void caseWaveRegen() {
		if (sacrificeCounter <= 0) {
			bossWaveManager.continueNextWave = false;
			int currentEnemiesLeft = bossWaveManager.currentEnemiesLeft() + bossWaveManager.enemiesLeftInWaveManager();
			int healthToRecover = (int)(currentEnemiesLeft * (mainBoss.GetComponent<Health>().MaximumHealth * 0.03));
			mainBoss.GetComponent<BurningSkyHealth>().SetHealth(mainBoss.GetComponent<Health>().CurrentHealth + healthToRecover, this.gameObject);
			mainBoss.GetComponent<BurningSkyHealth>().UpdateBossHealth();
			mainBoss.GetComponent<BurningSkyHealth>().Invulnerable = false;
			bossWaveManager.resetGameObject();
			bossWaveManager.gameObject.SetActive(true);

			if (turretChangePositionInSeconds > turretChangePositionInSecondsMin) {
				turretChangePositionInSeconds -= turretChangePositionInSecondsDecrease;
				turretChangePositionCounter -= turretChangePositionInSeconds;
			}

			if (laserWeapon.timeForLockOn > timeForLockOnOriginalSpeedLimit) {
				laserWeapon.timeForLockOn -= timeForLockOnOriginalSpeedIncrease;
			}

			if (laserWeapon.laserTrackNumber < laserTrackNumberOriginalLimit) {
				laserWeapon.laserTrackNumber -= laserTrackNumberOriginalIncrease;
			}

			if (laserWeapon.MagazineSize > laserNumberOfRaysOriginalBottomLimit) {
				laserWeapon.MagazineSize -= laserNumberOfRaysOriginalDecrease;
			}

			if (rocketWeaponPool.getPooledGameObjects()[0].GetComponent<RocketProjectile>().Speed < originalRocketInitialSpeedLimit) {
				foreach (GameObject projectile in rocketWeaponPool.getPooledGameObjects()) {
					projectile.GetComponent<RocketProjectile>().Speed += originalRocketInitialSpeedIncrease;
				}
			}
			
			if (radialWeapon.projectilesPerShot < originalRadialProjectileCountLimit) {
				radialWeapon.projectilesPerShot += originalRadialProjectileCountIncrease;
			}

			bossState.ChangeState(BossStates.BasicAttacksAndSpeedUp);
		} else {
			sacrificeCounter -= Time.deltaTime;
		}
	}

	//In this state, the same basic attacks start, but at the end of this phase the weapons projectiles get faster and the boss will start the move around faster. 
	private void caseBasicAttacksAndSpeedUp() {
		if (basicAttacksBeforeSpeedUpCounter <= 0) {
			mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
			mainBoss.transform.position = bossMoveLocationRadialWeapon.transform.position;
			mainBoss.GetComponent<Health>().Invulnerable = true;
			sacrificeCounter = sacrificeInSeconds;
			weaponsOverloadInSecondsCounter = weaponsOverloadOverloadDuration;
			basicAttacksBeforeSpeedUpCounter = basicAttacksBeforeSpeedUpDuration;
			bossState.ChangeState(BossStates.WeaponsOverload);
			return;
		}
		else {
			basicAttacksBeforeSpeedUpCounter -= Time.deltaTime;
		}

		if (turretChangePositionCounter <= 0) {
			turretChangePositionCounter = turretChangePositionInSeconds;
			changePositionAndStartFiring();
		}
		else {
			turretChangePositionCounter -= Time.deltaTime;
		}
	}

	//Method call that will get triggered by the user when they click the console. 
	public void triggerToDestroyGenerator1() {
		bossState.ChangeState(BossStates.DestroyGenerator1);
	}

	//In this state, the boss does one finally flurry of attacks, and promptly becomes inactive. The player can then finish the boss off. 
	private void caseFinalBlow() {
		if (weaponsOverloadInSecondsCounter <= 0) {
			mainBoss.transform.position = bossMoveLocationRadialWeapon.transform.position;
			mainBoss.GetComponent<Health>().Invulnerable = false;
			mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
			return;
		}
		else {
			weaponsOverloadInSecondsCounter -= Time.deltaTime;
		}

		if (turretChangePositionOverloadCounter <= 0) {
			turretChangePositionOverloadCounter = turretChangePositionOverloadInSeconds;

			changePositionAndStartFiring();
		}
		else {
			turretChangePositionOverloadCounter -= Time.deltaTime;
		}
	}

	public void resetGameObject() {
		bossState.ChangeState(BossStates.Inactive);

		weaponsOverloadInSecondsCounter = weaponsOverloadOverloadDuration;
		basicAttacksBeforeSpeedUpCounter = basicAttacksBeforeSpeedUpDuration;

		generator1.GetComponent<GeneratorScript>().resetGenerator();
		generator2.GetComponent<GeneratorScript>().resetGenerator();

		mainBoss.transform.position = bossMoveLocationRadialWeapon.transform.position;
		mainBoss.GetComponent<BossTurretShoot>().startFiring = false;
		mainBoss.GetComponent<BurningSkyHealth>().Revive();

		turretChangePositionInSeconds = originalTurretChangePositionTime;
		turretChangePositionCounter = originalTurretChangePositionTime;
		transitionToBasicAttacksInSecondsInternal = transitionToBasicAttacksInSeconds;
		turretChangePositionCounter = 0;

		laserWeapon.timeForLockOn = timeForLockOnOriginalSpeed;
		laserWeapon.laserTrackNumber = laserTrackNumberOriginal;
		laserWeapon.MagazineSize = laserNumberOfRaysOriginal;

		foreach (GameObject projectile in rocketWeaponPool.getPooledGameObjects()) {
			projectile.GetComponent<RocketProjectile>().Speed = originalRocketInitialSpeed;
			projectile.SetActive(false);
		}

		radialWeapon.projectilesPerShot = originalRadialProjectileCount;

		bossHealthBar.SetActive(false);
		bossWaveManager.resetGameObject();

		resetLights();

		soundManagerRef.StopBackgroundMusic();
	}
}
