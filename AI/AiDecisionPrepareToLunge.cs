﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiDecisionPrepareToLunge : AIDecision
{
    public float chargeUpTime;
    protected float timeTillLunge;
    public override bool Decide()
    {
        if (timeTillLunge <= 0)
        {
            timeTillLunge = chargeUpTime;
			gameObject.GetComponent<AiDecisionFindPlayerBrusier>().ChargeUpFeedback.StopFeedbacks();
            return true;
        }
        else
        {
            timeTillLunge -= Time.deltaTime;
        }
        return false;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        timeTillLunge = chargeUpTime;
    }
}