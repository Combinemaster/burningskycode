﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnTouchBrusier : DamageOnTouch
{
	protected Animator animator;

	private void Start() {
		animator = GetComponent<Character>()._animator;
	}

	protected override void Colliding(Collider2D collider) {
		base.Colliding(collider);

		if (collider.tag == "Player") {
			MMAnimatorExtensions.UpdateAnimatorBool(animator, "Punching", true);
			if (gameObject.activeSelf == true) {
				StartCoroutine(stopAnimation());
			}
		}
	}

	private IEnumerator stopAnimation() {
		yield return new WaitForSeconds(0.000667f);
		MMAnimatorExtensions.UpdateAnimatorBool(animator, "Punching", false);
	}
}
