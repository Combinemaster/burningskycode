﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTurretShoot : AIShootOnSight {

	private GameObject player;
	public bool startFiring;

	protected override void Start() {
		base.Start();
		player = GameObject.FindGameObjectWithTag("Player");
	}

	protected override void Update() {
		if ((_character == null) || (_characterShoot == null)) { return; }

		if ((_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead)
					|| (_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Frozen) ||
					(_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Spawning)) {
			_characterShoot.ShootStop();
			return;
		}

		if (startFiring) {
			_characterShoot.CurrentWeapon.GetComponent<WeaponAim>().SetCurrentAim(player.transform.position - transform.position);
			_characterShoot.ShootStart();
		} else {
			_characterShoot.ShootStop();
		}
	}
}
