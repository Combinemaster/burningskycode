﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiDecisionFindPlayerBrusier : AIDecision
{
    protected CorgiController _controller;
    protected Character _character;
    private GameObject playerObject;
    protected CharacterLunge lungeRef;
    public Vector3 MoveOnSightRayOffset = new Vector3(0, 0, 0);
    public LayerMask MoveOnSightObstaclesLayer;
	public LayerMask targetLayerMask;
	public MMFeedbacks ChargeUpFeedback;
	public float YOffsetTargetBelow;

    public override bool Decide()
    {
        //normalizes the position of the brusier and player to be on the lowest point of their model
        double playerYPostionNormalized = System.Math.Round(playerObject.transform.position.y + YOffsetTargetBelow, 1);
        double brusierYPostionNormalized = System.Math.Round(transform.position.y, 1);

        //then it compares if the brusiers position is higher or lower than the players. If it is, we know the user is below the brusier on the Y axis.
        bool targetBelow = brusierYPostionNormalized > playerYPostionNormalized;

        Vector2 raycastOrigin = transform.position + MoveOnSightRayOffset;
        Vector2 playerDirection = transform.InverseTransformPoint(playerObject.transform.position).normalized;
        RaycastHit2D raycastObstacle = MMDebug.RayCast(raycastOrigin, playerDirection, 1, MoveOnSightObstaclesLayer, Color.green, true);

        // if the ray hit the player and they are within lunge distance, then execute the lunge
        if (!targetBelow &&
            _controller.State.IsGrounded &&
            _character.MovementState.CurrentState != CharacterStates.MovementStates.Falling &&
            _character.MovementState.CurrentState != CharacterStates.MovementStates.Dashing &&
            lungeRef.canLunge())
        {
			RaycastHit2D blockingRaycast;
			RaycastHit2D targetRaycast;
			Vector2 _rayCastOrigin = new Vector2(transform.position.x + MoveOnSightRayOffset.x, transform.position.y + MoveOnSightRayOffset.y);
			Vector2 _playerOrigin = transform.InverseTransformPoint(playerObject.transform.position).normalized;
			float distance = 8f;

			// we cast a ray in front of the agent to check for a Player
			blockingRaycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, distance, MoveOnSightObstaclesLayer, Color.yellow, true);
			targetRaycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, distance, targetLayerMask, Color.yellow, true);

			if (!blockingRaycast && targetRaycast) {
				_brain.Target = playerObject.transform;
				ChargeUpFeedback.PlayFeedbacks();
				return true;
			}
        }
        return false;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        playerObject = GameObject.FindGameObjectWithTag("Player");
        _controller = GetComponent<CorgiController>();
        _character = GetComponent<Character>();
        lungeRef = GetComponent<CharacterLunge>();
    }
}