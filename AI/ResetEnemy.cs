﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetEnemy : MonoBehaviour, ResetGameObject {

	private Health healthScriptRef;
	private CharacterLunge characterDash;
	public bool respawned = true;
	private Vector3 initialPosition;

	bool ResetGameObject.respawned {
		get => respawned; set {
			respawned = value;
		}
	}

	private void Start() {
		healthScriptRef = gameObject.GetComponent<Health>();
		characterDash = gameObject.GetComponent<CharacterLunge>();
		initialPosition = transform.position;
	}

	public void resetGameObject() {
		transform.position = initialPosition;
		healthScriptRef.ResetHealthToMaxHealth();
		healthScriptRef.Revive();

		if (characterDash != null) {
			characterDash.StopDash();
		}

		respawned = true;
	}
}
