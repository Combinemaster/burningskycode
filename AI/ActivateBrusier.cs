﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBrusier : MonoBehaviour
{
	public GameObject brusier;

	private void OnEnable() {
		brusier.GetComponent<AIBrain>().BrainActive = true;
	}
}
