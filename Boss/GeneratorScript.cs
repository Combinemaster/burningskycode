﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BossManager;

public class GeneratorScript : MonoBehaviour
{
	public GameObject bossManager; 
	public BossStates stateToTriggerOnDestroy;
	public GameObject cap;
	public bool generatorExposed = false;

	private BossManager bossManagerScriptRef;

	private void Start() {
		bossManagerScriptRef = bossManager.GetComponent<BossManager>();
	}

	private void OnTriggerEnter2D(Collider2D collision) {
		if (collision.gameObject.tag.Equals("PlayerBullet") && generatorExposed) {
			bossManagerScriptRef.bossState.ChangeState(stateToTriggerOnDestroy);
			gameObject.SetActive(false);
		}
	}

	public void resetGenerator() {
		gameObject.SetActive(true);
		generatorExposed = false;
		cap.SetActive(true);
	}
}
