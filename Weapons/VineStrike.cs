﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gizmos = Popcron.Gizmos;

public class VineStrike : BurningSkySpell
{
    bool vineStrikeTriggered;
    public GameObject vine;
	public Vector3 vineSpawnOffset;
	public float vineStrikeCooldown;
	public LayerMask NodeLayerMask;
	public Vector2 vineSwingJumpStrength = new Vector2(5, 5);

	private GameObject tempVine;
	private float vineStrikeCooldownInternal;
	private bool vineSwinging = false;
	private bool vineAttached = false;
	private GameObject vineSwingNodeRef;

	public override void ProcessAbility()
    {
		if (vineAttached) {
			if (vineSwingNodeRef != null) {
				Vector3 attachedNodePosition = vineSwingNodeRef.GetComponent<PendulumNode>().getAttachedNode().transform.position;

				gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, attachedNodePosition, 0.1f / Vector3.Distance(attachedNodePosition, transform.position));
				if (gameObject.transform.position == attachedNodePosition) {
					vineAttached = false;
					vineSwingNodeRef.GetComponent<PendulumNode>().startVineSwing();
				}
			}
		}
		else if (vineSwinging) {
			if (vineSwingNodeRef != null) {
				gameObject.transform.position = vineSwingNodeRef.GetComponent<PendulumNode>().getAttachedNode().transform.position;
			}
		}
		else {
			if (vineStrikeTriggered && tempVine == null && manaSystemRef != null && AbilityPermitted && manaSystemRef.abilityUsed(manaCost)) {
				tempVine = Instantiate(vine, gameObject.transform.position + vineSpawnOffset, Quaternion.identity);
				Projectile tempProjectile = tempVine.GetComponent<Projectile>();
				VineStrikeDamageOnTouch vineStrikeDamageOnTouch = tempVine.GetComponent<VineStrikeDamageOnTouch>();

				tempProjectile.SetDirection(transform.right * (!_character.IsFacingRight ? -1 : 1), transform.rotation, _character.IsFacingRight);
				vineStrikeTriggered = false;

				PlayAbilityStartFeedbacks();

				vineStrikeCooldownInternal = vineStrikeCooldown;
			}
			else {
				vineStrikeCooldownInternal -= Time.deltaTime;
			}
		}
	}
    protected override void HandleInput()
    {
		if (vineAttached) {
			//do nothing, the player is currently travelling to the node. 
			return;
		}

		if (vineSwinging) {
			if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				vineSwinging = false;
				vineSwingNodeRef.GetComponent<CircleCollider2D>().enabled = true;
				vineSwingNodeRef.GetComponent<PendulumNode>().stopVineSwing();
				vineSwingNodeRef = null;

				float jumpX = vineSwingJumpStrength.x;
				float jumpY = vineSwingJumpStrength.y;

				if (!_character.IsFacingRight) {
					jumpX = -vineSwingJumpStrength.x;
				}

				Vector2 jumpPower = new Vector2(jumpX, jumpY);

				_controller.SetForce(jumpPower);
				gameObject.GetComponent<CorgiController>().GravityActive(true);
				gameObject.GetComponent<Character>().MovementState.ChangeState(CharacterStates.MovementStates.Jumping);
			} else if (_inputManager.DashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				vineSwinging = false;
				vineSwingNodeRef.GetComponent<CircleCollider2D>().enabled = true;
				vineSwingNodeRef.GetComponent<PendulumNode>().stopVineSwing();
				vineSwingNodeRef = null;
				gameObject.GetComponent<CorgiController>().GravityActive(true);

				_character.GetComponent<BurningSkyDash>().InitiateDash();
			}
			return;
		}

		if (_inputManager.Spell2.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
			RaycastHit2D vineSwingNodeCheck = MMDebug.BoxCast(_character.gameObject.transform.position, new Vector2(5f, 5f), 0, new Vector2(1f, 0f), 10, NodeLayerMask, Color.red, true);

			if (vineSwingNodeCheck) {
				vineSwinging = true;
				vineAttached = true;

				vineSwingNodeRef = vineSwingNodeCheck.collider.gameObject;

				if (vineSwingNodeRef != null) {
					vineSwingNodeRef.GetComponent<CircleCollider2D>().enabled = false;
				}

				gameObject.GetComponent<CorgiController>().GravityActive(false);
				gameObject.GetComponent<CorgiController>().resetSpeed();
				gameObject.GetComponent<Character>().MovementState.ChangeState(CharacterStates.MovementStates.VineSwinging);
				gameObject.GetComponent<CorgiController>().State.IsFalling = false;
				vineSwingNodeRef.GetComponent<PendulumNode>().alignVineWithPlayer();

				return;
			}
		} 

		if (_inputManager.Spell2.State.CurrentState == MMInput.ButtonStates.ButtonDown && manaSystemRef.enoughManaToUseAbility(manaCost) && vineStrikeCooldownInternal < 0 && AbilityPermitted) {
			vineStrikeTriggered = true;
			_character.Freeze();
		}
		else if (_inputManager.Spell2.State.CurrentState == MMInput.ButtonStates.ButtonUp) {
			vineStrikeTriggered = false;
		}
	}
}
