﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gizmos = Popcron.Gizmos;

public class BurningSkySlam : CharacterDive
{
	public GameObject midAirSlamRef;
	public LayerMask enemiesLayerMask;
	public int damage;
	public Vector2 slamKnockback;
	public float InvincibilityDuration;
	public bool isCharacterSlaming;
	protected override void HandleInput()
	{
		if ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
				 && (_verticalInput < -_inputManager.Threshold.y))
		{
			// we start the dive coroutine
			InitiateDive();
		}
	}

	protected override void InitiateDive()
	{
		if (!AbilityPermitted // if the ability is not permitted
				|| (_controller.State.IsGrounded) // or if the character is grounded
				|| (_movement.CurrentState == CharacterStates.MovementStates.Gripping) // or if it's gripping
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)) // or if we're not under normal conditions
		{
			// we do nothing and exit
			return;
		}
		// we start the dive coroutine
		isCharacterSlaming = true;
		StartCoroutine(Dive());
	}
	protected override IEnumerator Dive()
	{
		// we start our sounds
		PlayAbilityStartFeedbacks();

		// we make sure collisions are on
		_controller.CollisionsOn();
		// we set our current state to Diving
		_movement.ChangeState(CharacterStates.MovementStates.Diving);
		midAirSlamRef.SetActive(true);

		// while the player is not grounded, we force it to go down fast
		while (!_controller.State.IsGrounded)
		{
			_controller.SetVerticalForce(-Mathf.Abs(_controller.Parameters.Gravity) * DiveAcceleration);
			yield return null; //go to next frame
		}

		midAirSlamRef.SetActive(false);

		ContactFilter2D contactFilter2D = new ContactFilter2D();
		contactFilter2D.SetLayerMask(enemiesLayerMask);

		List<RaycastHit2D> results = new List<RaycastHit2D>();

		Physics2D.BoxCast(_character.gameObject.transform.position, new Vector2(2, 2), 0, new Vector2(0,0), contactFilter2D, results);

		foreach (RaycastHit2D raycastHit2D in results)
		{
			GameObject gameObject = raycastHit2D.collider.gameObject;
			CorgiController _colliderCorgiController = gameObject.GetComponent<CorgiController>();
			Health health = gameObject.GetComponent<Health>();
			// if what we're colliding with is a CorgiController, we apply a knockback force
			if ((_colliderCorgiController != null) && (slamKnockback != Vector2.zero) && (!health.TemporaryInvulnerable) && (!health.Invulnerable) && (!health.ImmuneToKnockback))
			{
				Vector2 _knockbackForce;
				_knockbackForce.x = slamKnockback.x;

				Vector2 totalVelocity = _colliderCorgiController.Speed + _colliderCorgiController.Speed;
				_knockbackForce.x *= -1 * Mathf.Sign(totalVelocity.x);

				_knockbackForce.y = slamKnockback.y;

				_colliderCorgiController.SetForce(_knockbackForce);
			}
		
			if (health != null) {
				// we apply the damage to the thing we've collided with
				health.Damage(damage, midAirSlamRef, InvincibilityDuration, InvincibilityDuration);
			}
		}
		isCharacterSlaming = false;
		// once the player is grounded, we shake the camera, and restore the diving state to false
		if (_sceneCamera != null)
		{
			_sceneCamera.Shake(ShakeParameters);
		}

		// we play our exit sound
		StopStartFeedbacks();
		PlayAbilityStopFeedbacks();

		_movement.ChangeState(CharacterStates.MovementStates.Idle);
	}
}
