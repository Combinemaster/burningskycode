﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnTouchGrenade : DamageOnTouch
{
	/// The layers the agent will be blocked by, the greande cannot hit past walls. 
	public LayerMask interactableLayers;
	/// The offset to apply to the shoot origin point (by default the position of the object)
	public Vector2 raycastOriginOffset = new Vector2(0, 0);
	/// The offset to apply to the target (by default the position of the object)
	public Vector2 raycastTargetOffset = new Vector2(0, 0);

	protected override void Colliding(Collider2D collider) {
		if (!this.isActiveAndEnabled) {
			return;
		}

		// if the object we're colliding with is part of our ignore list, we do nothing and exit
		if (_ignoredGameObjects.Contains(collider.gameObject)) {
			return;
		}

		// if what we're colliding with isn't part of the target layers, we do nothing and exit
		if (!MMLayers.LayerInLayerMask(collider.gameObject.layer, TargetLayerMask)) {
			return;
		}

		Vector3 _rayCastOrigin = new Vector3(transform.position.x + raycastOriginOffset.x, transform.position.y + raycastOriginOffset.y, 0);
		Vector3 _targetOrigin = new Vector3(collider.gameObject.transform.position.x + raycastTargetOffset.x, collider.gameObject.transform.position.y + raycastTargetOffset.y, 0);

		Vector2 _rayCastDestination = (_targetOrigin - _rayCastOrigin).normalized;

		RaycastHit2D blockingRaycast = MMDebug.RayCast(transform.position, _rayCastDestination, GetComponent<CircleCollider2D>().radius, interactableLayers, Color.red, true);

		//We check if the target is being blocked by a wall or not. If they are, we do nothing and exit;
		if (blockingRaycast && blockingRaycast.collider.gameObject.layer != LayerMask.NameToLayer("Enemies")) {
			return;
		}

		/*if (Time.time - _knockbackTimer < InvincibilityDuration)
		{
			return;
		}
		else
		{
			_knockbackTimer = Time.time;
		}*/

		_colliderHealth = collider.gameObject.MMGetComponentNoAlloc<Health>();

		// if what we're colliding with is damageable
		if (_colliderHealth != null) {
			if (_colliderHealth.CurrentHealth > 0) {
				OnCollideWithDamageable(_colliderHealth);
			}
		}

		// if what we're colliding with can't be damaged
		else {
			OnCollideWithNonDamageable();
		}
	}
}
