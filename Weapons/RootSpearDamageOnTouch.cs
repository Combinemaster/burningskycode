﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CharacterType;

public class RootSpearDamageOnTouch : DamageOnTouch
{
	public float rootTimeRanged = 1.5f;
	public float rootTimeMelee = 0.75f;
	public float rootTimeFlying = 1.5f;
	public float miniStunTime = 1f;
	public Sprite rootImage;

	private GameObject player;

	/// <summary>
	/// Initialization
	/// </summary>
	protected override void Awake() {
		player = GameObject.FindGameObjectWithTag("Player");
		base.Awake();
	}

	protected override void OnCollideWithDamageable(Health health) {
		CharacterType charType = health.gameObject.MMGetComponentNoAlloc<CharacterType>();
		StatusEffectEnemy statusEffectEnemy = health.gameObject.MMGetComponentNoAlloc<StatusEffectEnemy>();
		_colliderCorgiController = health.gameObject.MMGetComponentNoAlloc<CorgiController>();

		if (charType != null && statusEffectEnemy != null && (_colliderCorgiController != null) && (!_colliderHealth.TemporaryInvulnerable) && (!_colliderHealth.Invulnerable) && (!_colliderHealth.ImmuneToKnockback)) {
			StatusEffectRoot root = new StatusEffectRoot();

			switch (charType.enemyType) {
				case EnemyType.Ranged:
				root.statusEffectInitialize(health.gameObject, StatusEffect.StatusEffectId.Root, rootTimeRanged, 1, 0, rootImage);
				break;

				case EnemyType.Melee:
				root.statusEffectDelegate += root.statusEffectMelee;
				root.statusEffectEndDelegate += root.statusEffectMeleeEnd;

				root.statusEffectInitialize(health.gameObject, StatusEffect.StatusEffectId.Root, rootTimeMelee, 1, 0, rootImage);
				break;

				case EnemyType.Flying:
				root.statusEffectDelegate += root.statusEffectFlying;
				root.statusEffectEndDelegate += root.statusEffectFlyingEnd;

				root.statusEffectInitialize(health.gameObject, StatusEffect.StatusEffectId.Root, rootTimeFlying, 1, 0, rootImage);
				break;

				default:
				Debug.Log("no-op, root ability behavior has to be defined!");
				break;
			}

			statusEffectEnemy.addNewStatusEffect(root);

			player.GetComponent<ComboManager>().addAttackToCombo(ComboManager.AttacksPossible.RootSpear, health.gameObject);
		}

		base.OnCollideWithDamageable(health);
	}
}
