﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class BurningSkyClawAttack : CharacterHandleWeapon
{
	public ComboManager comboManager;

	public bool clawAttackOn = false;

	public float clawAttackSensitivity;
	private float durationInternal;

	public void handleInputClawWeaponAttack()
	{
		if (comboManager != null && !comboManager.isAttackInProgress())
		{
			if (((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown))
			{
				ShootStart();
				clawAttackOn = true;
			}
			if (CurrentWeapon != null)
			{
				if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)))
				{
					ShootStart();
					clawAttackOn = true;
				}
				if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed))
				{
					ShootStart();
					clawAttackOn = true;
				}
			}
			if (((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp)) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp))
			{
				ShootStop();
			}
			if (CurrentWeapon != null)
			{
				if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
					&& ((_inputManager.ShootAxis == MMInput.ButtonStates.Off) && (_inputManager.ClawMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.Off)))
				{
					CurrentWeapon.WeaponInputStop();
				}
			}
		}
	}
	protected override void Start()
	{
		base.Start();
		comboManager = gameObject.GetComponent<ComboManager>();
		durationInternal = clawAttackSensitivity;
	}

	protected override void HandleInput()
	{
		handleInputClawWeaponAttack();
	}

	public override void ProcessAbility()
	{
		base.ProcessAbility();
		if (clawAttackOn) {
			if (durationInternal > 0)
			{
				durationInternal -= Time.deltaTime;
			}
			else
			{
				durationInternal = clawAttackSensitivity;
				clawAttackOn = false;
			}
		}
		else
		{
			durationInternal = clawAttackSensitivity;
		}
	}
}