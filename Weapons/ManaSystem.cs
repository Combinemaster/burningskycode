﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaSystem : MonoBehaviour
{
	public float startingMana = 40;
	public float maxMana = 40;

	[ReadOnly]
	public float currentMana;

	private BurningSkyGuiManager guiManagerRef;

	// Start is called before the first frame update
	void Start()
    {
		guiManagerRef = FindObjectOfType<BurningSkyGuiManager>();
		currentMana = startingMana;

		if (guiManagerRef != null) {
			guiManagerRef.SetUpManaBar(true, GetComponent<Character>().PlayerID);
			updateManaBar();
		}

		GetComponent<BurningSkyHealth>().OnDeath += resetMana;
	}

	private void updateManaBar() {
		if (Application.isPlaying) {
			if (guiManagerRef != null) {
				guiManagerRef.UpdateManaBar(currentMana, maxMana, GetComponent<Character>().PlayerID);
			}
		}
	}

	public bool abilityUsed(float manaUsed) {
		if (currentMana <= 0) {
			return false;
		} else if (currentMana < manaUsed) {
			return false;
		} else {
			currentMana -= manaUsed;
			updateManaBar();
			return true;
		}
	}

	public void manaRecovered(float manaRecovered) {
		if (currentMana != maxMana) {
			currentMana += manaRecovered;
		}

		if (currentMana > maxMana) {
			currentMana = maxMana;
		}

		updateManaBar();
	}

	public bool enoughManaToUseAbility(float manaCost) {
		if (currentMana >= manaCost) {
			return true;
		} else {
			return false;
		}
	} 

	public void resetMana() {
		currentMana = startingMana;
		updateManaBar();
	}
}
