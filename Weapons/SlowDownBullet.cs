﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static StatusEffect;

public class SlowDownBullet : MonoBehaviour
{
	public StatusEffectId typeOfStatusEffect;
	public float statusTimeLeft;
	public int statusMaxStack;

	private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 13) //Layer 13 is Enemy Layer (Thanks Unity)
        {
			StatusEffectSlowdown statusEffectSlowDown = new StatusEffectSlowdown();
			statusEffectSlowDown.statusEffectInitialize(collision.gameObject, typeOfStatusEffect, statusTimeLeft, statusMaxStack, 0, null);

			statusEffectSlowDown.statusEffectDelegate += statusEffectSlowDown.statusEffect;
			statusEffectSlowDown.statusEffectEndDelegate += statusEffectSlowDown.statusEffectEnd;

			if (collision.gameObject.GetComponent<CharacterType>().enemyType.Equals(CharacterType.EnemyType.Ranged)) {
				statusEffectSlowDown.statusEffectDelegate += statusEffectSlowDown.statusEffectRaptor;
				statusEffectSlowDown.statusEffectEndDelegate += statusEffectSlowDown.statusEffectEndRaptor;
			} else if (collision.gameObject.GetComponent<CharacterType>().enemyType.Equals(CharacterType.EnemyType.Melee)) {
				statusEffectSlowDown.statusEffectDelegate += statusEffectSlowDown.statusEffectBrusier;
				statusEffectSlowDown.statusEffectEndDelegate += statusEffectSlowDown.statusEffectEndBrusier;
			}

			StatusEffectEnemy statusScript = collision.gameObject.GetComponent<StatusEffectEnemy>();
			if (statusScript != null) {
				statusScript.addNewStatusEffect(statusEffectSlowDown);
			}
		}
    }
}
