﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ComboManager;
using Gizmos = Popcron.Gizmos;

public class BurningSkyMeleeWeapon : MeleeWeapon
{
	public float bounceHeight;
	public float manaRecovered = 2.5f;
	//A small adjustment either + or - to the mini stuns that occurs when the melee hits.
	public float miniStunAdjustment = 0.1f;

	//This is different than TypeOfAttack in BruningSkyCharacterHandleWeapon. That one controls the type of attack to handle for that script (Which we might remove in due time),
	//this one controls what sort of attack it is in relation to the combo system. 
	public AttacksPossible attackType;

	public GameObject damageAreaSprite;

	/// The layers the agent will be blocked by, the grenade cannot hit past walls. 
	public LayerMask interactableLayersWeapon;
	/// The offset to apply to the shoot origin point (by default the position of the object)
	public Vector2 raycastOriginOffsetWeapon = new Vector2(0, 0);
	/// The offset to apply to the target (by default the position of the object)
	public Vector2 raycastTargetOffsetWeapons = new Vector2(0, 0);

	private ComboManager comboManager;

	/// <summary>
	/// Initialization
	/// </summary>
	public override void Initialization() {
		base.Initialization();
		comboManager = Owner.gameObject.GetComponent<ComboManager>();
	}

	protected override IEnumerator MeleeWeaponAttack() {
		if (_attackInProgress) { yield break; }
	
		_attackInProgress = true;

		int extraDamageCaused = 0;
		ChargedForm chargedFormRef = Owner.GetComponent<ChargedForm>();

		if (chargedFormRef != null && chargedFormRef.chargedFormActive) {
			extraDamageCaused = chargedFormRef.chargedFormExtraDamage;
		}

		_damageOnTouch.DamageCaused = DamageCaused + extraDamageCaused;

		if (comboManager != null && !comboManager.isAttackInProgress()) {
			if (comboManager != null) {
				comboManager.enableAttackInProgress();
			}

			yield return new WaitForSeconds(InitialDelay);
			EnableDamageArea();
			yield return new WaitForSeconds(ActiveDuration);
			DisableDamageArea();

			/*	if (Owner.IsFacingRight) {
					_gizmoOffset = AreaOffset;
				}
				else {
					_gizmoOffset = -AreaOffset;
				}
				*/
			_gizmoOffset = AreaOffset;

			Gizmos.Cube(_damageOnTouch.gameObject.transform.position + _gizmoOffset, transform.rotation, AreaSize, Color.green);

			if (comboManager != null) {
				comboManager.disableAttackInProgress();
			}
		}

		_attackInProgress = false;
	}

	protected override void EnableDamageArea() {
		if (damageAreaSprite != null) {
			damageAreaSprite.SetActive(true);
		}
		base.EnableDamageArea();
	}

	protected override void DisableDamageArea() {
		if (damageAreaSprite != null) {
			damageAreaSprite.SetActive(false);
		}
		base.DisableDamageArea();
	}

	protected override void CaseWeaponUse()
	{
		base.CaseWeaponUse();
		if (_comboWeapon != null)
		{
			_comboWeapon.resetOffset();
		}
	}

	protected override void CreateDamageArea()
	{
		_damageArea = new GameObject();
		_damageArea.name = this.name + "DamageArea";
		_damageArea.transform.position = this.transform.position;
		_damageArea.transform.rotation = this.transform.rotation;
		_damageArea.transform.SetParent(this.transform);

		if (DamageAreaShape == MeleeDamageAreaShapes.Rectangle)
		{
			_boxCollider2D = _damageArea.AddComponent<BoxCollider2D>();
			_boxCollider2D.offset = AreaOffset;
			_boxCollider2D.size = AreaSize;
			_damageAreaCollider = _boxCollider2D;
		}
		if (DamageAreaShape == MeleeDamageAreaShapes.Circle)
		{
			_circleCollider2D = _damageArea.AddComponent<CircleCollider2D>();
			_circleCollider2D.transform.position = this.transform.position + this.transform.rotation * AreaOffset;
			_circleCollider2D.radius = AreaSize.x / 2;
			_damageAreaCollider = _circleCollider2D;
		}
		_damageAreaCollider.isTrigger = true;

		Rigidbody2D rigidBody = _damageArea.AddComponent<Rigidbody2D>();
		rigidBody.isKinematic = true;

		_damageOnTouch = _damageArea.AddComponent<BurningSkyBounce>();
		_damageOnTouch.TargetLayerMask = TargetLayerMask;
		_damageOnTouch.DamageCaused = DamageCaused;
		_damageOnTouch.DamageCausedKnockbackType = Knockback;
		_damageOnTouch.DamageCausedKnockbackForce = KnockbackForce;
		_damageOnTouch.InvincibilityDuration = InvincibilityDuration;
		(_damageOnTouch as BurningSkyBounce).meleeWeaponRef = this;
		(_damageOnTouch as BurningSkyBounce).manaRecovered = manaRecovered;
		(_damageOnTouch as BurningSkyBounce).miniStunAdjustment = miniStunAdjustment;
		(_damageOnTouch as BurningSkyBounce).interactableLayers = interactableLayersWeapon;
		(_damageOnTouch as BurningSkyBounce).raycastOriginOffset = raycastOriginOffsetWeapon;
		(_damageOnTouch as BurningSkyBounce).raycastTargetOffset = raycastTargetOffsetWeapons;
	}
}
