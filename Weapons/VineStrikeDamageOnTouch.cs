﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VineStrikeDamageOnTouch : DamageOnTouch {
	public Vector2 PullStrength = new Vector2(10, 2);
	public float miniStunTime = 1f;

	private GameObject player;

	/// <summary>
	/// Initialization
	/// </summary>
	protected override void Awake() {
		player = GameObject.FindGameObjectWithTag("Player");
		base.Awake();
	}

	protected override void OnCollideWithDamageable(Health health) {

		_colliderCorgiController = health.gameObject.MMGetComponentNoAlloc<CorgiController>();

		if ((_colliderCorgiController != null) && (!_colliderHealth.TemporaryInvulnerable) && (!_colliderHealth.Invulnerable) && (!_colliderHealth.ImmuneToKnockback)) {
			if (player.tag == "Player") {
				float distance = Vector3.Distance(health.gameObject.transform.position, player.transform.position);

				PullStrength.x = (distance / 3) * PullStrength.x;

				if (player.GetComponent<Character>().IsFacingRight) {
					Vector2 invertedPullStrength = PullStrength;
					invertedPullStrength.x = -PullStrength.x;
					_colliderCorgiController.AddForce(invertedPullStrength);
				} else {
					_colliderCorgiController.AddForce(PullStrength);
				}

				player.GetComponent<ComboManager>().addAttackToCombo(ComboManager.AttacksPossible.VineStrike, health.gameObject);

				StatusEffectEnemy statusEffectEnemy = health.gameObject.MMGetComponentNoAlloc<StatusEffectEnemy>();

				if (statusEffectEnemy != null) {
					StatusEffectStun stun = new StatusEffectStun();
					stun.statusEffectInitialize(health.gameObject, StatusEffect.StatusEffectId.Stun, miniStunTime, 1, 0, null);

					if (health.gameObject.GetComponent<CharacterType>() != null && health.gameObject.GetComponent<CharacterType>().isCharacterMelee()) {
						stun.statusEffectMeleeInitialize(health.GetComponent<DamageOnTouchBrusier>());
					}

					statusEffectEnemy.addNewStatusEffect(stun);
				}
				else {
					Debug.Log("Status effect couldn't be applied! Either handle this case or add a StatusEffectEnemy script to the enemy!");
				}
			}
		}

		base.OnCollideWithDamageable(health);
	}
}
