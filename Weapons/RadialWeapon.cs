﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialWeapon : Weapon {

	/// the number of projectiles to spawn per shot
	public int projectilesPerShot = 1;
	public MMObjectPooler objectPooler { get; set; }
	public float randomizedSpreadAmount = 45;
	public bool randomizeForEveryBullet = false;

	protected bool poolInitialized = false;

	private static int TOTAL_COVERAGE_FOR_WEAPON = 360;

	private float currentAngle;

	/// <summary>
	/// Initialize this weapon
	/// </summary>
	public override void Initialization() {
		base.Initialization();
		_aimableWeapon = GetComponent<WeaponAim>();

		if (!poolInitialized) {
			if (GetComponent<MMMultipleObjectPooler>() != null) {
				objectPooler = GetComponent<MMMultipleObjectPooler>();
			}
			if (GetComponent<MMSimpleObjectPooler>() != null) {
				objectPooler = GetComponent<MMSimpleObjectPooler>();
			}
			if (objectPooler == null) {
				Debug.LogWarning(this.name + " : no object pooler (simple or multiple) is attached to this Projectile Weapon, it won't be able to shoot anything.");
				return;
			}
			poolInitialized = true;
		}

		currentAngle = (TOTAL_COVERAGE_FOR_WEAPON / projectilesPerShot);
	}

	/// <summary>
	/// Called everytime the weapon is used
	/// </summary>
	protected override void WeaponUse() {
		base.WeaponUse();

		float adjustedAngle = currentAngle + randomizedSpreadAmount;

		for (int i = 0; i < projectilesPerShot; i++) {
			spawnProjectile(transform.position, adjustedAngle, true);
			adjustedAngle += TOTAL_COVERAGE_FOR_WEAPON / projectilesPerShot;
		}

		currentAngle = adjustedAngle;
	}

	/// <summary>
	/// Spawns a new object and positions/resizes it
	/// </summary>
	private GameObject spawnProjectile(Vector3 spawnPosition, float bulletAngle, bool triggerObjectActivation = true) {
		/// we get the next object in the pool and make sure it's not null
		GameObject nextGameObject = objectPooler.GetPooledGameObject();

		// mandatory checks
		if (nextGameObject == null) { return null; }
		if (nextGameObject.GetComponent<MMPoolableObject>() == null) {
			throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
		}

		// we position the object
		nextGameObject.transform.position = spawnPosition;
		// we set its direction

		Projectile projectile = nextGameObject.GetComponent<Projectile>();
		if (projectile != null) {
			projectile.SetWeapon(this);
			if (Owner != null) {
				projectile.SetOwner(Owner.gameObject);
			}
		}
		// we activate the object
		nextGameObject.gameObject.SetActive(true);

		if (projectile != null) {
			Vector2 projectileRotation = Quaternion.AngleAxis(bulletAngle, Vector3.forward) * Vector2.right;
			projectile.Direction = projectileRotation;
			projectile.gameObject.transform.rotation = Quaternion.AngleAxis(bulletAngle, Vector3.forward);
		}

		if (triggerObjectActivation) {
			if (nextGameObject.GetComponent<MMPoolableObject>() != null) {
				nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
			}
		}

		return (nextGameObject);
	}
}
