﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAim : MonoBehaviour
{
	protected Character _character;
	protected Animator _animator;

	protected int _playerAimXAnimationParameter;
	protected int _playerAimYAnimationParameter;


	// Start is called before the first frame update
	void Start()
    {
		_character = GetComponent<Character>();
		_animator = _character._animator;

		RegisterAnimatorParameter("PlayerMousePositionX", AnimatorControllerParameterType.Float, out _playerAimXAnimationParameter);
		RegisterAnimatorParameter("PlayerMousePositionY", AnimatorControllerParameterType.Float, out _playerAimYAnimationParameter);
	}

	protected virtual void RegisterAnimatorParameter(string parameterName, AnimatorControllerParameterType parameterType, out int parameter) {
		parameter = Animator.StringToHash(parameterName);

		if (_animator == null) {
			return;
		}
		if (_animator.MMHasParameterOfType(parameterName, parameterType)) {
			_character._animatorParameters.Add(parameter);
		}
	}

	// Update is called once per frame
	void Update()
    {
		Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector2 direction = (Vector2)((worldMousePos - transform.position));

		MMAnimatorExtensions.UpdateAnimatorFloat(_animator, "PlayerMousePositionX", direction.normalized.x);
		MMAnimatorExtensions.UpdateAnimatorFloat(_animator, "PlayerMousePositionY", direction.normalized.y);

		if  (direction.normalized.x < 0) {
			_character.Face(Character.FacingDirections.Left);
		}
		else {
			_character.Face(Character.FacingDirections.Right);
		}
	}
}
