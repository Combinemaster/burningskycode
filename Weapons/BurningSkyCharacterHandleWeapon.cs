﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BurningSkyCharacterHandleWeapon : CharacterHandleWeapon
{
	public ComboManager comboManager;

	/// all possible states for the object
	public enum TypeOfAttack { Ranged, Light, Heavy, Claw, DroneGrenade, DroneRanged, SMAWeapon }
	public TypeOfAttack typeOfAttack;
	public LayerMask enemiesLayerMask;
	public int grenadeSensitivity = 10;
	public BurningSkyCharacterHandleWeapon grenadeRefScript;
	public float downwardsAttackSensitivity;
	public float grenadeSideSoundSensitivity; //This is the sensitivity for the moving sound of the grenade launcher

	private bool grenadeFiredLastFrame = false;
	private bool grenadeButtonTapped = false;
	private bool gunFiredLastFrame = false;

	private int framesHeldDown = 0;
	private float magnitudeLastFrame;
	private double lastFrameXMovement;
	private float grenadeSideSoundSensitivityInternal; //This is the sensitivity for the moving sound of the grenade launcher, for internal class use only.

	protected Vector2 areaOffsetRef; //Ref to the area offset for directional attacking
	protected Vector2 areaOffsetOld; //A Holding varriable for the old area offset

	/// the feedbacks to play when the ability starts
	public MMFeedbacks GrenadeLauncherStartMoving;
	public MMFeedbacks GrenadeLauncherStopMoving;
	public MMFeedbacks GrenadeLauncherMoving;

	public BurningSkySlam slamRef;

	public BurningSkyClawAttack clawAttackRef;

	protected override void Start() {
		base.Start();
		comboManager = gameObject.GetComponent<ComboManager>();
	}

	/// <summary>
	/// Gets input and triggers methods based on what's been pressed
	/// </summary>
	protected override void HandleInput() {
		switch (typeOfAttack) {
			case TypeOfAttack.Light:
			handleInputLightWeaponAttack();
			break;
			case TypeOfAttack.Heavy:
			handleInputHeavyWeaponAttack();
			break;
			case TypeOfAttack.Claw:
			handleInputClawWeaponAttack();
			break;
			case TypeOfAttack.DroneGrenade:
			handleDroneGrenadeWeaponAttack();
			break;
			case TypeOfAttack.DroneRanged:
			handleDroneRangedWeaponAttack();
			break;
			case TypeOfAttack.SMAWeapon:
			handleSMAWeaponAttack();
			break;
			default:
			handleRegularWeaponAttack();
			break;
		}
	}

	public void handleInputLightWeaponAttack() {
		if (clawAttackRef.clawAttackOn == false) {
			if (_inputManager.PrimaryMovement.y != 0)
			{
				areaOffsetRef = CurrentWeapon.gameObject.GetComponent<ComboWeapon>().Weapons[0].AreaOffset;
				if (areaOffsetRef.y != 0)
				{
					areaOffsetRef = areaOffsetOld;
				}
				areaOffsetOld = areaOffsetRef;
				if ((_inputManager.PrimaryMovement.x < (0 + downwardsAttackSensitivity)) && (_inputManager.PrimaryMovement.x > (0 - downwardsAttackSensitivity)))
				{
					if ((_inputManager.PrimaryMovement.y != 0) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown))
					{
						if ((_inputManager.PrimaryMovement.y < 0) && _character.Airborne)
						{
							areaOffsetRef.Set(0, -1);
						}
						if ((_inputManager.PrimaryMovement.y > 0))
						{
							areaOffsetRef.Set(0, 1);
						}
						foreach (MeleeWeapon meleeWeapon in CurrentWeapon.gameObject.GetComponent<ComboWeapon>().Weapons)
						{
							meleeWeapon.AreaOffset = areaOffsetRef;
							meleeWeapon.refreshDamageArea();
						}
						ShootStart();
					}
				}
			}

			if (comboManager != null && !comboManager.isAttackInProgress()) {
				if ((_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown)) {
					ShootStart();
				}

				if (CurrentWeapon != null) {
					if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed)) {
						ShootStart();
					}
					if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed)) {
						ShootStart();
					}
				}

				if ((_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp)) {
					ShootStop();
				}

				if (CurrentWeapon != null) {
					if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
					&& ((_inputManager.ShootAxis == MMInput.ButtonStates.Off) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.Off))) {
						CurrentWeapon.WeaponInputStop();
					}
				}
			}
		}
	}

	public void handleInputHeavyWeaponAttack() {
		if (clawAttackRef.clawAttackOn == false) {
			if ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
					 && (_verticalInput < -_inputManager.Threshold.y))
			{
				return;
			}

			if (comboManager != null && !comboManager.isAttackInProgress()) {
				if ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown)) {
					ShootStart();
				}

				if (CurrentWeapon != null) {
					if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed)) {
						ShootStart();
					}
					if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed)) {
						ShootStart();
					}
				}

				if ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp)) {
					ShootStop();
				}

				if (CurrentWeapon != null) {
					if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
					&& ((_inputManager.ShootAxis == MMInput.ButtonStates.Off) && (_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.Off))) {
						CurrentWeapon.WeaponInputStop();
					}
				}
			}
		}
	}

	public void handleInputClawWeaponAttack() {
		if (comboManager != null && !comboManager.isAttackInProgress()) {
			if (((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown)) {
					ShootStart();
			}
			if (CurrentWeapon != null) {
				if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && ((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonDown))) {
						ShootStart();
				}
				if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed)) {
						ShootStart();
				}
			}
			if (((_inputManager.HeavyMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) && (_inputManager.LightMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.ButtonUp)) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp)) {
					ShootStop();
			}
			if (CurrentWeapon != null) {
				if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
					&& ((_inputManager.ShootAxis == MMInput.ButtonStates.Off) && (_inputManager.ClawMeleeAttackButton.State.CurrentState == MMInput.ButtonStates.Off))) {
						CurrentWeapon.WeaponInputStop();
				}
			}
		}
	}

	public void handleRegularWeaponAttack() {
		if ((_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonDown)) {
			ShootStart();
		}

		if (CurrentWeapon != null) {
			if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonPressed)) {
				ShootStart();
			}
			if (ContinuousPress && (CurrentWeapon.TriggerMode == Weapon.TriggerModes.Auto) && (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonPressed)) {
				ShootStart();
			}
		}

		if (_inputManager.ReloadButton.State.CurrentState == MMInput.ButtonStates.ButtonDown && !typeOfAttack.Equals(TypeOfAttack.DroneRanged)) {
			Reload();
		}

		if ((_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) || (_inputManager.ShootAxis == MMInput.ButtonStates.ButtonUp)) {
			ShootStop();
		}

		if (CurrentWeapon != null) {
			if ((CurrentWeapon.WeaponState.CurrentState == Weapon.WeaponStates.WeaponDelayBetweenUses)
			&& ((_inputManager.ShootAxis == MMInput.ButtonStates.Off) && (_inputManager.ShootButton.State.CurrentState == MMInput.ButtonStates.Off))) {
				CurrentWeapon.WeaponInputStop();
			}
		}
	}

	public void handleDroneGrenadeWeaponAttack() {
		if (_inputManager.isGamepadEnabled) {
			_aimableWeapon.SetCurrentAim(_inputManager.SecondaryMovement);

			if (grenadeButtonTapped) {
				if (_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
					if (!GrenadeLauncherMoving.IsPlaying) {
						GrenadeLauncherMoving.PlayFeedbacks();
					}
					else if (System.Math.Round(_inputManager.SecondaryMovement.x, 1) == lastFrameXMovement) {
						GrenadeLauncherMoving.StopFeedbacks();
					}

					if (grenadeSideSoundSensitivityInternal < 0) {
						grenadeSideSoundSensitivityInternal = grenadeSideSoundSensitivity;
						lastFrameXMovement = System.Math.Round(_inputManager.SecondaryMovement.x, 1);
					}
					else {
						grenadeSideSoundSensitivityInternal -= Time.deltaTime;
					}

					framesHeldDown++;
				}
				else if ((_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonUp) && framesHeldDown < grenadeSensitivity) {
					grenadeButtonTapped = false;
					_aimableWeapon.SetCurrentAim(Vector2.right);
					ShootStart();
					grenadeFiredLastFrame = true;
					framesHeldDown = 0;
					GrenadeLauncherMoving.StopFeedbacks();
					GrenadeLauncherStopMoving.PlayFeedbacks();
				}
				else {
					grenadeButtonTapped = false;
					grenadeFiredLastFrame = false;
					framesHeldDown = 0;
					GrenadeLauncherMoving.StopFeedbacks();
					GrenadeLauncherStopMoving.PlayFeedbacks();
				}
			}

			if ((_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonUp) && _inputManager.SecondaryMovement.magnitude > 0.5 && !grenadeFiredLastFrame) {
				ShootStart();
				framesHeldDown = 0;
				grenadeFiredLastFrame = true;
			}
			else if ((_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonDown) && !grenadeButtonTapped) {
				framesHeldDown = 0;
				grenadeButtonTapped = true;
				GrenadeLauncherStartMoving.PlayFeedbacks();
			}
			else if (_inputManager.SecondaryMovement.magnitude == 0) {
				grenadeFiredLastFrame = false;
			}
		} else {
			Vector3 newVector = Camera.main.ScreenToWorldPoint(new Vector3(_inputManager.SecondaryMovementMouse.x, _inputManager.SecondaryMovementMouse.y, -Camera.main.gameObject.transform.position.z)) - CurrentWeapon.gameObject.transform.position;
			_aimableWeapon.SetCurrentAim(newVector);

			if (_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				grenadeButtonTapped = false;
				ShootStart();
				grenadeFiredLastFrame = true;
				framesHeldDown = 0;
				GrenadeLauncherMoving.StopFeedbacks();
				if (!GrenadeLauncherStopMoving.IsPlaying) {
					GrenadeLauncherStopMoving.PlayFeedbacks();
				}
			}
		}
	}

	public void handleSMAWeaponAttack() {
		if (_inputManager.SwitchSMAMode.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
			(CurrentWeapon as SMAWeapon).toggleWeapon();
			(CurrentWeapon as SMAWeapon).gunSwitchSound.PlayFeedbacks();

			//Have to turn the button off as soon as the weapon is switched. the new input system is SUPER sensitive and can sometimes get stuck in the "ButtonDown" state for multiple frames. 
			_inputManager.SwitchSMAMode.State.ChangeState(MMInput.ButtonStates.Off);
		}

		if (_inputManager.isGamepadEnabled) {
			if (_inputManager.SecondaryMovement.magnitude > 0.5 && !gunFiredLastFrame) {
				RaycastHit2D _lockInEnemyCheck = MMDebug.BoxCast(CurrentWeapon.gameObject.transform.position, new Vector2(2.5f, 2.5f), 0, _inputManager.SecondaryMovement, 10, enemiesLayerMask, Color.red, false);
				Vector2 enemyDirection;
				if (_lockInEnemyCheck) {
					enemyDirection = _lockInEnemyCheck.collider.transform.position - CurrentWeapon.gameObject.transform.position;
					_aimableWeapon.SetCurrentAim(enemyDirection);
					ShootStart();
				}
				else {
					Vector2 noAimDirection;
					if (_character.IsFacingRight) {
						noAimDirection = _inputManager.SecondaryMovement;
					}
					else {
						noAimDirection = -_inputManager.SecondaryMovement;
					}

					_aimableWeapon.SetCurrentAim(noAimDirection);
					ShootStart();
				}

				gunFiredLastFrame = true;
			}
			else if (_inputManager.SecondaryMovement.magnitude == 0) {
				gunFiredLastFrame = false;
			}

			magnitudeLastFrame = _inputManager.SecondaryMovement.magnitude;
		}
		else {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(_inputManager.SecondaryMovementMouse.x, _inputManager.SecondaryMovementMouse.y, 10f)) - CurrentWeapon.gameObject.transform.position;

			_aimableWeapon.SetCurrentAim(mousePosition);

			if (_inputManager.KeyboardMouseFireDrone.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				ShootStart();
			}
		}
	}

	public void handleDroneRangedWeaponAttack() {
		if (_inputManager.isGamepadEnabled) {
			if (!(_inputManager.DroneGrenadeAim.State.CurrentState == MMInput.ButtonStates.ButtonDown) && _inputManager.SecondaryMovement.magnitude > 0.5 && !gunFiredLastFrame && !grenadeRefScript.grenadeFiredLastFrame) {
				RaycastHit2D _lockInEnemyCheck = MMDebug.BoxCast(_character.gameObject.transform.position, new Vector2(2.5f, 2.5f), 0, _inputManager.SecondaryMovement, 10, enemiesLayerMask, Color.red, false);
				Vector2 enemyDirection;
				if (_lockInEnemyCheck) {
					if (_character.IsFacingRight) {
						enemyDirection = -(_character.gameObject.transform.position - _lockInEnemyCheck.collider.transform.position);
					}
					else {
						enemyDirection = (_character.gameObject.transform.position - _lockInEnemyCheck.collider.transform.position);
					}

					_aimableWeapon.SetCurrentAim(enemyDirection);
					ShootStart();
				}
				else {
					Vector2 noAimDirection;
					if (_character.IsFacingRight) {
						noAimDirection = _inputManager.SecondaryMovement;
					}
					else {
						noAimDirection = -_inputManager.SecondaryMovement;
					}

					_aimableWeapon.SetCurrentAim(noAimDirection);
					ShootStart();
				}

				gunFiredLastFrame = true;
			}
			else if (_inputManager.SecondaryMovement.magnitude == 0) {
				gunFiredLastFrame = false;
			}

			magnitudeLastFrame = _inputManager.SecondaryMovement.magnitude;
		} else {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(_inputManager.SecondaryMovementMouse.x, _inputManager.SecondaryMovementMouse.y, 10f)) - CurrentWeapon.gameObject.transform.position;

			_aimableWeapon.SetCurrentAim(mousePosition);

			if (_inputManager.KeyboardMouseFireDrone.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				ShootStart();
			}
		}
	}

	public override void ChangeWeapon(Weapon newWeapon, string weaponID, bool combo = false) {
		// if the character already has a weapon, we make it stop shooting
		if (CurrentWeapon != null) {
			if (!combo) {
				ShootStop();

				if (_character._animator != null) {
					AnimatorControllerParameter[] parameters = _character._animator.parameters;
					foreach (AnimatorControllerParameter parameter in parameters) {
						if (parameter.name == CurrentWeapon.EquippedAnimationParameter) {
							MMAnimatorExtensions.UpdateAnimatorBool(_animator, CurrentWeapon.EquippedAnimationParameter, false);
						}
					}
				}

				Destroy(CurrentWeapon.gameObject);
			}
		}

		if (newWeapon != null) {
			if (!combo) {
				CurrentWeapon = (Weapon)Instantiate(newWeapon, WeaponAttachment.transform.position + newWeapon.WeaponAttachmentOffset, Quaternion.identity);
			}
			CurrentWeapon.transform.SetParent(WeaponAttachment.transform);
			CurrentWeapon.SetOwner(_character, this);
			CurrentWeapon.WeaponID = weaponID;
			_aimableWeapon = CurrentWeapon.GetComponent<WeaponAim>();
			// we handle (optional) inverse kinematics (IK) 
			if (_weaponIK != null) {
				_weaponIK.SetHandles(CurrentWeapon.LeftHandHandle, CurrentWeapon.RightHandHandle);
			}
			// we turn off the gun's emitters.
			CurrentWeapon.Initialization();
			CurrentWeapon.InitializeComboWeapons();
			CurrentWeapon.InitializeAnimatorParameters();
			InitializeAnimatorParameters();
			if ((_character != null) && !combo) {
				if (!_character.IsFacingRight) {
					if (CurrentWeapon != null) {
						CurrentWeapon.FlipWeapon();
						CurrentWeapon.FlipWeaponModel();
					}
				}
			}
		}
		else {
			CurrentWeapon = null;
		}
	}
}
