﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketProjectile : Projectile {

	/// the turn speed, the projectile will slowly turn towards the player instead of locking directly on them. 
	public float turnSpeed = 0;

	private GameObject bossObject;
	private GameObject playerObject;

	private Vector2 oldDirection;

	private void Start() {
		playerObject = GameObject.FindGameObjectWithTag("Player");
		bossObject = GameObject.FindGameObjectWithTag("Boss");

		oldDirection = playerObject.transform.position - this.transform.position;
	}

	public override void Movement() {
		float turnSpeedWithSpeed = turnSpeed - ((Speed / 1000f) * 1.5f);

		Vector2 newDirection = Vector2.MoveTowards(oldDirection, playerObject.transform.position - this.transform.position, turnSpeedWithSpeed);
		Vector2 modifiedDirectionForTurnSpeed = Vector2.MoveTowards(oldDirection, newDirection, turnSpeedWithSpeed);

		Quaternion newRotation = Quaternion.Euler(0, 0, Mathf.Atan2(modifiedDirectionForTurnSpeed.y, modifiedDirectionForTurnSpeed.x) * Mathf.Rad2Deg);

		SetDirection(modifiedDirectionForTurnSpeed.normalized, newRotation);
		base.Movement();

		oldDirection = modifiedDirectionForTurnSpeed;
	}
}
