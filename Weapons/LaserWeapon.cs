﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeapon : Weapon 
{
	[Header("Laser Weapon Specific Settings")]
	/// the number of projectiles to spawn per shot
	public float timeForLockOn = 0.5f;
	public float timeForFire = 0.5f;
	public int damage = 2;
	public MMObjectPooler objectPooler { get; set; }

	public Vector2 LaserWidth = new Vector2(0.05f, 0.05f);
	public Material LaserMaterial;

	public Vector2 LightingWidth = new Vector2(0.05f, 0.05f);
	public Material LightingMaterial;

	public enum LaserWeaponState { WeaponIdle, WeaponStart, WeaponAim, WeaponPrepareToFire, WeaponUse, WeaponDelayBetweenUses, WeaponStop, WeaponReloadNeeded, WeaponReloadStart, WeaponReload, WeaponReloadStop, WeaponInterrupted }
	public MMStateMachine<LaserWeaponState> laserWeaponState;

	public GameObject lightingStrikeObject;
	public LayerMask wallLayerMask;
	public LayerMask targetLayerMask;

	public int laserTrackNumber = 4;

	public bool isNonProjectileBasedWeapon = true;

	protected LineRenderer _line;
	protected LineRenderer lightingStrikeLine;
	protected bool poolInitialized = false;
	protected float timeForLockOnInternal;
	protected float timeForFireInternal;

	private GameObject bossObject;
	private GameObject playerObject;

	Vector2 origin = new Vector2();
	Vector2 destination = new Vector2();

	/// <summary>
	/// Initialize this weapon
	/// </summary>
	public override void Initialization() {
		base.Initialization();
		_aimableWeapon = GetComponent<WeaponAim>();

		if (!poolInitialized && !isNonProjectileBasedWeapon) {
			if (GetComponent<MMMultipleObjectPooler>() != null) {
				objectPooler = GetComponent<MMMultipleObjectPooler>();
			}
			if (GetComponent<MMSimpleObjectPooler>() != null) {
				objectPooler = GetComponent<MMSimpleObjectPooler>();
			}
			if (objectPooler == null) {
				Debug.LogWarning(this.name + " : no object pooler (simple or multiple) is attached to this Projectile Weapon, it won't be able to shoot anything.");
				return;
			}
			poolInitialized = true;
		}

		_line = gameObject.AddComponent<LineRenderer>();
		_line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		_line.receiveShadows = true;
		_line.startWidth = LaserWidth.x;
		_line.endWidth = LaserWidth.y;
		_line.material = LaserMaterial;

		lightingStrikeLine = lightingStrikeObject.AddComponent<LineRenderer>();
		lightingStrikeLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		lightingStrikeLine.receiveShadows = true;
		lightingStrikeLine.startWidth = LightingWidth.x;
		lightingStrikeLine.endWidth = LightingWidth.y;
		lightingStrikeLine.material = LightingMaterial;

		playerObject = GameObject.FindGameObjectWithTag("Player");
		bossObject = GameObject.FindGameObjectWithTag("Boss");

		timeForLockOnInternal = timeForLockOn;
		timeForFireInternal = timeForFire;

		laserWeaponState = new MMStateMachine<LaserWeaponState>(gameObject, true);
		laserWeaponState.ChangeState(LaserWeaponState.WeaponIdle);
	}

	/// <summary>
	/// Called everytime the weapon is used
	/// </summary>
	protected override void WeaponUse() {
		base.WeaponUse();

		RaycastHit2D _raycast;
		Vector2 _rayCastOrigin = new Vector2(origin.x, origin.y);

		Vector2 _playerOrigin = bossObject.transform.InverseTransformPoint(destination).normalized;

		_raycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, 100, wallLayerMask, Color.blue, false);

		lightingStrikeLine.SetPosition(0, origin);
		lightingStrikeLine.SetPosition(1, _raycast.point);

		Vector2 raycastHitDirection = bossObject.transform.InverseTransformPoint(_raycast.point).normalized;

		RaycastHit2D _raycastHit;
		_raycastHit = MMDebug.RayCast(origin, raycastHitDirection, 100, targetLayerMask, Color.blue, true);

		if (_raycastHit) {
			playerObject.GetComponent<BurningSkyHealth>().Damage(damage, gameObject, 0, 0);
		}
	}

	protected override void CaseWeaponUse() {
		WeaponUse();
		_delayBetweenUsesCounter = TimeBetweenUses;
		laserWeaponState.ChangeState(LaserWeaponState.WeaponDelayBetweenUses);
	}

	public override void TurnWeaponOff() {
		if (_characterHorizontalMovement != null) {
			_characterHorizontalMovement.MovementSpeedMultiplier = _permanentMovementMultiplierStorage;
		}

		if ((laserWeaponState.CurrentState == LaserWeaponState.WeaponIdle || laserWeaponState.CurrentState == LaserWeaponState.WeaponStop)) {
			return;
		}
		_triggerReleased = true;
		TriggerWeaponStopFeedback();
		laserWeaponState.ChangeState(LaserWeaponState.WeaponStop);

		if (_comboWeapon != null) {
			_comboWeapon.WeaponStopped(this);
		}
		if (PreventAllMovementWhileInUse && (_characterHorizontalMovement != null)) {
			_characterHorizontalMovement.MovementForbidden = false;
		}
	}

	protected override void ProcessWeaponState() {
		if (WeaponState == null) { return; }
		if (laserWeaponState == null) { return; }

		switch (laserWeaponState.CurrentState) {
			case LaserWeaponState.WeaponIdle:
			CaseWeaponIdle();
			break;

			case LaserWeaponState.WeaponStart:
			CaseWeaponStart();
			break;

			case LaserWeaponState.WeaponAim:
			CaseWeaponAim();
			break;

			case LaserWeaponState.WeaponPrepareToFire:
			CaseWeaponPrepareToFire();
			break;

			case LaserWeaponState.WeaponUse:
			CaseWeaponUse();
			break;

			case LaserWeaponState.WeaponDelayBetweenUses:
			CaseWeaponDelayBetweenUses();
			break;

			case LaserWeaponState.WeaponStop:
			CaseWeaponStop();
			break;

			case LaserWeaponState.WeaponReloadNeeded:
			CaseWeaponReloadNeeded();
			break;

			case LaserWeaponState.WeaponReloadStart:
			CaseWeaponReloadStart();
			break;

			case LaserWeaponState.WeaponReload:
			CaseWeaponReload();
			break;

			case LaserWeaponState.WeaponReloadStop:
			CaseWeaponReloadStop();
			break;

			case LaserWeaponState.WeaponInterrupted:
			CaseWeaponInterrupted();
			break;
		}
	}

	protected override void CaseWeaponDelayBetweenUses() {
		_delayBetweenUsesCounter -= Time.deltaTime;
		if (_delayBetweenUsesCounter <= 0) {
			if ((TriggerMode == TriggerModes.Auto) && !_triggerReleased) {
				ShootRequest();
			}
			else {
				TurnWeaponOff();
			}

			lightingStrikeLine.SetPosition(0, new Vector3());
			lightingStrikeLine.SetPosition(1, new Vector3());
			
			_line.SetPosition(0, new Vector3());
			_line.SetPosition(1, new Vector3());
		}
	}

	protected override void CaseWeaponStop() {
		laserWeaponState.ChangeState(LaserWeaponState.WeaponIdle);

		lightingStrikeLine.SetPosition(0, new Vector3());
		lightingStrikeLine.SetPosition(1, new Vector3());

		_line.SetPosition(0, new Vector3());
		_line.SetPosition(1, new Vector3());
	}

	protected override void TurnWeaponOn() {
		base.TurnWeaponOn();
		laserWeaponState.ChangeState(LaserWeaponState.WeaponStart);
	}

	protected override void CaseWeaponReloadNeeded() {
		ReloadNeeded();
		ResetMovementMultiplier();
		laserWeaponState.ChangeState(LaserWeaponState.WeaponIdle);
	}

	protected override void CaseWeaponReloadStart() {
		ReloadWeapon();
		_reloadingCounter = ReloadTime;
		laserWeaponState.ChangeState(LaserWeaponState.WeaponReload);
	}

	protected override void CaseWeaponReload() {
		ResetMovementMultiplier();
		_reloadingCounter -= Time.deltaTime;
		if (_reloadingCounter <= 0) {
			laserWeaponState.ChangeState(LaserWeaponState.WeaponReloadStop);
		}
	}

	protected override void CaseWeaponReloadStop() {
		_reloading = false;
		laserWeaponState.ChangeState(LaserWeaponState.WeaponIdle);
		if (WeaponAmmo == null) {
			CurrentAmmoLoaded = MagazineSize;
		}
	}

	protected override void CaseWeaponInterrupted() {
		TurnWeaponOff();
		ResetMovementMultiplier();
		laserWeaponState.ChangeState(LaserWeaponState.WeaponIdle);
	}

	protected override void CaseWeaponStart() {
		if (DelayBeforeUse > 0) {
			_delayBeforeUseCounter = DelayBeforeUse;
			laserWeaponState.ChangeState(LaserWeaponState.WeaponAim);
		}
		else {
			ShootRequest();
		}
	}

	protected void CaseWeaponAim() {
		if (timeForLockOnInternal < 0) {
			laserWeaponState.ChangeState(LaserWeaponState.WeaponPrepareToFire);
			timeForLockOnInternal = timeForLockOn;
		}
		else {
			timeForLockOnInternal -= Time.deltaTime;

			origin = bossObject.transform.position;
			destination = playerObject.transform.position;

			RaycastHit2D _raycast;
			Vector2 _rayCastOrigin = new Vector2(origin.x, origin.y);

			Vector2 destiniationWithLead = destination + (playerObject.GetComponent<CorgiController>().Speed / laserTrackNumber);
			Vector2 _playerOrigin = bossObject.transform.InverseTransformPoint(destiniationWithLead).normalized;

			_raycast = MMDebug.RayCast(_rayCastOrigin, _playerOrigin, 100, wallLayerMask, Color.blue, false);

			_line.SetPosition(0, origin);
			_line.SetPosition(1, _raycast.point);

			destination = destiniationWithLead;
		}
	}

	protected void CaseWeaponPrepareToFire() {
		if (timeForFireInternal < 0) {
			laserWeaponState.ChangeState(LaserWeaponState.WeaponUse);
			timeForFireInternal = timeForFire;
		}
		else {
			timeForFireInternal -= Time.deltaTime;
		}
	}

	protected override void ShootRequest() {
		// if we have a weapon ammo component, we determine if we have enough ammunition to shoot
		if (_reloading) {
			return;
		}

		if (MagazineBased) {
			if (WeaponAmmo != null) {
				if (WeaponAmmo.EnoughAmmoToFire()) {
					laserWeaponState.ChangeState(LaserWeaponState.WeaponAim);
				}
				else {
					if (AutoReload && MagazineBased) {
						InitiateReloadWeapon();
					}
					else {
						laserWeaponState.ChangeState(LaserWeaponState.WeaponReloadNeeded);
					}
				}
			}
			else {
				if (CurrentAmmoLoaded > 0) {
					laserWeaponState.ChangeState(LaserWeaponState.WeaponAim);
					CurrentAmmoLoaded -= AmmoConsumedPerShot;
				}
				else {
					if (AutoReload) {
						InitiateReloadWeapon();
					}
					else {
						laserWeaponState.ChangeState(LaserWeaponState.WeaponReloadNeeded);
					}
				}
			}
		}
		else {
			if (WeaponAmmo != null) {
				if (WeaponAmmo.EnoughAmmoToFire()) {
					laserWeaponState.ChangeState(LaserWeaponState.WeaponAim);
				}
				else {
					laserWeaponState.ChangeState(LaserWeaponState.WeaponReloadNeeded);
				}
			}
			else {
				laserWeaponState.ChangeState(LaserWeaponState.WeaponAim);
			}
		}
	}
}
