﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyBounce : DamageOnTouch
{
    public BurningSkyMeleeWeapon meleeWeaponRef;
	public float manaRecovered;

	//A small adjustment either + or - to the mini stuns that occurs when the melee hits.
	public float miniStunAdjustment;

	/// The layers the agent will be blocked by, the grenade cannot hit past walls. 
	public LayerMask interactableLayers;
	/// The offset to apply to the shoot origin point (by default the position of the object)
	public Vector2 raycastOriginOffset = new Vector2(0, 0);
	/// The offset to apply to the target (by default the position of the object)
	public Vector2 raycastTargetOffset = new Vector2(0, 0);

	protected CorgiController _playerCorgiControllerRef;

    private void Start()
    {
        _playerCorgiControllerRef = GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<CorgiController>();
    }
    protected override void OnCollideWithDamageable(Health health)
    {
		Vector3 _rayCastOrigin = new Vector3(transform.position.x + raycastOriginOffset.x, transform.position.y + raycastOriginOffset.y, 0);
		Vector3 _targetOrigin = new Vector3(health.gameObject.transform.position.x + raycastTargetOffset.x, health.gameObject.transform.position.y + raycastTargetOffset.y, 0);

		Vector2 _rayCastDestination = (_targetOrigin - _rayCastOrigin).normalized;

		RaycastHit2D blockingRaycast = MMDebug.RayCast(transform.position, _rayCastDestination, GetComponent<BoxCollider2D>().size.x, interactableLayers, Color.red, true);

		//We check if the target is being blocked by a wall or not. If they are, we do nothing and exit;
		if (blockingRaycast && blockingRaycast.collider.gameObject.layer != LayerMask.NameToLayer("Enemies")) {
			return;
		}

		_colliderCorgiController = health.gameObject.MMGetComponentNoAlloc<CorgiController>();

		if ((_colliderCorgiController != null) && (!_colliderHealth.TemporaryInvulnerable) && (!_colliderHealth.Invulnerable) && (!_colliderHealth.ImmuneToKnockback)) {
			GameObject.FindGameObjectWithTag("Player").GetComponent<ManaSystem>().manaRecovered(manaRecovered);
            GameObject.FindGameObjectWithTag("Player").GetComponent<ComboManager>().addAttackToCombo(meleeWeaponRef.attackType, health.gameObject);
        }

		StatusEffectEnemy statusEffectEnemy = health.gameObject.GetComponent<StatusEffectEnemy>();

		if (statusEffectEnemy != null) {
			StatusEffectStun stun = new StatusEffectStun();
			stun.statusEffectInitialize(health.gameObject, StatusEffect.StatusEffectId.Stun, meleeWeaponRef.TimeBetweenUses - miniStunAdjustment, 1, 0, null);

			if (health.gameObject.GetComponent<CharacterType>() != null && health.gameObject.GetComponent<CharacterType>().isCharacterMelee()) {
				stun.statusEffectMeleeInitialize(health.GetComponent<DamageOnTouchBrusier>());
			}

			statusEffectEnemy.addNewStatusEffect(stun);
		}

		base.OnCollideWithDamageable(health);
        if(meleeWeaponRef.AreaOffset.y < -0.1) {
            _playerCorgiControllerRef.SetVerticalForce(Mathf.Sqrt(2f * meleeWeaponRef.bounceHeight * Mathf.Abs(_playerCorgiControllerRef.Parameters.Gravity)));
        }
	}
}