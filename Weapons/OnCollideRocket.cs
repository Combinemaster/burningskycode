﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollideRocket : MonoBehaviour
{
	public GameObject rocketModel;

	[Header("Targets")]
	[Information("This component will make your object cause damage to objects that collide with it. Here you can define what layers will be affected by the damage (for a standard enemy, choose Player), how much damage to give, and how much force should be applied to the object that gets the damage on hit. You can also specify how long the post-hit invincibility should last (in seconds).", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	// the layers that will be damaged by this object
	public LayerMask TargetLayerMask;

	[Header("Explosion Information")]
	[Information("This part of the script stores information about how long the resulting explosion should last and a reference to where the explosion will occur", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	public Collider2D explosiveAreaGameObject;
	public float explosionDuration;
	/// the MMFeedbacks to trigger on explosion
	public MMFeedbacks ExplosionFeedback;

	protected CorgiController _corgiController;
	protected Health _health;
	protected Color _gizmosColor;
	protected BoxCollider2D _boxCollider2D;
	protected SpriteRenderer _renderer;
	protected bool _damageAreaActive;
	protected float explosionDurationInternal;

	/// <summary>
	/// Initialization
	/// </summary>
	protected virtual void Awake() {
		_health = GetComponent<Health>();
		_corgiController = GetComponent<CorgiController>();
		_boxCollider2D = GetComponent<BoxCollider2D>();
		_gizmosColor = Color.red;
		_gizmosColor.a = 0.25f;
		explosionDurationInternal = explosionDuration;
	}

	/// <summary>
	/// On enable we initialize our bomb
	/// </summary>
	protected virtual void OnEnable() {
		Initialization();
	}
	/// <summary>
	/// Grabs renderer and pool components
	/// </summary>
	protected virtual void Initialization() {
		if (explosiveAreaGameObject == null) {
			Debug.LogWarning("There's no damage area associated to this rocket : " + this.name + ". You should set one via its inspector.");
			return;
		}
		explosiveAreaGameObject.isTrigger = true;
		DisableDamageArea();

		_renderer = rocketModel.MMGetComponentNoAlloc<SpriteRenderer>();
		_damageAreaActive = false;
	}

	/// <summary>
	/// On Update we handle our cooldowns and activate the bomb if needed
	/// </summary>
	protected virtual void Update() {
		if (_damageAreaActive && explosionDurationInternal <= 0) {
			Destroy();
		} else if (_damageAreaActive) {
			explosionDurationInternal -= Time.deltaTime;
		}
	}


	/// <summary>
	/// On destroy we disable our object
	/// </summary>
	protected virtual void Destroy() {
		_renderer.enabled = true;
		explosionDurationInternal = explosionDuration;
		gameObject.MMGetComponentNoAlloc<Projectile>().enabled = true;
		_health.Damage(_health.CurrentHealth, gameObject, 0f, 0);
	}

	/// <summary>
	/// When a collision with the player is triggered, we give damage to the player and knock it back
	/// </summary>
	/// <param name="collider">what's colliding with the object.</param>
	public virtual void OnTriggerStay2D(Collider2D collider) {
		Colliding(collider);
	}

	public virtual void OnTriggerEnter2D(Collider2D collider) {
		Colliding(collider);
	}

	protected virtual void Colliding(Collider2D collider) {
		if (!this.isActiveAndEnabled) {
			return;
		}

		// if what we're colliding with isn't part of the target layers, we do nothing and exit
		if (!MMLayers.LayerInLayerMask(collider.gameObject.layer, TargetLayerMask)) {
			return;
		}

		// activate damage area
		if (!_damageAreaActive) {
			EnableDamageArea();
			_renderer.enabled = false;
			gameObject.MMGetComponentNoAlloc<Projectile>().enabled = false;
			ExplosionFeedback?.PlayFeedbacks();
			_damageAreaActive = true;
		}
	}

	/// <summary>
	/// Enables the damage area.
	/// </summary>
	protected virtual void EnableDamageArea() {
		explosiveAreaGameObject.enabled = true;
	}

	/// <summary>
	/// Disables the damage area.
	/// </summary>
	protected virtual void DisableDamageArea() {
		explosiveAreaGameObject.enabled = false;
	}
}
