﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SMAWeapon : BurningSkyProjectileWeapon
{
	[Header("Assault Drone Attributes")]
	public bool isRocketLauncherSelected = false;
	public string weaponNameGun;
	public string weaponNameRocket;
	public int rocketManaCost;

	public MMObjectPooler ObjectPoolerGun;
	public MMObjectPooler ObjectPoolerRocket;

	/// the MMFeedbacks to trigger on gun switch
	public MMFeedbacks gunSwitchSound;

	private Text weaponNameUI;

	/// <summary>
	/// Initialize this weapon, attaching 2 object poolers to the gun. One being the rocket, one being the gun.
	/// </summary>
	public override void Initialization() {
		base.Initialization();
		_aimableWeapon = GetComponent<WeaponAim>();

		if (!_poolInitialized) {
			_flippedProjectileSpawnOffset = ProjectileSpawnOffset;
			_flippedProjectileSpawnOffset.y = -_flippedProjectileSpawnOffset.y;
			_poolInitialized = true;
		}

		weaponNameUI = GameObject.FindGameObjectWithTag("WeaponNameUi").GetComponent<Text>();
	}

	protected override void ShootRequest() {
		if (isWeaponsClipRechargeable) {
			if (isRocketLauncherSelected) {
				if (manaSystemRef != null && manaSystemRef.abilityUsed(rocketManaCost)) {
					WeaponState.ChangeState(WeaponStates.WeaponUse);
				} else {
					WeaponState.ChangeState(WeaponStates.WeaponIdle);
				}
			} else {
				if (_reloading) {
					return;
				}

				if (MagazineBased) {
					if (WeaponAmmo != null) {
						if (WeaponAmmo.EnoughAmmoToFire()) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
						}
						else {
							WeaponState.ChangeState(WeaponStates.WeaponIdle);
						}
					}
					else {
						if (CurrentAmmoLoaded > 0) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
							CurrentAmmoLoaded -= AmmoConsumedPerShot;
							updateBulletUi();
						}
						else {
							WeaponState.ChangeState(WeaponStates.WeaponIdle);
						}
					}
				}
				else {
					if (WeaponAmmo != null) {
						if (WeaponAmmo.EnoughAmmoToFire()) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
						}
					}
					else {
						WeaponState.ChangeState(WeaponStates.WeaponUse);
					}
				}
			}
		} else {
			base.ShootRequest();
		}
	}

	public void toggleWeapon() {
		isRocketLauncherSelected = !isRocketLauncherSelected;
		if (isRocketLauncherSelected) {
			weaponNameUI.text = weaponNameRocket;
		} else {
			weaponNameUI.text = weaponNameGun;
		}
	}

	/// <summary>
	/// Spawns a new object and positions/resizes it
	/// </summary>
	public override GameObject SpawnProjectile(Vector3 spawnPosition, int projectileIndex, int totalProjectiles, bool triggerObjectActivation = true) {
		/// we get the next object in the pool and make sure it's not null

		GameObject nextGameObject;

		if (isRocketLauncherSelected) {
			nextGameObject = ObjectPoolerRocket.GetPooledGameObject();
		} else {
			nextGameObject = ObjectPoolerGun.GetPooledGameObject();
		}

		// mandatory checks
		if (nextGameObject == null) { return null; }
		if (nextGameObject.GetComponent<MMPoolableObject>() == null) {
			throw new Exception(gameObject.name + " is trying to spawn objects that don't have a PoolableObject component.");
		}
		// we position the object
		nextGameObject.transform.position = spawnPosition;
		// we set its direction

		Projectile projectile = nextGameObject.GetComponent<Projectile>();
		if (projectile != null) {
			projectile.SetWeapon(this);
			if (Owner != null) {
				projectile.SetOwner(Owner.gameObject);
			}
		}
		// we activate the object
		nextGameObject.gameObject.SetActive(true);


		if (projectile != null) {
			if (RandomSpread) {
				_randomSpreadDirection.x = UnityEngine.Random.Range(-Spread.x, Spread.x);
				_randomSpreadDirection.y = UnityEngine.Random.Range(-Spread.y, Spread.y);
				_randomSpreadDirection.z = UnityEngine.Random.Range(-Spread.z, Spread.z);
			}
			else {
				if (totalProjectiles > 1) {
					_randomSpreadDirection.x = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.x, Spread.x);
					_randomSpreadDirection.y = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.y, Spread.y);
					_randomSpreadDirection.z = MMMaths.Remap(projectileIndex, 0, totalProjectiles - 1, -Spread.z, Spread.z);
				}
				else {
					_randomSpreadDirection = Vector3.zero;
				}
			}

			Quaternion spread = Quaternion.Euler(_randomSpreadDirection);
			projectile.SetDirection(spread * transform.right * (Flipped ? -1 : 1), transform.rotation, Owner.IsFacingRight);
			if (RotateWeaponOnSpread) {
				this.transform.rotation = this.transform.rotation * spread;
			}
		}

		if (triggerObjectActivation) {
			if (nextGameObject.GetComponent<MMPoolableObject>() != null) {
				nextGameObject.GetComponent<MMPoolableObject>().TriggerOnSpawnComplete();
			}
		}

		return (nextGameObject);
	}
}
