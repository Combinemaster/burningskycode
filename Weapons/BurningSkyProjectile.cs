﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyProjectile : Projectile
{
	[Header("Custom Projectile Options")]
	public bool destroyObject = false;

	private Character playerCharacterReference;

	protected override void Initialization() {
		Speed = _initialSpeed;
		ProjectileIsFacingRight = _facingRightInitially;
		if (_spriteRenderer != null) { _spriteRenderer.flipX = _initialFlipX; }
		transform.localScale = _initialLocalScale;
		playerCharacterReference = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
	}

	public override void Destroy() {
		if (destroyObject) {
			playerCharacterReference.UnFreeze();
			Destroy(gameObject);
		} else {
			base.Destroy();
		}
	}

	public GameObject getOwner() {
		return _owner;
	}
}
