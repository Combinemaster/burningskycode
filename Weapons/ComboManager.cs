﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BurningSkyCharacterHandleWeapon;

public class ComboManager : MonoBehaviour {
	[Header("Vinestrike + Heavy values")]
	public Vector2 KnockbackStrength = new Vector2(30, 5);

	[Header("Root Spear + Heavy values")]
	public float KnockbackStrengthUpperCut = 5f;
	public float stunTime = 1f;
	public Sprite stunImage;

	[Header("Vine strike + Claw values")]
	public float swipeDashStrength = 5f;
	public float bleedTime = 2;
	public float bleedTickTime = 1;
	public int bleedDamage = 1;
	public float flickerDuration;
	public float invincibilityDuration;
	public Sprite bleedImage;
	public float invincibilityFramesVineStrike;

	[Header("Root Spear + Claw values")]
	public float swipeDashStrengthRootSpear = 5f;
	public float bleedTimeRootSpear = 2;
	public float bleedTickTimeRootSpear = 1;
	public int bleedDamageRootSpear = 1;
	public float flickerDurationRootSpear;
	public float invincibilityDurationRootSpear;
	public Sprite bleedImageRootSpear;
	public float invincibilityFramesRootSpear;

	[Header("Light + Light + Heavy values")]
	public Vector2 LightLightHeavyKnockbackStrength = new Vector2(30, 5);

	[Header("Combos")]
	public ComboArray[] listOfCombos;

	private bool attackInProgress;

	[System.Serializable]
	public class ComboArray {
		public comboEffects comboName;
		public AttacksPossible[] combos;
	}

	public class ComboInfo {
		public comboEffects comboName;
		public string comboText;
	}

	public float timeoutTime;

	private float timeoutTimeInternal;

	private ArrayList listOfAttacksDone;
	private ArrayList listOfCombosInternal;

	/// the possible attacks that can happen. 
	[System.Serializable]
	public enum AttacksPossible { VineStrike, RootSpear, Light, Heavy, Claw }

	/// <summary>
	///  the possible combo effects
	/// </summary>
	public enum comboEffects { VineStrikeHeavyKnockback, VineStrikeClawSwipe, RootSpearHeavyKnockup, RootSpearClawSwipe, LightLightHeavy }

	// Start is called before the first frame update
	void Start() {
		timeoutTimeInternal = timeoutTime;
		listOfAttacksDone = new ArrayList();
		listOfCombosInternal = new ArrayList();

		foreach (ComboArray combo in listOfCombos) {
			string comboText = "";

			foreach (AttacksPossible attack in combo.combos) {
				string attackText = attack.ToString();
				comboText += attack.ToString();
			}

			ComboInfo comboInfo = new ComboInfo();
			comboInfo.comboText = comboText;
			comboInfo.comboName = combo.comboName;

			listOfCombosInternal.Add(comboInfo);
		}

		gameObject.GetComponent<BurningSkyHealth>().OnDeath += disableAttackInProgress;
	}

	// Update is called once per frame
	void Update() {
		if (timeoutTimeInternal < 0) {
			clearCombo();
		} else {
			timeoutTimeInternal -= Time.deltaTime;
		}
	}

	public void enableAttackInProgress() {
		attackInProgress = true;
	}

	public void disableAttackInProgress() {
		attackInProgress = false;
	}

	public bool isAttackInProgress() {
		return attackInProgress;
	}

	public void startTimer() {
		timeoutTimeInternal = timeoutTime;
	}

	public void addAttackToCombo(AttacksPossible typeOfAttack, GameObject target) {
		listOfAttacksDone.Add(typeOfAttack);
		startTimer();

		string attacksDone = "";

		foreach (AttacksPossible attack in listOfAttacksDone) {
			attacksDone += attack.ToString();
		}

		foreach (ComboInfo combo in listOfCombosInternal) {
			if (combo.comboText == attacksDone) {
				switch (combo.comboName) {
					case comboEffects.VineStrikeHeavyKnockback:
					vineStrikeHeavyKnockback(target);
					break;

					case comboEffects.RootSpearHeavyKnockup:
					RootSpearHeavyKnockup(target);
					break;

					case comboEffects.VineStrikeClawSwipe:
					VineStrikeClawSwipe(target);
					break;

					case comboEffects.RootSpearClawSwipe:
					RootSpearClawSwipe(target);
					break;

					case comboEffects.LightLightHeavy:
					LightLightHeavy(target);
					break;

					default:
					//no-op!
					break;
				}

				clearCombo();
			}
		}
	}

	public void clearCombo() {
		listOfAttacksDone.Clear();
	}

	private void vineStrikeHeavyKnockback (GameObject target) {
		if (GetComponent<Character>().IsFacingRight) {
			target.GetComponent<CorgiController>().SetForce(KnockbackStrength);
		}
		else {
			Vector2 invertedKnockback = KnockbackStrength;
			invertedKnockback.x = -KnockbackStrength.x;
			target.GetComponent<CorgiController>().SetForce(invertedKnockback);
		}
	}

	private void RootSpearHeavyKnockup(GameObject target) {
		target.GetComponent<CorgiController>().AddVerticalForce(KnockbackStrengthUpperCut);

		StatusEffectEnemy statusEffectEnemy = target.gameObject.MMGetComponentNoAlloc<StatusEffectEnemy>();

		if (statusEffectEnemy != null) {
			StatusEffectStun stun = new StatusEffectStun();
			stun.statusEffectInitialize(target.gameObject, StatusEffect.StatusEffectId.Stun, stunTime, 1, 0, stunImage);

			if (target.gameObject.GetComponent<CharacterType>().isCharacterMelee()) {
				stun.statusEffectMeleeInitialize(target.gameObject.GetComponent<DamageOnTouchBrusier>());
			}

			statusEffectEnemy.addNewStatusEffect(stun);
		} else {
			Debug.Log("Status effect couldn't be applied! Either handle this case or add a StatusEffectEnemy script to the enemy!");
		}
	}

	private void VineStrikeClawSwipe(GameObject target) {
		StartCoroutine(playerInvincibility(invincibilityFramesVineStrike));

		if (GetComponent<Character>().IsFacingRight) {
			GetComponent<CorgiController>().SetHorizontalForce(swipeDashStrength);
		}
		else {
			GetComponent<CorgiController>().SetHorizontalForce(-swipeDashStrength);
		}

		StatusEffectEnemy statusEffectEnemy = target.gameObject.MMGetComponentNoAlloc<StatusEffectEnemy>();

		if (statusEffectEnemy != null) {
			StatusEffectBleed bleed = new StatusEffectBleed();
			bleed.statusEffectInitialize(target.gameObject, StatusEffect.StatusEffectId.Bleed, bleedTime, 1, bleedTickTime, bleedImage);
			bleed.bleedInitialize(bleedDamage, flickerDuration, invincibilityDuration);

			statusEffectEnemy.addNewStatusEffect(bleed);
		}
		else {
			Debug.Log("Status effect couldn't be applied! Either handle this case or add a StatusEffectEnemy script to the enemy!");
		}
	}

	private void RootSpearClawSwipe(GameObject target) {
		StartCoroutine(playerInvincibility(invincibilityFramesRootSpear));

		if (GetComponent<Character>().IsFacingRight) {
			GetComponent<CorgiController>().SetHorizontalForce(swipeDashStrengthRootSpear);
		}
		else {
			GetComponent<CorgiController>().SetHorizontalForce(-swipeDashStrengthRootSpear);
		}

		StatusEffectEnemy statusEffectEnemy = target.gameObject.MMGetComponentNoAlloc<StatusEffectEnemy>();

		if (statusEffectEnemy != null) {
			StatusEffectBleed bleed = new StatusEffectBleed();
			bleed.statusEffectInitialize(target.gameObject, StatusEffect.StatusEffectId.Bleed, bleedTimeRootSpear, 1, bleedTickTimeRootSpear, bleedImageRootSpear);
			bleed.bleedInitialize(bleedDamageRootSpear, flickerDurationRootSpear, invincibilityDurationRootSpear);

			statusEffectEnemy.addNewStatusEffect(bleed);
		}
		else {
			Debug.Log("Status effect couldn't be applied! Either handle this case or add a StatusEffectEnemy script to the enemy!");
		}
	}

	private void LightLightHeavy(GameObject target) {
		if (GetComponent<Character>().IsFacingRight) {
			target.GetComponent<CorgiController>().SetForce(LightLightHeavyKnockbackStrength);
		}
		else {
			Vector2 invertedKnockback = LightLightHeavyKnockbackStrength;
			invertedKnockback.x = -LightLightHeavyKnockbackStrength.x;
			target.GetComponent<CorgiController>().SetForce(invertedKnockback);
		}
	}

	private IEnumerator playerInvincibility(float frames) {
		BurningSkyHealth burningSkyHealth = GetComponent<BurningSkyHealth>();

		if (burningSkyHealth != null) {
			burningSkyHealth.TemporaryInvulnerable = true;
			yield return new WaitForSeconds(frames);
			burningSkyHealth.TemporaryInvulnerable = false;
		} else {
			yield return new WaitForSeconds(0);
		}
	}
}
