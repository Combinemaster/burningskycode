﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyWeaponAim : WeaponAim
{
	//Overrides the secondary movement input to fall back on the "script" variant of getting the current aim. Since PC relies on mouse to aim we have to have a seperate use case for it.
	//If we would use the normal secondary movement that the controller utilizes as the input for the mouse, it'll go crazy and shoot everywhere thanks to how we check for the flick. 
	public bool SecondaryMovementOverride;

	//Toggles the usage of the new/old script, primarily used to prevent the grenade launcher from bugging out when using this script
	public bool grenadeLauncher;

	private Vector2 oldDirection;
	private InputManager inputManager;

	protected override void Start() {
		inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().LinkedInputManager;
		base.Start();
	}

	protected override void DetermineWeaponRotation()
    {
		if (!grenadeLauncher) {
			if (_currentAim != Vector3.zero) {
				if (_direction != Vector3.zero) {
					Vector2 modifiedDirectionForTurnSpeed = Vector2.MoveTowards(oldDirection, _currentAim, WeaponRotationSpeed);

					Quaternion newRotation = Quaternion.Euler(0, 0, Mathf.Atan2(modifiedDirectionForTurnSpeed.y, modifiedDirectionForTurnSpeed.x) * Mathf.Rad2Deg);
					RotateWeapon(newRotation);

					oldDirection = modifiedDirectionForTurnSpeed;
				}
			}
			else {
				CurrentAngle = 0f;
				if (_characterGravity == null) {
					RotateWeapon(_initialRotation);
				}
				else {
					RotateWeapon(_characterGravity.transform.rotation);
				}
			}
		} else {
			base.DetermineWeaponRotation();
		}
	}

	/// <summary>
	/// Computes the current aim direction, slightly modified due to how the input is read for the grenade launcher. Keyboard is technically "script" controlled, and controller is business as usual. 
	/// </summary>
	protected override void GetCurrentAim() {
		if (!grenadeLauncher) {
			if (_weapon.Owner == null) {
				return;
			}

			if ((_weapon.Owner.LinkedInputManager == null) && (_weapon.Owner.CharacterType == Character.CharacterTypes.Player)) {
				return;
			}

			if (_weapon.Owner.LinkedInputManager != null && !_weapon.Owner.LinkedInputManager.isGamepadEnabled) {
				ScriptBasedAim();
			}
			else {
				base.GetCurrentAim();
			}
		} else {
			base.GetCurrentAim();
		}
	}

	/// <summary>
	/// Every frame, moves the reticle if it's been told to follow the pointer
	/// </summary>
	protected override void MoveReticle() {
		if (_reticle == null) { return; }

		// if we're not supposed to rotate the reticle, we force its rotation, otherwise we apply the current look rotation
		if (!RotateReticle) {
			_reticle.transform.rotation = Quaternion.identity;
		}
		else {
			if (ReticleAtMousePosition) {
				_reticle.transform.rotation = _lookRotation;
			}
		}

		// if we're in follow mouse mode, we move the reticle to the mouse's position, but only usable in PC mode, if the person is using a gamepad, we fall back to the old way. 
		if (ReticleAtMousePosition && !inputManager.isGamepadEnabled) {
			_mousePosition = inputManager.SecondaryMovementMouse;
			_mousePosition.z = 10;

			_reticle.transform.position = _mainCamera.ScreenToWorldPoint(_mousePosition);
		} else {
			_reticle.transform.localPosition = ReticleDistance * Vector3.right;
		}
	}

	private void ScriptBasedAim() {
		_currentAim = (_weapon.Owner.IsFacingRight) ? _currentAim : -_currentAim;
		_direction = -(transform.position - _currentAim);
	}
}
