﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class RootSpear : BurningSkySpell
{
    bool rootSpearTriggered;
    public GameObject spear;
	public Vector3 spearSpawnOffset;
	public Vector3 spearCollsionCheckOffset;
    private GameObject tempSpear;
    [Information("Here you can set how far the ability is from the player on the X axis", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
    public float distance;
	public float xOffset;
	public LayerMask wallLayerMask;
	public LayerMask targetLayerMask;
	public float rootSpearCooldown;

	private float rootSpearCooldownInternal;

	public override void ProcessAbility()
    {
		if (rootSpearCooldownInternal < 0) {
			if (rootSpearTriggered && tempSpear == null && manaSystemRef != null && manaSystemRef.abilityUsed(manaCost)) {
				tempSpear = Instantiate(spear, gameObject.transform.position + spearSpawnOffset, Quaternion.identity);
				Projectile tempProjectile = tempSpear.GetComponent<Projectile>();

				float tempProjectileXValue = 0;

				Vector2 directionOfRayCast = _character.IsFacingRight ? Vector2.right : Vector2.left;

				RaycastHit2D collisionCheckWall = MMDebug.RayCast(transform.position + spearCollsionCheckOffset, directionOfRayCast, distance, wallLayerMask, Color.cyan, true);
				RaycastHit2D collisionCheckEnemy = MMDebug.RayCast(transform.position + spearCollsionCheckOffset, directionOfRayCast, distance, targetLayerMask, Color.cyan, true);

				if (collisionCheckEnemy) {
					tempProjectileXValue = collisionCheckEnemy.collider.gameObject.transform.position.x;
				}
				else if (collisionCheckWall) {
					if (_character.IsFacingRight) {
						tempProjectileXValue = gameObject.transform.position.x + (collisionCheckWall ? (collisionCheckWall.distance - xOffset) : distance);
					}
					else {
						tempProjectileXValue = gameObject.transform.position.x - (collisionCheckWall ? (collisionCheckWall.distance - xOffset) : distance);
					}
				}
				else {
					if (_character.IsFacingRight) {
						tempProjectileXValue = gameObject.transform.position.x + distance;
					}
					else {
						tempProjectileXValue = gameObject.transform.position.x - distance;
					}
				}

				Vector3 locationOfRootStrike = new Vector3(tempProjectileXValue, gameObject.transform.position.y, gameObject.transform.position.z);
				RaycastHit2D raycast = MMDebug.RayCast(locationOfRootStrike, -transform.up, 100, _controller.PlatformMask | _controller.MovingPlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.cyan, true);

				tempProjectile.transform.position = raycast.point;
				tempProjectile.SetDirection(new Vector3(), Quaternion.Euler(0, 0, 90));
				rootSpearTriggered = false;

				PlayAbilityStartFeedbacks();

				rootSpearCooldownInternal = rootSpearCooldown;
			}
		} else {
			rootSpearCooldownInternal -= Time.deltaTime;
		}
    }

    protected override void HandleInput()
    {
		if (_inputManager.Spell1.State.CurrentState == MMInput.ButtonStates.ButtonDown && manaSystemRef.enoughManaToUseAbility(manaCost) && rootSpearCooldownInternal < 0 && AbilityPermitted) {
			rootSpearTriggered = true;
			_character.Freeze();
		}
		else if (_inputManager.Spell1.State.CurrentState == MMInput.ButtonStates.ButtonUp) {
			rootSpearTriggered = false;
		}
    }
}