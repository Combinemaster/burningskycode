﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BurningSkyProjectileWeapon : ProjectileWeapon {
	[Header("Custom Weapon Attributes")]
	public string weaponName;
	public AudioClip weaponSwitchSound;
	public bool isWeaponsClipRechargeable;
	public float rechargeTimePerShot;
	public int manaCost;
	public bool useMana = false;

	[Header("UI Images")]
	public Sprite fullBulletImage;
	public Sprite emptyBulletImage;

	protected ManaSystem manaSystemRef;
	protected float rechargeTimePerShotInternal;
	private Image fullBulletImage1;
	private Image fullBulletImage2;

	private void Start() {
		manaSystemRef = Owner.gameObject.GetComponent<ManaSystem>();
		rechargeTimePerShotInternal = rechargeTimePerShot;
		fullBulletImage1 = GameObject.FindGameObjectWithTag("FullBulletSlot1").GetComponent<Image>();
		fullBulletImage2 = GameObject.FindGameObjectWithTag("FullBulletSlot2").GetComponent<Image>();
	}

	protected override void Update() {
		base.Update();
		if (isWeaponsClipRechargeable) {
			if (rechargeTimePerShotInternal < 0 && CurrentAmmoLoaded < MagazineSize) {
				rechargeTimePerShotInternal = rechargeTimePerShot;
				CurrentAmmoLoaded++;
				updateBulletUi();
			} else if (CurrentAmmoLoaded == MagazineSize) {
				rechargeTimePerShotInternal = rechargeTimePerShot;
			} else {
				rechargeTimePerShotInternal -= Time.deltaTime;
			}
		}
	}

	public virtual void InitiateReloadWeaponNoSound() {
		// if we're already reloading, we do nothing and exit
		if (_reloading) {
			return;
		}
		WeaponState.ChangeState(WeaponStates.WeaponReload);
		_reloading = true;
	}

	public void updateCurrentAmmoCount(int currentAmmo) {
		CurrentAmmoLoaded = currentAmmo;
	}

	protected override void CaseWeaponReloadNeeded() {
		if (!isWeaponsClipRechargeable) {
			base.CaseWeaponReloadNeeded();
		}
	}

	protected override void CaseWeaponReloadStart() {
		if (!isWeaponsClipRechargeable) {
			base.CaseWeaponReloadStart();
		}
	}

	protected override void CaseWeaponReload() {
		if (!isWeaponsClipRechargeable) {
			base.CaseWeaponReload();
		}
	}

	protected override void CaseWeaponReloadStop() {
		if (!isWeaponsClipRechargeable) {
			base.CaseWeaponReloadStop();
		}
	}

	/// <summary>
	/// Determines whether or not the weapon can fire
	/// </summary>
	protected override void ShootRequest() {
		if (isWeaponsClipRechargeable) {
			if ((!useMana) || (manaSystemRef != null && manaSystemRef.abilityUsed(manaCost))) {
				if (_reloading) {
					return;
				}

				if (MagazineBased) {
					if (WeaponAmmo != null) {
						if (WeaponAmmo.EnoughAmmoToFire()) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
						}
						else {
							WeaponState.ChangeState(WeaponStates.WeaponIdle);
						}
					}
					else {
						if (CurrentAmmoLoaded > 0) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
							CurrentAmmoLoaded -= AmmoConsumedPerShot;
						}
						else {
							WeaponState.ChangeState(WeaponStates.WeaponIdle);
						}
					}
				}
				else {
					if (WeaponAmmo != null) {
						if (WeaponAmmo.EnoughAmmoToFire()) {
							WeaponState.ChangeState(WeaponStates.WeaponUse);
						}
					}
					else {
						WeaponState.ChangeState(WeaponStates.WeaponUse);
					}
				}
			} else {
				WeaponState.ChangeState(WeaponStates.WeaponIdle);
			}
		} else {
			base.ShootRequest();
		}
	}

	protected void updateBulletUi() {
		if (CurrentAmmoLoaded == 2) {
			fullBulletImage1.sprite = fullBulletImage;
			fullBulletImage2.sprite = fullBulletImage;
		} else if (CurrentAmmoLoaded == 1) {
			fullBulletImage1.sprite = fullBulletImage;
			fullBulletImage2.sprite = emptyBulletImage;
		} else {
			fullBulletImage1.sprite = emptyBulletImage;
			fullBulletImage2.sprite = emptyBulletImage;
		}
	}
}
