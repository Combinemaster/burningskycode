﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumNode : MonoBehaviour
{
	Quaternion start;
	Quaternion end;

	[SerializeField, Range(0.0f, 360f)]
	private float angle = 90.0f;

	[SerializeField, Range(0.0f, 5.0f)]
	private float speed = 2f;

	[SerializeField, Range(0.0f, 10.0f)]
	private float startTime = 0.0f;

	private bool vineSwingActive = false;
	private Vector3 _direction;
	private float defaultAngleZero = 1.56f;
	private float defaultAngleMax = 3.2f;
	private Vector3 newRotationRaw;

	public GameObject nodeAttachPoint;

	private void Start() {
		start = pendulumRotation(angle);
		end = pendulumRotation(-angle);
	}

	private void FixedUpdate() {
		if (vineSwingActive) {
			startTime += Time.deltaTime;
			transform.rotation = Quaternion.Lerp(start, end, (Mathf.Sin(startTime * speed + Mathf.PI / 2) + 1.0f) / 2.0f);
		}
	}

	public void stopVineSwing() {
		vineSwingActive = false;
	}

	public void startVineSwing() {
		vineSwingActive = true;

		float currentAngleExtracted = newRotationRaw.x / 100;
		float defaultAngleZeroSpeed = defaultAngleZero / speed;
		float resultingStartTime = 0;

		if (newRotationRaw.y == 90) {
			if (newRotationRaw.x == 90) {
				resultingStartTime = defaultAngleMax / speed;
			} else {
				resultingStartTime = (defaultAngleZeroSpeed - (currentAngleExtracted / speed)) * 2;
			}
		} else if (newRotationRaw.y == 270) {
			if (newRotationRaw.x == 90) {
				resultingStartTime = 0;
			} else {
				resultingStartTime = defaultAngleZeroSpeed + (currentAngleExtracted / speed);
			}
		} 

		startTime = resultingStartTime;
	}

	public void alignVineWithPlayer() {
		_direction = (GameObject.FindGameObjectWithTag("Player").transform.position - transform.position);
		var newRotation = Quaternion.LookRotation(_direction).eulerAngles;

		//Adjusted by 90 due to the swinging pole for some reason starts at X = 90, anything else and the rotation becomes odd. 
		newRotation.x -= 90;
		newRotation.x = Mathf.Clamp(newRotation.x, -angle, angle);

		//we apply the rotation to track the user as they move
		newRotationRaw = newRotation;
		transform.eulerAngles = newRotation;

		//If the player hits the max angle on either side, we adjust the angle by 180 to prevent the vine swing node from appearing on the other side (aka a full rotation) 
		if (newRotation.x == 90) {
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -180);
		}
		else if (newRotation.x == -90) {
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 180);
		}
	}

	void resetTimer() {
		startTime = 0.0f;
	}

	Quaternion pendulumRotation(float angle) {
		Quaternion pendulumRotation = transform.rotation;
		float angleZ = pendulumRotation.eulerAngles.z + angle;

		if (angleZ > 180) {
			angleZ -= 360;
		} else if (angleZ < -180) {
			angleZ += 360;
		}

		pendulumRotation.eulerAngles = new Vector3(pendulumRotation.eulerAngles.x, pendulumRotation.eulerAngles.y, angleZ);
		return pendulumRotation;
	}

	public GameObject getAttachedNode() {
		return nodeAttachPoint;
	}
}
