﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityPickup : PickableItem
{
	/// The type of character that this enemy is.
	public enum AbilityType { WallClinging }

	/// storage for enemy type
	[Information("Here you can set the type of ability that the user will gain when picking up this item.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	public AbilityType abilityType = AbilityType.WallClinging;
}
