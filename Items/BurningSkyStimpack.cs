﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyStimpack : PickableItem
{
    public int healthToGive;
	public HealthVine healthVineRef;

	private void OnTriggerStay2D(Collider2D collision) {
		_pickingCollider = collision;
		PickItem();
	}

	protected override bool CheckIfPickable()
	{
		// if what's colliding with the coin ain't a characterBehavior, we do nothing and exit
		_character = _pickingCollider.GetComponent<Character>();
		if (_character == null)
		{
			return false;
		}
		if (_character.CharacterType != Character.CharacterTypes.Player)
		{
			return false;
		}
		if (_itemPicker != null)
		{
			if (!_itemPicker.Pickable())
			{
				return false;
			}
		}

		int maxHealth = _pickingCollider.GetComponent<Health>().MaximumHealth;
		if (_pickingCollider.GetComponent<Health>().CurrentHealth == maxHealth)
		{
			return false;
		}

		if (_character.LinkedInputManager.UseButton.State.CurrentState != MMInput.ButtonStates.ButtonDown) {
			return false;
		}

		return true;
	}

	protected override void Pick() {
		Health characterHealth = _pickingCollider.GetComponent<Health>();
		// else, we give health to the player
		characterHealth.GetHealth(healthToGive, gameObject);
		healthVineRef.itemGrabbed();
	}
}
