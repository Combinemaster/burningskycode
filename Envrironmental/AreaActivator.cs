﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Activator class. All assigned objects will be activated once the player passes the trigger. 
/// </summary>
public class AreaActivator : MonoBehaviour
{
	public GameObject[] objectsToActivate;

	public bool keepAttachedObjectsOn;

	private void Start() {
		if (!keepAttachedObjectsOn) {
			foreach (GameObject objectToActivate in objectsToActivate) {
				objectToActivate.SetActive(false);
			}
		}
	}

	protected void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == "Player") {
			foreach (GameObject objectToActivate in objectsToActivate) {
				if (objectToActivate.GetComponent<ResetGameObject>().respawned == true) {
					if (objectToActivate.GetComponent<ResetBarrier>() != null) {
						objectToActivate.GetComponent<ResetBarrier>().resetGameObject();
					}
					else if (objectToActivate.GetComponent<ResetGameObject>() != null && objectToActivate.GetComponent<ResetGameObject>().respawned == true) {
						objectToActivate.GetComponent<ResetGameObject>().respawned = false;
					}

					objectToActivate.SetActive(true);
				}
			}
		}
	}
}
