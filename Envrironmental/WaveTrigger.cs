﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveTrigger : MonoBehaviour
{
	public GameObject waveManagerUsed;
	public List<GameObject> activateObjects;

	private GenericWaveManager waveManagerUsedScriptRef;
	private bool isPlayerOnTrigger;
	private InputManager _inputManager;

	private void Start() {
		waveManagerUsedScriptRef = waveManagerUsed.GetComponent<GenericWaveManager>();
		_inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().LinkedInputManager;
	}

	private void Update()
	{
	    if (!isPlayerOnTrigger) 
		{
			return;
		}
		if (_inputManager.UseButton.State.CurrentState == MMInput.ButtonStates.ButtonDown && !waveManagerUsedScriptRef.wavesFinished()) 
		{
			foreach (GameObject activatedObject in activateObjects)
			{
				activatedObject.SetActive(true);
				GenericWaveManager waveManagerRef = activatedObject.GetComponent<GenericWaveManager>();
				if (waveManagerRef != null)
				{
					waveManagerRef.startWave();
				}
			}
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = false;
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = true;
		}
	}
}
