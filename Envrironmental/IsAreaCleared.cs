﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsAreaCleared : MonoBehaviour
{
	public GameObject[] objectsToTrack;
	public GameObject[] objectsToDeactivate;

	// Update is called once per frame
	void Update()
    {
		if (areTargetsDead()) {
			foreach (GameObject trackedObject in objectsToDeactivate) {
				trackedObject.SetActive(false);
			}
		} else {
			foreach (GameObject trackedObject in objectsToDeactivate) {
				trackedObject.SetActive(true);
			}
		}
    }

	public bool areTargetsDead() {
		foreach (GameObject trackedObject in objectsToTrack) {
			BurningSkyEnemyHealth burningSkyHealth = trackedObject.GetComponent<BurningSkyEnemyHealth>();

			if (burningSkyHealth != null && (burningSkyHealth.CurrentHealth > 0 || burningSkyHealth.gameObject.activeInHierarchy)) {
				return false;
			}
		}

		return true;
	}
}
