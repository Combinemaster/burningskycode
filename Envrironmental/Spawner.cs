﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace spawner {
	public class Spawner : MonoBehaviour {
		public bool directionLeft;

		private SpriteRenderer spriteRenderer;
		private List<GameObject> enemiesSpawned;

		[Header("Enemy type per wave")]
		[Information("Here you can set enemy type per wave", MoreMountains.Tools.InformationAttribute.InformationType.Info, true)]
		public List<GameObject> enemyTypePerWave;

		[Header("Enemies per wave")]
		[Information("Here you can set the number of enemies determine the number of waves", InformationAttribute.InformationType.Info, true)]
		public List<int> numberOfEnemiesPerWave;

		public void Start() {
			spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			enemiesSpawned = new List<GameObject>();
		}
		
		public List<GameObject> spawnWave(int wave) {
			spriteRenderer.enabled = true;
			int numberOfEnemies = numberOfEnemiesPerWave[wave];
			while (numberOfEnemies > 0) {
				GameObject enemy = Instantiate(enemyTypePerWave[wave], this.transform.position, this.transform.rotation);
				enemy.SetActive(false);
				if (directionLeft) {
					enemy.GetComponent<Character>().Flip();
				}
				enemiesSpawned.Add(enemy);
				numberOfEnemies--;
				// TODO: analyze if we can delay the spawn after the previous enemy is dead or if we can keep the state of the spawner to spawn a new enemiy
			}
			return enemiesSpawned;
		}

		public IEnumerator showEnemies() {
			yield return new WaitForSeconds(1);

			foreach(GameObject enemySpawned in enemiesSpawned) {
				enemySpawned.SetActive(true);
			}
			enemiesSpawned.Clear();
			spriteRenderer.enabled = false;
		}

		public bool hasEnemiesForWave(int currentWave) {
			return numberOfEnemiesPerWave[currentWave] != 0;
		}

		public int numberOfEnemiesLeft(int waveNumber) {
			int enemyCount = 0;
			for (int i = waveNumber; i < numberOfEnemiesPerWave.Count; i++) {
				enemyCount += numberOfEnemiesPerWave[i];
			}
			return enemyCount;
		}

		public void resetSpawner() {
			enemiesSpawned.Clear();
			spriteRenderer.enabled = false;
		}
	}

	public class CoroutineWithData {
		public Coroutine coroutine { get; private set; }
		public object result;
		private IEnumerator target;

		public CoroutineWithData(MonoBehaviour owner, IEnumerator target) {
			this.target = target;
			this.coroutine = owner.StartCoroutine(Run());
		}

		private IEnumerator Run() {
			while (target.MoveNext()) {
				result = target.Current;
				yield return result;
			}
		}
	}
}
