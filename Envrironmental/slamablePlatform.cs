﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slamablePlatform : Health
{
	//a simple override of the damage method, if the instigator's tag is the mid-air slam, it'll just instantly kill itself. 
	public override void Damage(int damage, GameObject instigator, float flickerDuration, float invincibilityDuration) {
		if (damage <= 0) {
			OnHitZero?.Invoke();
			return;
		}

		// if the object is invulnerable, we do nothing and exit
		if (TemporaryInvulnerable || Invulnerable) {
			OnHitZero?.Invoke();
			return;
		}

		// if we're already below zero, we do nothing and exit
		if ((CurrentHealth <= 0) && (InitialHealth != 0)) {
			return;
		}

		OnHit?.Invoke();

		// we prevent the character from colliding with Projectiles, Player and Enemies
		if (invincibilityDuration > 0) {
			DamageDisabled();
			StartCoroutine(DamageEnabled(invincibilityDuration));
		}

		// we play the damage feedback
		DamageFeedbacks?.PlayFeedbacks();

		if (FlickerSpriteOnHit) {
			// We make the character's sprite flicker
			if (_renderer != null) {
				StartCoroutine(MMImage.Flicker(_renderer, _initialColor, _flickerColor, 0.05f, flickerDuration));
			}
		}

		if (instigator.tag == "midAirSlam") {
			CurrentHealth = 0;
			Kill();
		}
	}
}
