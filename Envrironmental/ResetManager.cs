﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetManager : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    private LevelManager levelManagerRef;

	public void Start() {
		levelManagerRef = gameObject.GetComponent<LevelManager>();
	}

	public void OnMMEvent(CorgiEngineEvent eventType)
    {
        if (eventType.EventType == CorgiEngineEventTypes.PlayerDeath)
        {
            levelManagerRef.CurrentCheckPoint.gameObject.GetComponent<CheckPointBurningSky>().resetTrackedObjects();
        }
    }

    void OnEnable()
    {
        this.MMEventStartListening<CorgiEngineEvent>();
    }

    void OnDisable()
    {
        this.MMEventStopListening<CorgiEngineEvent>();
    }
}
