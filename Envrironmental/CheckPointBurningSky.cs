using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

/// <summary>
/// Checkpoint class. Will make the player respawn at this point if it dies.
/// </summary>
[RequireComponent(typeof(BoxCollider2D))]
[AddComponentMenu("Corgi Engine/Spawn/Checkpoint")]
public class CheckPointBurningSky : CheckPoint 
{
	[Information("Populate this array with just area activators. The reason for this is so that when the player fully dies and respawns at the last bonfire/spawn area, it will reset ALL the areas that the smaller area activators control.", InformationAttribute.InformationType.Info, false)]
	public GameObject[] trackedAreaActivators;

	[Header("Music")]
	public AudioSource musicForArea;

	private SoundManager soundManagerRef;

	private void Start() {
		soundManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundManager>();
	}

	protected override void OnTriggerEnter2D(Collider2D collision) {
		base.OnTriggerEnter2D(collision);
		if (musicForArea != null && collision.gameObject.tag == "Player") {
			if (!musicForArea.isPlaying) {
				soundManagerRef.PlayBackgroundMusic(musicForArea);
			}
		}
	}

	public void resetTrackedObjects()
    {
		//This used to just reset individual objects but now resets area activators and all the objects within them. 
		foreach (GameObject gameObject in trackedAreaActivators)
        {
			AreaActivator areaActivator = gameObject.GetComponent<AreaActivator>();

			if (areaActivator != null) {
				foreach (GameObject resettableObject in areaActivator.objectsToActivate) {
					ResetGameObject resetRef = resettableObject.GetComponent<ResetGameObject>();
					if (resetRef != null) {
						resetRef.resetGameObject();
						resetRef.respawned = true;
						resettableObject.SetActive(false);
					}
				}
			} else {
				Debug.Log("Do not put non areaActivator objects in the checkpoint!");
			}
		}
	}
}