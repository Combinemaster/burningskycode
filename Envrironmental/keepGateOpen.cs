﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keepGateOpen : MonoBehaviour
{
	public GameObject gate;

    // Update is called once per frame
    void Update()
    {
		gate.SetActive(false);
	}
}
