﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeTree : MonoBehaviour, ResetGameObject {
	public int startingHealthStored;
	public float recoveryRate;

	[Header("Feedbacks")]
	/// the feedback to play when the recharge tree is used.
	public MMFeedbacks rechargeTreeUsedFeedback;

	[ReadOnly]
	public int currentHealthStored;

	private bool isPlayerOnTrigger;
	private float recoveryRateInternal;
	private InputManager _inputManager;
	private BurningSkyHealth healthRef;

	bool ResetGameObject.respawned {
		get => true; set {
			value = true;
		}
	}

	// Start is called before the first frame update
	void Start() {
		currentHealthStored = startingHealthStored;
		recoveryRateInternal = recoveryRate;

		_inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().LinkedInputManager;
		healthRef = GameObject.FindGameObjectWithTag("Player").GetComponent<BurningSkyHealth>();
	}

	private void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = false;
			rechargeTreeUsedFeedback.StopFeedbacks();
		}
	}

	private void OnTriggerStay2D(Collider2D collision) {
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = true;
		}
	}

	private void Update() {
		if (!isPlayerOnTrigger) {
			return;
		}

		if (_inputManager.UseButton.State.CurrentState == MMInput.ButtonStates.ButtonDown && healthRef != null && currentHealthStored > 0) {
			rechargeTreeUsedFeedback.PlayFeedbacks();

			if (recoveryRateInternal > 0) {
				recoveryRateInternal -= Time.deltaTime;
			}
			else {
				recoveryRateInternal = recoveryRate;
				if (healthRef.CurrentHealth < healthRef.MaximumHealth) {
					healthRef.SetHealth(healthRef.CurrentHealth + 1, gameObject);
					currentHealthStored -= 1;
				}
			}
		} else {
			rechargeTreeUsedFeedback.StopFeedbacks();
			recoveryRateInternal = recoveryRate;
		}
	}

	public void resetGameObject() {
		currentHealthStored = startingHealthStored;
	}
}
