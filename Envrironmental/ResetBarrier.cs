﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBarrier : MonoBehaviour, ResetGameObject
{
    public enum CheckpointsDefaultState { on, off }
    public CheckpointsDefaultState defaultState = CheckpointsDefaultState.on;

	bool ResetGameObject.respawned {
		get => true; set {
			value = true;
		}
	}

	public void resetGameObject() {
		if (defaultState == CheckpointsDefaultState.on) {
			gameObject.SetActive(true);
		}
		else {
			gameObject.SetActive(false);
		}
	}
}
