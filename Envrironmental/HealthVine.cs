﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthVine : MonoBehaviour
{
	public GameObject itemToDrop;
	public GameObject spawnPoint;
	
	private ResetHealthVine resetHealthVineRef;

	private void Start() {
		resetHealthVineRef = gameObject.GetComponent<ResetHealthVine>();
		dropItem();
	}

	public void dropItem() {
		if (!resetHealthVineRef.respawned) {
			GameObject healthPack = Instantiate(itemToDrop, spawnPoint.transform.position, spawnPoint.transform.rotation);
			healthPack.GetComponent<BurningSkyStimpack>().healthVineRef = this;
			resetHealthVineRef.respawned = true;
		}
	}

	public void itemGrabbed() {
		resetHealthVineRef.respawned = false;
	}
}
