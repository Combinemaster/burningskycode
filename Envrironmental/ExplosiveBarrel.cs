﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBarrel : MonoBehaviour
{
    public Vector2 explosionSize = new Vector2(5f, 5f);
    public float explosionRadius = 5f;
    public LayerMask targetLayerMask;
    public int explosionDamage = 10;
    public float invincibilityDuration = 0.5f;
    public MMFeedbacks explosionFeedbacks;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "PlayerBullet")
        {
            RaycastHit2D[] _raycast;
            Vector2 _objectOrigin = transform.position;
            _raycast = BurningSkyDebug.BoxCastAll(_objectOrigin, explosionSize, 0f, new Vector2(), explosionRadius, targetLayerMask, Color.red, true);
            if (_raycast.Length > 0)
            {
                foreach (RaycastHit2D hit in _raycast)
                {
                    Health healthScriptRef = hit.transform.gameObject.GetComponent<Health>();
                    if(healthScriptRef != null)
                    {
                        healthScriptRef.Damage(explosionDamage, gameObject, invincibilityDuration, invincibilityDuration);
                    }
                }
            }
            explosionFeedbacks.PlayFeedbacks();
            gameObject.SetActive(false);
        }
    }
}
