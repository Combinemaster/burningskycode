﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyButton : MonoBehaviour
{
	public List<GameObject> activateObjects;
	public List<GameObject> deactivateObjects;

	private bool isPlayerOnTrigger;
	private InputManager _inputManager;

	private void Start()
	{
		_inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().LinkedInputManager;
	}

	private void Update()
	{
		if (!isPlayerOnTrigger)
		{
			return;
		}
		if (_inputManager.UseButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
		{
			ButtonTrigger();
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			isPlayerOnTrigger = false;
		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Player"))
		{
			isPlayerOnTrigger = true;
		}
	}
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("VineStrike"))
		{
			ButtonTrigger();
		}
	}
	private void ButtonTrigger()
	{
		foreach (GameObject activatedObject in activateObjects)
		{
			activatedObject.SetActive(true);
		}
		foreach (GameObject deactivatedObject in deactivateObjects)
		{
			deactivatedObject.SetActive(false);
		}
	}
}
