﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BurningSky;
using spawner;
using MoreMountains.Feedbacks;

// Generic Wave Manager
public class GenericWaveManager : MonoBehaviour, MMEventListener<EnemyEvent>, ResetGameObject
{
    public int numberOfWaves;

    [Header("Spawners")]
    [Information("Each spawner will spawn the enemy from the current wave, You can put as much spawners as you want", MoreMountains.Tools.InformationAttribute.InformationType.Info, true)]
    public List<GameObject> spawners;
	public List<GameObject> waveEndedDeactivators;
    public List<GameObject> waveEndedActivators;
    public MMFeedbacks waveStartedFeedback;
	public bool continueNextWave = true;

	[Header("Music")]
	public AudioSource musicForEncounter;
	public AudioSource musicForArea;

	private List<GameObject> currentEnemies;
    private int currentWave = 0;
	private bool waveStarted = false;
	private SoundManager soundManagerRef;

	bool ResetGameObject.respawned {
		get => true; set {
			value = true;
		}
	}

	// Start is called before the first frame update
	void Start() 
    {
        if (waveStartedFeedback != null) {
            waveStartedFeedback.Initialization();
        }
		currentEnemies = new List<GameObject>();
		soundManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundManager>();
	}

    private void OnEnable()
    {
        this.MMEventStartListening<EnemyEvent>();
	}

    private void OnDisable()
    {
        this.MMEventStopListening<EnemyEvent>();
    }

	private IEnumerator spawnWave() {
		if ((currentWave < numberOfWaves) && continueNextWave) {
            if (waveStartedFeedback != null) {
                waveStartedFeedback.PlayFeedbacks();
            }
            foreach (GameObject spawner in spawners) {
				if (spawner.GetComponent<Spawner>().hasEnemiesForWave(currentWave)) {
                    currentEnemies.AddRange(spawner.GetComponent<Spawner>().spawnWave(currentWave));
					CoroutineWithData spawnEnemiesCoroutine = new CoroutineWithData(this, spawner.GetComponent<Spawner>().showEnemies());
					yield return spawnEnemiesCoroutine.coroutine;
				}
			}
		}
	}

	public void OnMMEvent(EnemyEvent eventType)
    {
       int position = getEnemyPosition(eventType.enemyId);
       if (position >= 0)
       {
            // Enemy dies, remove it from the current enemies list
             currentEnemies.RemoveAt(position);

			if (currentEnemies.Count == 0) {
				currentWave++;
				IEnumerator coroutine = spawnWave();
				StartCoroutine(coroutine);
			}

			if (currentWave == numberOfWaves && currentEnemies.Count == 0) {
				foreach (GameObject wavedEndedDeactivator in waveEndedDeactivators) {
					wavedEndedDeactivator.SetActive(false);
				}
                foreach (GameObject wavedEndedActivator in waveEndedActivators)
                {
                    wavedEndedActivator.SetActive(true);
                }
				if (musicForArea != null) {
					soundManagerRef.PlayBackgroundMusic(musicForArea);
				}
			}
		}
    }

    private int getEnemyPosition(int instanceId)
    {
        for (int i = 0; i < currentEnemies.Count; i++)
        {
            if (currentEnemies[i].GetInstanceID() == instanceId)
            {
                return i;
            }
        }

        return -1;
    }

	public bool wavesFinished() {
		return currentWave == numberOfWaves && currentEnemies.Count == 0;
	}

	public void startWave() {
        if (!waveStarted) {
            IEnumerator coroutine = spawnWave();
            StartCoroutine(coroutine);
        }
        waveStarted = true;
		if (musicForEncounter != null) {
			soundManagerRef.PlayBackgroundMusic(musicForEncounter);
		}
	}

	public void resetGameObject() {
		gameObject.SetActive(false);
		currentWave = 0;
		foreach (GameObject enemy in currentEnemies) {
			Destroy(enemy);
		}
		foreach (GameObject spawner in spawners) {
			spawner.GetComponent<Spawner>().resetSpawner();
		}
		currentEnemies.Clear();
		waveStarted = false;
		continueNextWave = true;
	}

	public int currentEnemiesLeft() {
		return currentEnemies.Count;
	}

	public int enemiesLeftInWaveManager() {
		int enemyCount = 0;
		if (currentWave < numberOfWaves) {
			for (int i = currentWave; i < spawners.Count; i++) {
				enemyCount += spawners[i].GetComponent<Spawner>().numberOfEnemiesLeft(currentWave + 1);
			}
		}
		return enemyCount;
	}
}
