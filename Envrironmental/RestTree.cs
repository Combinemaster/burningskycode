﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestTree : CheckPoint
{
	[Information("Populate this array with just area activators. The reason for this is so that when the player fully dies and respawns at the last bonfire/spawn area, it will reset ALL the areas that the smaller area activators control.", InformationAttribute.InformationType.Info, false)]
	public GameObject[] trackedAreaActivators;
	[Information("Populate this array with objects that you want to turn off after the player rests/respawns at a rest tree", InformationAttribute.InformationType.Info, false)]
	public GameObject[] turnOffObjects;
	[Information("Populate this array with objects that you want to turn on after the player rests/respawns at a rest tree", InformationAttribute.InformationType.Info, false)]
	public GameObject[] turnOnObjects;
	[Information("Populate this array with individual objects you'd want to reset. For example, health vines for an area.", InformationAttribute.InformationType.Info, false)]
	public GameObject[] trackedResettableObjects;

	[Header("Music")]
	public AudioSource musicForArea;

	private SoundManager soundManagerRef;
	private bool isPlayerOnTrigger;
	private InputManager _inputManager;

	[Header("Feedbacks")]
	/// the feedback to play when the rest tree is used.
	public MMFeedbacks restTreeUsedFeedback;

	private void Start() {
		soundManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundManager>();
		_inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().LinkedInputManager;

		BurningSkyHealth healthRef = GameObject.FindGameObjectWithTag("Player").GetComponent<BurningSkyHealth>();
		healthRef.OnDeath += resetTrackedObjects;
	}

	public override void SpawnPlayer(Character player) {
		player.RespawnAt(transform, FacingDirection);

		foreach (Respawnable listener in _listeners) {
			listener.OnPlayerRespawn(this, player);
		}
	}

	public override void AssignObjectToCheckPoint(Respawnable listener) {
		_listeners.Add(listener);
	}

	private void Update() {
		if (!isPlayerOnTrigger) {
			return;
		}
		if (_inputManager.UseButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
			if (restTreeUsedFeedback != null) {
				restTreeUsedFeedback.PlayFeedbacks();
			}

			resetTrackedObjects();

			foreach (GameObject objectsToTurnOff in turnOffObjects) {
				objectsToTurnOff.SetActive(false);
			}

			foreach (GameObject objectsToTurnOn in turnOnObjects) {
				objectsToTurnOn.SetActive(true);
			}

			Character character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();

			if (character == null) { return; }
			if (character.CharacterType != Character.CharacterTypes.Player) { return; }
			if (_reached) { return; }
			if (LevelManager.Instance == null) { return; }

			LevelManager.Instance.SetCurrentCheckpoint(this);

			if (musicForArea != null) {
				if (!musicForArea.isPlaying) {
					soundManagerRef.PlayBackgroundMusic(musicForArea);
				}
			}

			BurningSkyHealth burningSkyHealth = character.gameObject.GetComponent<BurningSkyHealth>();
	
			if (burningSkyHealth != null) {
				burningSkyHealth.ResetHealthToMaxHealth();
				burningSkyHealth.resetFeedbacks();
			}

			ManaSystem manaSystem = character.gameObject.GetComponent<ManaSystem>();

			if (manaSystem != null) {
				manaSystem.resetMana();
			}
		}
	}

	protected override void OnTriggerEnter2D(Collider2D collider) {
		//no-op, the original spawn system utilized checkpoints to save the users spawn position when they walked through it. Ours only gets saved when the player uses the rest tree.
	}

	private void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = false;
		}
	}

	private void OnTriggerStay2D(Collider2D collision) {
		if (collision.gameObject.CompareTag("Player")) {
			isPlayerOnTrigger = true;
		}
	}

	/// <summary>
	/// On DrawGizmos, we draw lines to show the path the object will follow
	/// </summary>
	protected override void OnDrawGizmos() {
	#if UNITY_EDITOR

		if (LevelManager.Instance == null) {
			return;
		}

		if (LevelManager.Instance.Checkpoints == null) {
			return;
		}

		if (LevelManager.Instance.Checkpoints.Count == 0) {
			return;
		}

		for (int i = 0; i < LevelManager.Instance.Checkpoints.Count; i++) {
			// we draw a line towards the next point in the path
			if ((i + 1) < LevelManager.Instance.Checkpoints.Count) {
				Gizmos.color = Color.green;
				Gizmos.DrawLine(LevelManager.Instance.Checkpoints[i].transform.position, LevelManager.Instance.Checkpoints[i + 1].transform.position);
			}
		}
	#endif
	}

	public void resetTrackedObjects() {
		//This used to just reset individual objects but now resets area activators and all the objects within them. 
		foreach (GameObject gameObject in trackedAreaActivators) {
			AreaActivator areaActivator = gameObject.GetComponent<AreaActivator>();

			if (areaActivator != null) {
				foreach (GameObject resettableObject in areaActivator.objectsToActivate) {
					ResetGameObject resetRef = resettableObject.GetComponent<ResetGameObject>();
					if (resetRef != null) {
						resetRef.resetGameObject();
						resetRef.respawned = true;
						resettableObject.SetActive(false);
					}
				}
			}
			else {
				Debug.Log("Do not put non areaActivator objects in the list!");
			}
		}

		foreach (GameObject objectsToTurnOff in turnOffObjects) {
			objectsToTurnOff.SetActive(false);
		}

		foreach (GameObject objectsToTurnOn in turnOnObjects) {
			objectsToTurnOn.SetActive(true);
		}

		foreach (GameObject resettableObject in trackedResettableObjects) {
			ResetGameObject resetRef = resettableObject.GetComponent<ResetGameObject>();
			if (resetRef != null) {
				resetRef.resetGameObject();
			}
		}
	}
}
