﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static BossManager;

public class BossTrigger : MonoBehaviour
{
	public GameObject bossManager;

	private BossManager bossManagerScriptRef;
	private bool isPlayerOnTrigger;

	private void Start() {
		bossManagerScriptRef = bossManager.GetComponent<BossManager>();
	}

	private void Update() {
		if (!isPlayerOnTrigger) {
			return;
		}
		if (Input.GetKeyUp(KeyCode.E)) {
			switch (bossManagerScriptRef.bossState.CurrentState) {
				case BossStates.Inactive:
				bossManagerScriptRef.bossState.ChangeState(BossStates.DestroyGenerator1);
				break;
			}
		}
	}

	private void OnTriggerExit2D(Collider2D collision) {
		isPlayerOnTrigger = !collision.gameObject.CompareTag("Player");
	}

	private void OnTriggerStay2D(Collider2D collision) {
		isPlayerOnTrigger = collision.gameObject.CompareTag("Player");
	}
}
