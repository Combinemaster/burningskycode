﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetHealthVine : MonoBehaviour, ResetGameObject
{
	private HealthVine healthVineRef;
	public bool respawned;

	bool ResetGameObject.respawned { get => respawned; set => respawned = value; }

	protected virtual void Start() {
		healthVineRef = gameObject.GetComponent<HealthVine>();
		respawned = false;
	}

	public void resetGameObject() {
		healthVineRef.dropItem();
	} 
}
