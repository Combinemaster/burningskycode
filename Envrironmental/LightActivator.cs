﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class LightActivator : MonoBehaviour
{
    public GameObject[] lights;
    public MMFeedbacks waveEndedFeedbacks;
    // Start is called before the first frame update
    void Start()
    {
		waveEndedFeedbacks.Initialization();
		waveEndedFeedbacks.PlayFeedbacks();
        foreach (GameObject light in lights){
            light.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.green;
        }
    }
}
