﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundChanger : MonoBehaviour
{
	public AudioSource[] audioSourcesToMute;
	public AudioSource[] audioSourcesToUnmute;

	// when the GameObjects collider arrange for this GameObject to travel to the left of the screen
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Player") {
			foreach (AudioSource sourceToMute in audioSourcesToMute) {
				sourceToMute.mute = true;
			}
			foreach (AudioSource sourceToUnmute in audioSourcesToUnmute) {
				sourceToUnmute.mute = false;
			}
		}
	}
}
