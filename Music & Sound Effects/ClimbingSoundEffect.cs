﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingSoundEffect : MonoBehaviour
{
	/// Feedbacks that are used for the climbing sound effect, we are using a single audio source instead of a entire feedback class because corgi does weird fucking shit
	/// with it's sounds that break it for some unknown reason. Loop-based sounds (Like the falling sound) should be a seperate system than the MMFeedbacks, one off ones should use MMFeedbacks
	public AudioSource audioSourceRef;
	public float startingVolume;
	public float endingVolume;
	public float volumeIncrement;
	public float waitTimeBeforeSound;

	private float intensity;
	private float waitTimeBeforeSoundInternal;

	private CorgiController corgiControllerRef;
	private BurningSkyHealth healthRef;
	private BurningSkyWallClinging burningSkyWallClingingRef;

	// Start is called before the first frame update
	void Start()
    {
		intensity = startingVolume;
		waitTimeBeforeSoundInternal = waitTimeBeforeSound;
		corgiControllerRef = gameObject.GetComponent<CorgiController>();
		healthRef = gameObject.GetComponent<BurningSkyHealth>();
		burningSkyWallClingingRef = gameObject.GetComponent<BurningSkyWallClinging>();

		updateAudioSource();
	}

	// Update is called once per frame
	void Update()
    {
        if (!corgiControllerRef.State.IsGrounded && 
			burningSkyWallClingingRef.getMovementState() != CharacterStates.MovementStates.WallClinging &&
			corgiControllerRef.Speed.y < 0 && 
			!corgiControllerRef.State.HasCollisions && 
			healthRef.CurrentHealth > 0) {

			if (waitTimeBeforeSoundInternal > 0) {
				waitTimeBeforeSoundInternal -= Time.deltaTime;
			}
			else {
				if (intensity >= endingVolume) {
					intensity = endingVolume;
				}
				else {
					intensity += volumeIncrement;
				}
			}
			updateAudioSource();
		} else {
			waitTimeBeforeSoundInternal = waitTimeBeforeSound;
			intensity = startingVolume;
			updateAudioSource();
		}
    }

	private void updateAudioSource() {
		audioSourceRef.volume = intensity;
	}

	public float getIntensity() {
		return intensity;
	}
}
