﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCombatMusic : MonoBehaviour, MMEventListener<MMGameEvent>
{
	//Enabled/Disabled level booleans go below
	//Filler layer, level 0 when the enemy sees a player
	public AudioSource[] fillerAudioSources;

	//First layer, level 1 when the enemy detects the player
	public AudioSource[] level1AudioSources;

	//second layer, level 2, triggers after the enemy detects the player and there is more than 1 enemy present (or one that has A LOT of threat)
	public AudioSource[] level2AudioSources;

	//third layer, level 3, triggers after the enemy detects the player and there is more than 1 enemy present (or one that has A LOT of threat)
	public AudioSource[] level3AudioSources;

	//Intensity level, controls how intense the music should be
	public int intensity = 0;

	//Controls what enemies to watch out, all enemies should be on the "Enemies" layer
	public LayerMask targetLayerMask;

	//How fast the volume should go up/down.
	public float volumeModificationSpeed;

	//How much should the volume increase/decrease per tick
	public float volumeModificationIncrement = 0.1f;

	//How long to wait before activiating the dynamic combat music upon seeing a enemy;
	public float activationTime = 0;
	
	private List<GameObject> enemies = new List<GameObject>();
	[ReadOnly]
	public const string eventPlayerDetectedId = "PlayerDetected";
	private bool playerHasBeenDetected = false;
	private float volumeModificationSpeedInternal;
	private float areaMusicVolume;

	private AudioSource fillerAudioSource;
	private AudioSource level1AudioSource;
	private AudioSource level2AudioSource;
	private AudioSource level3AudioSource;
	private AudioSource areaMusic;

	private List<AudioSource> audioSourcesToDeafen = new List<AudioSource>();
	private List<AudioSource> audioSourcesToTurnOn = new List<AudioSource>();

	private void OnTriggerEnter2D(Collider2D collision) {
		if (((1 << collision.gameObject.layer) & targetLayerMask) != 0) {
			enemies.Add(collision.gameObject);
			if (audioSourcesToDeafen.Contains(fillerAudioSource)) {
				audioSourcesToDeafen.Remove(fillerAudioSource);
			}

			fillerAudioSource = fillerAudioSources[Random.Range(0, fillerAudioSources.Length)];
			addAudioSourceToTurnOn(fillerAudioSource);

			CharacterType characterType = collision.gameObject.GetComponent<CharacterType>();
			if (characterType != null) {
				intensity += characterType.threatLevel;
				updateCombatMusic();
			} else {
				Debug.Log("Enemy does NOT have a character type script attached! Which means no updating the combat music");
			}

			areaMusic.volume = 0;
		}
	}

	private void OnTriggerExit2D(Collider2D collision) {
		if (((1 << collision.gameObject.layer) & targetLayerMask) != 0) {
			enemies.RemoveAt(enemies.IndexOf(collision.gameObject));
			if (enemies.Count == 0) {
				deafenMusic();
				playerHasBeenDetected = false;
				intensity = 0;
				areaMusic.volume = areaMusicVolume;
			} else {
				CharacterType characterType = collision.gameObject.GetComponent<CharacterType>();
				if (characterType != null) {
					intensity -= characterType.threatLevel;
					updateCombatMusic();
				}
				else {
					Debug.Log("Enemy does NOT have a character type script attached! Which means no updating the combat music");
				}
				areaMusic.volume = 0;
			}
		}
	}

	private void Update() {
		if (volumeModificationSpeedInternal > 0) {
			volumeModificationSpeedInternal -= Time.deltaTime;
		}
		else {
			volumeModificationSpeedInternal = volumeModificationSpeed;

			int numberOfAudioSourcesToDeafen = audioSourcesToDeafen.Count - 1;
			if (numberOfAudioSourcesToDeafen > 0) {
				for (int i = numberOfAudioSourcesToDeafen; i != 0; i--) {
					if (audioSourcesToDeafen[i].volume == 0) {
						audioSourcesToDeafen.RemoveAt(i);
					}
				}
			}

			int numberOfAudioSourcesToTurnOn = audioSourcesToTurnOn.Count - 1;
			if (numberOfAudioSourcesToTurnOn > 0) {
				for (int i = numberOfAudioSourcesToTurnOn; i != 0; i--) {
					if (audioSourcesToTurnOn[i].volume == 1) {
						audioSourcesToTurnOn.RemoveAt(i);
					}
				}
			}

			foreach (AudioSource audioSource in audioSourcesToDeafen) {
				audioSource.volume -= volumeModificationIncrement;
			}

			foreach (AudioSource audioSource in audioSourcesToTurnOn) {
				audioSource.volume += volumeModificationIncrement;
			}
		}
	}

	private void updateCombatMusic() {
		if (playerHasBeenDetected) {
			switch (intensity) {
				case 1:
				level1Intensity();
				break;
				case 2:
				level2Intensity();
				break;
				case 3:
				maxIntensity();
				break;
				default:
				if (intensity > 3) {
					maxIntensity();
				}
				break;
			}
		}
	}

	private void level1Intensity() {
		clearAudioSourcesToModify();

		addAudioSourceToTurnOn(fillerAudioSource);
		addAudioSourceToTurnOn(level1AudioSource);

		addAudioSourceToDeafen(level2AudioSource);
		addAudioSourceToDeafen(level3AudioSource);
	}

	private void level2Intensity() {
		clearAudioSourcesToModify();

		addAudioSourceToTurnOn(fillerAudioSource);
		addAudioSourceToTurnOn(level1AudioSource);
		addAudioSourceToTurnOn(level2AudioSource);

		addAudioSourceToDeafen(level3AudioSource);
	}

	private void maxIntensity() {
		clearAudioSourcesToModify();

		addAudioSourceToTurnOn(fillerAudioSource);
		addAudioSourceToTurnOn(level1AudioSource);
		addAudioSourceToTurnOn(level2AudioSource);
		addAudioSourceToTurnOn(level3AudioSource);
	}

	private void deafenMusic() {
		clearAudioSourcesToModify();

		addAudioSourceToDeafen(fillerAudioSource);
		addAudioSourceToDeafen(level1AudioSource);
		addAudioSourceToDeafen(level2AudioSource);
		addAudioSourceToDeafen(level3AudioSource);
	}

	private void addAudioSourceToTurnOn(AudioSource audioSource) {
		if (!audioSourcesToTurnOn.Contains(audioSource) && audioSource != null) {
			audioSourcesToTurnOn.Add(audioSource);
		}
	}

	private void addAudioSourceToDeafen(AudioSource audioSource) {
		if (!audioSourcesToDeafen.Contains(audioSource) && audioSource != null) {
			audioSourcesToDeafen.Add(audioSource);
		}
	}

	private void clearAudioSourcesToModify() {
		audioSourcesToTurnOn.Clear();
		audioSourcesToDeafen.Clear();
	}

	private bool isAudioSourcesFullyDeafened() {
		return audioSourcesToDeafen.Count <= 0;
	}

	private void Start() {
		volumeModificationSpeedInternal = volumeModificationSpeed;
		areaMusic = GameObject.FindGameObjectWithTag("MusicForArea").GetComponent<AudioSource>();
		areaMusicVolume = areaMusic.volume;
	}

	/// <summary>
	/// On enable, we start listening for MMGameEvents. 
	/// </summary>
	protected virtual void OnEnable() {
		this.MMEventStartListening<MMGameEvent>();
	}

	/// <summary>
	/// On disable, we stop listening for MMGameEvents.
	/// </summary>
	protected virtual void OnDisable() {
		this.MMEventStopListening<MMGameEvent>();
	}

	/// <summary>
	/// When we catch an MMGameEvent, we do stuff based on its name
	/// </summary>
	/// <param name="gameEvent">Game event.</param>
	public virtual void OnMMEvent(MMGameEvent gameEvent) {
		switch (gameEvent.EventName) {
			case eventPlayerDetectedId:
			if (playerHasBeenDetected == false && isAudioSourcesFullyDeafened()) {
				level1AudioSource = level1AudioSources[Random.Range(0, level1AudioSources.Length)];
				level2AudioSource = level2AudioSources[Random.Range(0, level2AudioSources.Length)];
				level3AudioSource = level3AudioSources[Random.Range(0, level3AudioSources.Length)];
			}
			playerHasBeenDetected = true;
			updateCombatMusic();
			break;
		}
	}
}
