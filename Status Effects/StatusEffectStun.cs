﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectStun : StatusEffect {
	private AIBrain aiBrainRef;
	private DamageOnTouchBrusier damageOnTouchBrusier;

	public void statusEffectMelee() {
		if (damageOnTouchBrusier != null) {
			damageOnTouchBrusier.enabled = false;
		}
	}

	public void statusEffectEndMelee() {
		if (damageOnTouchBrusier != null) {
			damageOnTouchBrusier.enabled = true;
		}
	}

	public override void statusEffect() {
		if (aiBrainRef != null) {
			aiBrainRef.BrainActive = false;
		} else {
			Debug.Log("Can't stun enemy! Either missing a AI brain or doesn't have handling!");
		}
	}

	public override void statusEffectEnd() {
		if (aiBrainRef != null) {
			aiBrainRef.BrainActive = true;
		}
		else {
			Debug.Log("Can't undo the stun! Either missing a AI brain or doesn't have handling!");
		}
	}

	public override void statusEffectInitialize(GameObject target, StatusEffectId typeOfStatusEffect, float statusTimeLeft, int statusMaxStack, float internalDelay, Sprite statusEffectImage) {
		this.target = target;
		this.typeOfStatusEffect = typeOfStatusEffect;
		this.statusTimeLeft = statusTimeLeft;
		this.statusMaxStack = statusMaxStack;
		this.internalDelay = internalDelay;
		this.statusEffectImage = statusEffectImage;

		aiBrainRef = target.GetComponent<AIBrain>();

		statusEffectDelegate += statusEffect;
		statusEffectEndDelegate += statusEffectEnd;
	}

	public void statusEffectMeleeInitialize(DamageOnTouchBrusier damageOnTouchBrusier) {
		this.damageOnTouchBrusier = damageOnTouchBrusier;

		statusEffectDelegate += statusEffectMelee;
		statusEffectEndDelegate += statusEffectEndMelee;
	}
}
