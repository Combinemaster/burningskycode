﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This is merely a example of what it can do. I don't even think we are gonna have slow down effects within our final game (Burning sky). 
//Take everything here with a grain of salt, make modifications or whatnot where needed.
public class StatusEffectSlowdown : StatusEffect
{
	protected Character character;

	private CharacterHorizontalMovement movementRef;
	private CharacterLunge lungeRef;
	private CharacterHandleWeapon weaponRef;
	private MeshRenderer modelRenderer;
	private Animator modelAnimator;

	private float originalLungeForce;
	private float originalMovementSpeed;
	private float originalFireSpeed;

	private static string MATERIAL_INTENSITY_SLOW_DOWN_PARAM = "_IntensitySlowDown";

	public override void statusEffectInitialize(GameObject target, StatusEffectId typeOfStatusEffect, float statusTimeLeft, int statusMaxStack, float internalDelay, Sprite statusEffectImage) {
		this.target = target;

		character = target.GetComponent<Character>();
		modelRenderer = character.CharacterModel.gameObject.GetComponent<MeshRenderer>();
		modelAnimator = character.CharacterAnimator;
		movementRef = target.GetComponent<CharacterHorizontalMovement>();
		lungeRef = target.GetComponent<CharacterLunge>();
		weaponRef = target.GetComponent<CharacterHandleWeapon>();

		if (lungeRef != null) {
			originalLungeForce = lungeRef.DashForce;
		}
		if (weaponRef != null) {
			originalFireSpeed = weaponRef.InitialWeapon.TimeBetweenUses;
		}
		originalMovementSpeed = movementRef.WalkSpeed;

		this.typeOfStatusEffect = typeOfStatusEffect;
		this.statusTimeLeft = statusTimeLeft;
		this.statusMaxStack = statusMaxStack;
		this.internalDelay = internalDelay;
	}

	public override void statusEffect() {
		movementRef.WalkSpeed = originalMovementSpeed / statusCurrentStack;

		MaterialPropertyBlock mpb = new MaterialPropertyBlock();
		modelRenderer.GetPropertyBlock(mpb);

		mpb.SetFloat(MATERIAL_INTENSITY_SLOW_DOWN_PARAM, statusCurrentStack);

		modelRenderer = character.CharacterModel.gameObject.GetComponent<MeshRenderer>();
		modelRenderer.SetPropertyBlock(mpb);

		float newAnimationSpeed = 1 - ((statusCurrentStack - modelAnimator.speed) / 4);
		modelAnimator.speed = newAnimationSpeed;
	}

	public void statusEffectBrusier() {
		if (lungeRef != null) {
			lungeRef.DashForce = originalLungeForce / statusCurrentStack;
		}
	}

	public void statusEffectRaptor() {
		if (weaponRef != null) {
			weaponRef.CurrentWeapon.TimeBetweenUses = originalFireSpeed * (statusCurrentStack * 1.5f);
		}
	}

	public override void statusEffectEnd() {
		movementRef.WalkSpeed = originalMovementSpeed;

		MaterialPropertyBlock mpb = new MaterialPropertyBlock();
		modelRenderer.GetPropertyBlock(mpb);

		mpb.SetFloat(MATERIAL_INTENSITY_SLOW_DOWN_PARAM, 0);

		modelRenderer.SetPropertyBlock(mpb);

		modelAnimator.speed = 1;
	}

	public void statusEffectEndBrusier() {
		if (lungeRef != null) {
			lungeRef.DashForce = originalLungeForce;
		}
	}

	public void statusEffectEndRaptor() {
		if (weaponRef != null) {
			weaponRef.CurrentWeapon.TimeBetweenUses = originalFireSpeed;
		}
	}
}
