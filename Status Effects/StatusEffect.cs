﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusEffect
{
	public enum StatusEffectId { Slowdown, Root, Stun, Bleed }

	public StatusEffectId typeOfStatusEffect;
	public float statusTimeLeft;
	public int statusMaxStack;
	public int statusCurrentStack = 1;
	public Sprite statusEffectImage;

	//In some cases, we want to have a internal delay for whatever effect happens to not overwhelm the system (Like for a bleed)
	public float internalDelay;
	public float internalDelayCount;

	public GameObject target;

	public abstract void statusEffectInitialize(GameObject target, StatusEffectId typeOfStatusEffect, float statusTimeLeft, int statusMaxStack, float internalDelay, Sprite statusEffectImage);
	public abstract void statusEffect();
	public abstract void statusEffectEnd();

	//This delegates are here so we can "stack" method calls. Have 1 general status effect and then 
	//1+ for specific types of enemies (Ranged, melee, etc). This gives the status effect system
	//a very dynamic and modular functionality. If you need a example, look into the "SlowDownBullet.cs" script, located on the SoldierMachineGunProjectileFreeze prefab
	public delegate void StatusEffectDelegate();
	public delegate void StatusEffectEndDelegate();

	public StatusEffectDelegate statusEffectDelegate;
	public StatusEffectEndDelegate statusEffectEndDelegate;
}
