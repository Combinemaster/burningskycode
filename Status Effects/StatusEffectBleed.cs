﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectBleed : StatusEffect {
	private Health healthRef;
	private int bleedDamage;
	private float flickerDuration;
	private float invincibilityDuration;

	public override void statusEffect() {
		if (healthRef != null) {
			healthRef.Damage(bleedDamage, GameObject.FindGameObjectWithTag("Player"), flickerDuration, invincibilityDuration);
		}
		else {
			Debug.Log("Can't bleed enemy! Either missing a health script or doesn't have handling!");
		}
	}

	public override void statusEffectEnd() {
		//no-op, the bleed stops but the damage stays!
	}

	public override void statusEffectInitialize(GameObject target, StatusEffectId typeOfStatusEffect, float statusTimeLeft, int statusMaxStack, float internalDelay, Sprite statusEffectImage) {
		this.target = target;
		this.typeOfStatusEffect = typeOfStatusEffect;
		this.statusTimeLeft = statusTimeLeft;
		this.statusMaxStack = statusMaxStack;
		this.internalDelay = internalDelay;
		this.statusEffectImage = statusEffectImage;

		healthRef = target.GetComponent<Health>();

		statusEffectDelegate += statusEffect;
		statusEffectEndDelegate += statusEffectEnd;
	}

	public void bleedInitialize(int bleedDamage, float flickerDuration, float invincibilityDuration) {
		this.bleedDamage = bleedDamage;
		this.flickerDuration = flickerDuration;
		this.invincibilityDuration = invincibilityDuration;
	}
}
