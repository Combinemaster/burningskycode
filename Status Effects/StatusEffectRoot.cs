﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectRoot : StatusEffect {
	private CharacterHorizontalMovement movementRef;

	public override void statusEffect() {
		if (movementRef != null) {
			movementRef.MovementSpeed = 0;
		}
	}

	public override void statusEffectEnd() {
		if (movementRef != null) {
			movementRef.MovementSpeed = movementRef.WalkSpeed;
		}
	}

	public void statusEffectMelee() {
		CharacterLunge characterLunge = target.GetComponent<CharacterLunge>();

		if (characterLunge != null) {
			characterLunge.AbilityPermitted = false;
		}
	}

	public void statusEffectMeleeEnd() {
		CharacterLunge characterLunge = target.GetComponent<CharacterLunge>();

		if (characterLunge != null) {
			characterLunge.AbilityPermitted = true;
		}
	}

	public void statusEffectFlying() {
		target.GetComponent<CorgiController>().GravityActive(true);
	}

	public void statusEffectFlyingEnd() {
		target.GetComponent<CorgiController>().GravityActive(false);
	}

	public override void statusEffectInitialize(GameObject target, StatusEffectId typeOfStatusEffect, float statusTimeLeft, int statusMaxStack, float internalDelay, Sprite statusEffectImage) {
		this.target = target;
		this.typeOfStatusEffect = typeOfStatusEffect;
		this.statusTimeLeft = statusTimeLeft;
		this.statusMaxStack = statusMaxStack;
		this.internalDelay = internalDelay;
		this.statusEffectImage = statusEffectImage;

		movementRef = target.GetComponent<CharacterHorizontalMovement>();

		statusEffectDelegate += statusEffect;
		statusEffectEndDelegate += statusEffectEnd;
	}
}
