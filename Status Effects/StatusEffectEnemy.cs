﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffectEnemy : MonoBehaviour
{
	public SpriteRenderer statusEffectSprite;

	private ArrayList statusList = new ArrayList();

	public void addNewStatusEffect(StatusEffect statusEffect) {
		bool statusEffectAlreadyInList = false;

		foreach (StatusEffect statusEffectInList in statusList) {
			if (statusEffect.typeOfStatusEffect.Equals(statusEffectInList.typeOfStatusEffect)) {
				if (statusEffectInList.statusCurrentStack < statusEffectInList.statusMaxStack) {
					statusEffectInList.statusCurrentStack++;
					statusEffectInList.statusTimeLeft = statusEffect.statusTimeLeft;
				}
				statusEffectAlreadyInList = true;
			}
		}

		if (!statusEffectAlreadyInList) {
			statusList.Add(statusEffect);
			if (statusEffect.statusEffectImage != null) {
				statusEffectSprite.gameObject.SetActive(true);
				statusEffectSprite.sprite = statusEffect.statusEffectImage;
			}
		}
	}

    // Update is called once per frame
    void Update()
    {
		foreach (StatusEffect statusEffect in statusList) {
			if (statusEffect.statusTimeLeft <= 0) {
				statusEffect.statusEffectEndDelegate?.Invoke();
				statusEffectSprite.gameObject.SetActive(false);
				statusList.Remove(statusEffect);
				break;
			} else {
				if (statusEffect.internalDelayCount <= 0) {
					statusEffect.statusEffectDelegate?.Invoke();
					statusEffect.internalDelayCount = statusEffect.internalDelay;
				} else {
					statusEffect.internalDelayCount -= Time.deltaTime;
				}
				statusEffect.statusTimeLeft -= Time.deltaTime;

				if (statusEffect.statusEffectImage != null) {
					statusEffectSprite.gameObject.SetActive(true);
					statusEffectSprite.sprite = statusEffect.statusEffectImage;
				}
			}
		}
	}
}
