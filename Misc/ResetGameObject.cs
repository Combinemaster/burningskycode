﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ResetGameObject
{
	bool respawned {
		get;
		set;
	}

	void resetGameObject();
}
