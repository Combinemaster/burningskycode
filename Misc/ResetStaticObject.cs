﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetStaticObject : MonoBehaviour, ResetGameObject {

	public bool respawned = true;

	bool ResetGameObject.respawned {
		get => respawned; set
		{
			respawned = value;
		}
	}

	public void resetGameObject() {
		gameObject.SetActive(true);
		respawned = true;
	}
}
