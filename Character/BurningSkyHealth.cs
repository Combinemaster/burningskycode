﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyHealth : Health {
	[Header("Custom Feedbacks")]
	public MMFeedbacks lowHealthFeedbacks;
	public MMFeedbacks rangedDamageFeedback;
	public MMFeedbacks meleeDamageFeedback;

	[Header("Health Bindings")]
	public string enemyId;

	[Header("Custom Projectile Options")]
	public bool destroyObject = false;

	private BurningSkyGuiManager guiManagerRef;
	private CloakOfThorns cloakOfThornsRef;
	private Character playerRef;

	protected Vector2 lastSafePosition; //This is the place the player last touched on the appropriate platform layer
	protected CorgiController corgiControllerRef;

	public LayerMask safePlatformsLayerMask;
	public LayerMask damagePlatformLayerMask;
	public float controllerInputDelay;
	

	/// <summary>
	/// On initialization, we init the base time control as well as the GUI trackers for the time control bar. 
	/// </summary>
	protected override void Initialization() {
		base.Initialization();
		guiManagerRef = FindObjectOfType<BurningSkyGuiManager>();
		cloakOfThornsRef = FindObjectOfType<CloakOfThorns>();
		playerRef = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
		if (guiManagerRef != null) {
			guiManagerRef.SetUpEnemyHealthBar(false, enemyId);
			UpdateBossHealth();
		}
		corgiControllerRef = gameObject.GetComponent<CorgiController>();
	}

	/// <summary>
	/// Updates the enemies health bar.
	/// </summary>
	public void UpdateBossHealth() {
		if (Application.isPlaying) {
			if (guiManagerRef != null) {
				guiManagerRef.UpdateEnemyHealthBar(CurrentHealth, 0f, MaximumHealth, enemyId);
			}
		}
	}

	/// <summary>
	/// Method used to damage the object that this script is attached too. 
	/// </summary>
	public override void Damage(int damage, GameObject instigator, float flickerDuration, float invincibilityDuration) {
		if (cloakOfThornsRef != null && cloakOfThornsRef.cloakOfThornsActive) {
			CharacterType characterType = instigator.GetComponent<CharacterType>();

			if (characterType != null && characterType.isCharacterMelee()) {
				instigator.GetComponent<Health>().Damage(cloakOfThornsRef.cloakDamage, gameObject, 0, cloakOfThornsRef.cloakOfThornsDamageInvincibilityDuration);
			}
		}

		if (gameObject.tag == "Player") {
			BurningSkyProjectile projectile = instigator.GetComponent<BurningSkyProjectile>();
			CharacterType characterTypeRanged = null;
			if (projectile != null) {
				characterTypeRanged = projectile.getOwner().GetComponent<CharacterType>();
			}

			CharacterType characterTypeMelee = instigator.GetComponent<CharacterType>();
			//TODO: make this a case statement when the character type class gets refactored
			if (characterTypeRanged != null && characterTypeRanged.isCharacterRanged()) {
				rangedDamageFeedback.PlayFeedbacks();
			}
			else if (characterTypeMelee != null && characterTypeMelee.isCharacterMelee() && !(TemporaryInvulnerable || Invulnerable)) {
				meleeDamageFeedback.PlayFeedbacks();
			}
		}

		base.Damage(damage, instigator, flickerDuration, invincibilityDuration);
		UpdateBossHealth();
		if (CurrentHealth <= Mathf.Ceil(MaximumHealth * 0.33f) && lowHealthFeedbacks != null) {
			lowHealthFeedbacks.PlayFeedbacks();
		} else {
			resetFeedbacks();
		}

		if (gameObject.tag == "Player" && instigator.tag == "DamagingPlatforms" && CurrentHealth > 0)
		{
			StartCoroutine(ControllerInputDelay());
			gameObject.transform.position = lastSafePosition;
		}
	}

	/// <summary>
	/// Sets the health of the user and removes health feedbacks if required. 
	/// </summary>
	public override void GetHealth(int newHealth, GameObject instigator) {
		base.GetHealth(newHealth, instigator);
		if (CurrentHealth > Mathf.Ceil(MaximumHealth * 0.15f)) {
			lowHealthFeedbacks.StopFeedbacks();
		}
	}

	/// <summary>
	/// Revives the entity, this makes sure that if there is any health feedbacks playing they stop. 
	/// </summary>
	public override void Revive() {
		base.Revive();
		if (lowHealthFeedbacks != null) {
			lowHealthFeedbacks.StopFeedbacks();
		}
	}

	protected override void DestroyObject() {
		if (destroyObject) {
			if (_character != null) {
				_character.UnFreeze();
			}

			//Only applies to the vine strike and root spear object, if the game object that triggered this method is a vine strike/root spear, also unfreeze the player once it dies. 
			if (gameObject.tag == "VineStrike" || gameObject.tag == "RootSpear") {
				playerRef.UnFreeze();
			}

			Destroy(gameObject);
		}
		else {
			base.DestroyObject();
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (gameObject.tag == "Player" && collision.tag == "FallDamageCheckpoints")
		{
			lastSafePosition = collision.transform.position;
		}
	}

	IEnumerator ControllerInputDelay()
	{
		_character.Freeze();
		yield return new WaitForSeconds(controllerInputDelay);
		_character.UnFreeze();
	}

	public void resetFeedbacks() {
		if (lowHealthFeedbacks != null) {
			lowHealthFeedbacks.StopFeedbacks();
		}
	}
}
