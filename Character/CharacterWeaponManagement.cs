﻿using MoreMountains.CorgiEngine;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterWeaponManagement : MonoBehaviour
{
    public BurningSkyProjectileWeapon[] weaponList;
    private CharacterHandleWeapon characterWeaponRef;
    private int currentWeaponSelected = 0;
    private Text weaponNameRef;
    private Dictionary<int,int> weaponAmmoState;
    private void Start()
    {
        characterWeaponRef = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterHandleWeapon>();
		weaponNameRef = GameObject.FindGameObjectWithTag("WeaponNameUi").GetComponent<Text>();
        weaponAmmoState = new Dictionary<int, int>(); //weaponAmmoState uses the same index as weaponList ex: 0 - 0 / 1 - 1
        for (int i = 0; i < weaponList.Length; i++)
        {
            weaponAmmoState.Add(i, weaponList[i].MagazineSize);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("ScrollWheel") > 0f || Input.GetAxis("ScrollWheel") < 0f) // forward
        {
            saveWeaponState();
            if(currentWeaponSelected == weaponList.Length - 1)
            {
                currentWeaponSelected = 0;
                switchWeapon(weaponList[0]);
            }
            else
            {
                currentWeaponSelected++;
                switchWeapon(weaponList[currentWeaponSelected]);
            }
        }
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            saveWeaponState();
            currentWeaponSelected = 0;
            switchWeapon(weaponList[0]);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            saveWeaponState();
            currentWeaponSelected = 1;
            switchWeapon(weaponList[1]);
        }
    }
    public void switchWeapon(BurningSkyProjectileWeapon gun)
    {
        characterWeaponRef.ChangeWeapon(gun, null);
        playSound(gun.weaponSwitchSound);
        weaponNameRef.text = gun.weaponName;
        characterWeaponRef.CurrentWeapon.GetComponent<BurningSkyProjectileWeapon>().updateCurrentAmmoCount(weaponAmmoState[currentWeaponSelected]);
    }
    public void playSound(AudioClip sound)
    {
        GameObject temporaryAudioHost = new GameObject("TempAudio");
        temporaryAudioHost.transform.position = transform.position;
        AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;
        audioSource.clip = sound;
        audioSource.volume = 0.5f;
        audioSource.pitch = 1f;
        audioSource.loop = false;
        audioSource.Play();
        Destroy(temporaryAudioHost, sound.length);
    }
    public void saveWeaponState()
    {
        weaponAmmoState[currentWeaponSelected] = characterWeaponRef.CurrentWeapon.CurrentAmmoLoaded;
    }
}