﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CharacterType;

/*
This is a class specifying Immnuties for Enemies in the game

    If you want an enemy to the list, simply check the enemy type of that enemy in the top level if statement

    If you want to add a attack that an enemy can be immune to, then add a bool value for that attack,
    and make a copy of one of the attack if statements and change the condition to the bool for that attack.
*/
public class BurningSkyEnemyHealth : Health
{
    [Header("NOTE")]
    [Information("If you want to add any enemy immunities to any ability, Notify the coding team so that they can change this class", MoreMountains.Tools.InformationAttribute.InformationType.Warning, false)]

    public bool PlayerbulletGunAttackImmnuity = false;

    public override void Damage(int damage, GameObject instigator, float flickerDuration, float invincibilityDuration)
    {
        if (GetComponent<CharacterType>().enemyType.Equals(EnemyType.Melee))
        {
            if (PlayerbulletGunAttackImmnuity && instigator.tag == "PlayerBullet")
            {
                return;
            }
            
        }
        base.Damage(damage, instigator, flickerDuration, invincibilityDuration);
    }

	public Vector3 getInitialPosition() {
		return _initialPosition;
	}
}
