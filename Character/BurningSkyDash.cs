﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyDash : CharacterDash
{
	public float dashDoubleTapResetTime = 0.5f; // Half a second before reset

	private CorgiController corgiControllerRef;
	private bool isDashing = false;
	private int dashDoubleTapLeftCount = 0;
	private int dashDoubleTapRightCount = 0;
	private float dashDoubleTapResetTimeInternal;

	// Start is called before the first frame update
	protected override void Start()
    {
        base.Start();
        corgiControllerRef = gameObject.GetComponent<CorgiController>();
		dashDoubleTapResetTimeInternal = dashDoubleTapResetTime;

	}

	private void Update() {
		if (corgiControllerRef.State.JustGotGrounded && !corgiControllerRef.State.IsFalling) {
			isDashing = false;
		}
	}

	protected override void HandleInput() {
		bool doubleTapToDashOption = GameObject.FindGameObjectWithTag("OptionsManager").GetComponent<OptionsManager>().doubleTapToDashEnabled;

		if (doubleTapToDashOption) {
			//For checking for left
			if (_inputManager.DoubleTapLeftDashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				if (dashDoubleTapResetTimeInternal > 0 && dashDoubleTapLeftCount == 1) {
					StartDash();
				} else {
					dashDoubleTapResetTimeInternal = dashDoubleTapResetTime;
					dashDoubleTapLeftCount += 1;
				}
			}

			if (dashDoubleTapResetTimeInternal > 0) {
				dashDoubleTapResetTimeInternal -= 1 * Time.deltaTime;

			} else {
				dashDoubleTapLeftCount = 0;
			}

			//For checking for right
			if (_inputManager.DoubleTapRightDashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				if (dashDoubleTapResetTimeInternal > 0 && dashDoubleTapRightCount == 1) {
					StartDash();
				}
				else {
					dashDoubleTapResetTimeInternal = dashDoubleTapResetTime;
					dashDoubleTapRightCount += 1;
				}
			}

			if (dashDoubleTapResetTimeInternal > 0) {
				dashDoubleTapResetTimeInternal -= 1 * Time.deltaTime;

			}
			else {
				dashDoubleTapRightCount = 0;
			}
		} else {
			if (_inputManager.DashButton.State.CurrentState == MMInput.ButtonStates.ButtonDown) {
				StartDash();
			}
		}
	}

	public override void StartDash()
    {
		// if the Dash action is enabled in the permissions, we continue, if not we do nothing
		if (!AbilityPermitted
            || (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
            || (_movement.CurrentState == CharacterStates.MovementStates.LedgeHanging)
            || (_movement.CurrentState == CharacterStates.MovementStates.Gripping)
			|| (_horizontalInput == 0 && _controller.Speed.y == 0) 
			|| (_movement.CurrentState == CharacterStates.MovementStates.Dashing))
            return;

        // If the user presses the dash button and is not aiming down
        if (_characterDive != null)
        {
            if (_verticalInput < -_inputManager.Threshold.y)
            {
                return;
            }
        }

        // if the character is allowed to dash
        if (_cooldownTimeStamp <= Time.time && !isDashing)
        {
            InitiateDash();
			isDashing = true;
		}
    }
}
