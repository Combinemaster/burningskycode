﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using MoreMountains.CorgiEngine;
using static MoreMountains.Tools.MMCooldown;

/// <summary>
/// Add this class to a character and it'll be able to slow down time when pressing down the TimeControl button
/// This version of the time control comes with a way to setup a bar to see how much time control the user has left. 
/// </summary>
[AddComponentMenu("Corgi Engine/Character/Abilities/Character Time Control")] 
public class BurningSkyCharacterTimeControl : CharacterAbility
{
	private BurningSkyGuiManager guiManagerRef;
	public BurningSkyCooldown burningSkyCooldown;
	/// the time scale to switch to when the time control button gets pressed
	public float TimeScale = 0.5f;
	/// the duration for which to keep the timescale changed
	public float Duration = 1f;
	/// whether or not the timescale should get lerped
	public bool LerpTimeScale = true;
	/// the speed at which to lerp the timescale
	public float LerpSpeed = 5f;
	/// the cooldown for this ability
	protected bool _timeControlled = false;

	public void reset()
	{
		Initialization();
	}

	protected override void Initialization()
    {
        base.Initialization();
		burningSkyCooldown.Initialization();
		guiManagerRef = FindObjectOfType<BurningSkyGuiManager>();
		if (guiManagerRef != null && _character.CharacterType == Character.CharacterTypes.Player) {
			guiManagerRef.SetUpTimeSlowDownBar(true, _character.PlayerID);
			UpdateTimeControlBar();
		}
	}

	protected virtual void UpdateTimeControlBar() {
		if (Application.isPlaying) {
			if ((guiManagerRef != null) && (_character.CharacterType == Character.CharacterTypes.Player)) {
				guiManagerRef.UpdateTimeSlowDownBar(burningSkyCooldown.CurrentDurationLeft, 0f, burningSkyCooldown.ConsumptionDuration, _character.PlayerID);
			}
		}
	}

	public override void ProcessAbility() {
		base.ProcessAbility();
		burningSkyCooldown.Update();
		if ((burningSkyCooldown.CooldownState != MMCooldown.CooldownStates.Consuming) && _timeControlled)
		{
			_timeControlled = false;
			MMTimeScaleEvent.Trigger(MMTimeScaleMethods.Unfreeze, 1f, 0f, false, 0f, false);
		}
		UpdateTimeControlBar();

	}

	public virtual void TimeControlStart()
	{
		if (burningSkyCooldown.Ready())
		{
			PlayAbilityStartFeedbacks();
			MMTimeScaleEvent.Trigger(MMTimeScaleMethods.For, TimeScale, Duration, LerpTimeScale, LerpSpeed, true);
			burningSkyCooldown.Start();
			_timeControlled = true;
		}
	}

	public virtual void TimeControlStop()
	{
		if (!burningSkyCooldown.cooldownReset && !burningSkyCooldown.cooldownFullySpent) {
			StopStartFeedbacks();
			PlayAbilityStopFeedbacks();
			burningSkyCooldown.Stop();
		}
	}

	protected override void HandleInput()
	{
		base.HandleInput();
		if (_inputManager.TimeControlButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
		{
			TimeControlStart();
		}
		if (_inputManager.TimeControlButton.State.CurrentState == MMInput.ButtonStates.ButtonUp || (!burningSkyCooldown.cooldownReset && burningSkyCooldown.CooldownState == CooldownStates.PauseOnEmpty))
		{
			TimeControlStop();
		}
	}
}