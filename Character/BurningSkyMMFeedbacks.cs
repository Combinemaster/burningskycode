﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class meant to be extendable by all future feedbacks. Because corgiengine can be a little shit, and not liking feedback specific coroutines that tend to, for some reason, not listen whatsoever, I added a failsafe to this
/// class that kills ALL coroutines that the feedback tries to run. Why does this work despite us already killing all the coroutines? Beats me. 
/// </summary>
public class BurningSkyMMFeedbacks : MMFeedback {
	public override void Stop(Vector3 position, float attenuation = 1) {
		base.Stop(position, attenuation);
		StopAllCoroutines();

		_lastPlayTimestamp = 0f;
		_playsLeft = Timing.NumberOfRepeats;
		CustomStopFeedback(position, attenuation);
	}

	protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1) {
		//no-op, this class serves as a base for all future feedbacks
	}
}
