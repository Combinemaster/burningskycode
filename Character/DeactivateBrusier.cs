﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateBrusier : MonoBehaviour
{
	public GameObject brusier;
	public GameObject activateBrusier;

	protected void OnTriggerEnter2D(Collider2D collider) {
		if (collider.tag == "Player") {
			brusier.GetComponent<AIBrain>().BrainActive = false;
			brusier.transform.position = brusier.GetComponent<BurningSkyEnemyHealth>().getInitialPosition();
			activateBrusier.SetActive(false);
		}
	}
}
