﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MoreMountains.Tools;
using UnityEngine.EventSystems;
using MoreMountains.CorgiEngine;

/// <summary>
/// Handles all GUI effects and changes the burning sky version here allows the addition of more UI progress bars for custom abilities (Such as time control)
/// </summary>
public class BurningSkyGuiManager : GUIManager {
    [Header("Custom Bindings")]
	public MMProgressBar[] timeSlowDownBars;
	public MMProgressBar[] enemyHealthBars;
	public MMProgressBar manaBar;

	/// <summary>
	/// Sets up the boss heatlh bar
	/// </summary>
	/// <param name="state">If set to <c>true</c>, sets the pause.</param>
	/// <param name="enemyID">The enemy ID, just in case there is more than 1 enemy health bar that needs to be updated</param>
	public virtual void SetUpEnemyHealthBar(bool state, string enemyID) {
		if (enemyHealthBars == null) {
			return;
		}

		foreach (MMProgressBar enemyHealthBar in enemyHealthBars) {
			if (enemyHealthBar != null) {
				if (enemyHealthBar.PlayerID == enemyID) {
					enemyHealthBar.gameObject.SetActive(state);
				}
			}
		}
	}

	/// <summary>
	/// Updates the time slow down bar
	/// </summary>
	/// <param name="currentSlowDownLeft">Current boss health left.</param>
	/// <param name="minSlowDown">Minimum health.</param>
	/// <param name="maxSlowDown">Max health.</param>
	/// <param name="enemyID">The enemy ID, just in case there is more than 1 enemy health bar that needs to be updated</param>
	public virtual void UpdateEnemyHealthBar(float currentHealthLeft, float minHealth, float maxHealth, string enemyID) {
		if (enemyHealthBars == null) {
			return;
		}

		foreach (MMProgressBar enemyHealthBar in enemyHealthBars) {
			if (enemyHealthBar == null) { return; }
			if (enemyHealthBar.PlayerID == enemyID) {
				enemyHealthBar.UpdateBar(currentHealthLeft, minHealth, maxHealth);
			}
		}
	}

	/// <summary>
	/// Sets up the time slow down bar
	/// </summary>
	/// <param name="state">If set to <c>true</c>, sets the pause.</param>
	public virtual void SetUpTimeSlowDownBar(bool state, string playerID) {
		if (timeSlowDownBars == null) {
			return;
		}

		foreach (MMProgressBar timeSlowDownBar in timeSlowDownBars) {
			if (timeSlowDownBar != null) {
				if (timeSlowDownBar.PlayerID == playerID) {
					timeSlowDownBar.gameObject.SetActive(state);
				}
			}
		}
	}

	/// <summary>
	/// Updates the time slow down bar
	/// </summary>
	/// <param name="currentSlowDownLeft">Current slow down left.</param>
	/// <param name="minSlowDown">Minimum  slow down.</param>
	/// <param name="maxSlowDown">Max slow down.</param>
	/// <param name="playerID">Player ID.</param>
	public virtual void UpdateTimeSlowDownBar(float currentSlowDownLeft, float minSlowDown, float maxSlowDown, string playerID) {
		if (timeSlowDownBars == null) {
			return;
		}

		foreach (MMProgressBar timeSlowDownBar in timeSlowDownBars) {
			if (timeSlowDownBar == null) { return; }
			if (timeSlowDownBar.PlayerID == playerID) {
				timeSlowDownBar.UpdateBar(currentSlowDownLeft, minSlowDown, maxSlowDown);
			}
		}
	}

	/// <summary>
	/// Sets up the mana bar
	/// </summary>
	/// <param name="state">If set to <c>true</c>, sets the pause.</param>
	public virtual void SetUpManaBar(bool state, string playerID) {
		if (manaBar == null) {
			return;
		}

		if (manaBar.PlayerID == playerID) {
			manaBar.gameObject.SetActive(state);
		}
	}

	/// <summary>
	/// Updates the time slow down bar
	/// </summary>
	/// <param name="currentSlowDownLeft">Current slow down left.</param>
	/// <param name="minSlowDown">Minimum  slow down.</param>
	/// <param name="maxSlowDown">Max slow down.</param>
	/// <param name="playerID">Player ID.</param>
	public virtual void UpdateManaBar(float currentManaLeft, float maxMana, string playerID) {
		if (manaBar == null) {
			return;
		}

		if (manaBar.PlayerID == playerID) {
			manaBar.UpdateBar(currentManaLeft, 0, maxMana);
		}
	}
}