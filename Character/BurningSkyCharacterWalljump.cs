﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine;

public class BurningSkyCharacterWalljump : CharacterWalljump {
	private bool jumpedThisFrame = false;
	[Information("Keep this number between 0f and 1f unless you want to add weird ghost forces in a direction", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	public float wallJumpDirection = 1f;

	protected override void HandleInput() {
		WallJumpHappenedThisFrame = false;

		if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonDown && !jumpedThisFrame) {
			jumpedThisFrame = true;
			Walljump();
		} else if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) {
			jumpedThisFrame = false;
		}
	}

	protected override void Walljump() {
		if (!AbilityPermitted
				|| _condition.CurrentState != CharacterStates.CharacterConditions.Normal) {
			return;
		}

		// if we're here the jump button has been pressed. If we were wallclinging, we walljump
		if (_movement.CurrentState == CharacterStates.MovementStates.WallClinging) {
			_movement.ChangeState(CharacterStates.MovementStates.WallJumping);

			// we decrease the number of jumps left
			if ((_characterJump != null) && ShouldReduceNumberOfJumpsLeft) {
				_characterJump.SetNumberOfJumpsLeft(_characterJump.NumberOfJumpsLeft - 1);
				_characterJump.SetJumpFlags();
			}

			_condition.ChangeState(CharacterStates.CharacterConditions.Normal);
			_controller.GravityActive(true);
			_controller.SlowFall(0f);

			//The original corgi implementation for the below for some reason doesn't work unless the user is holding the joystick towards the wall
			//our implementation is way more reliable, and much more sensitive. 
			Vector3 raycastOrigin = transform.position;
			Vector3 raycastDirection;

			float wallJumpDirectionInternal = 0f;

			if (GetComponent<BurningSkyWallClinging>().wasCharacterFacingRight()) {
				raycastOrigin = raycastOrigin + transform.right * _controller.Width() / 2;
				raycastDirection = transform.right - transform.up;
			}
			else {
				raycastOrigin = raycastOrigin - transform.right * _controller.Width() / 2;
				raycastDirection = -transform.right - transform.up;
			}

			RaycastHit2D hit = MMDebug.RayCast(raycastOrigin, raycastDirection, 0.3f, _controller.PlatformMask | _controller.OneWayPlatformMask | _controller.MovingOneWayPlatformMask, Color.black, _controller.Parameters.DrawRaycastsGizmos);

			if (GetComponent<BurningSkyWallClinging>().wasCharacterFacingRight()) {
				if (hit) {
					wallJumpDirectionInternal = -wallJumpDirection;
				}
			} else {
				if (hit) {
					wallJumpDirectionInternal = wallJumpDirection;
				}
			}

			Vector2 walljumpVector = new Vector2(
									wallJumpDirectionInternal * WallJumpForce.x,
									Mathf.Sqrt(2f * WallJumpForce.y * Mathf.Abs(_controller.Parameters.Gravity))
			);
			_controller.AddForce(walljumpVector);
			PlayAbilityStartFeedbacks();
			WallJumpHappenedThisFrame = true;

			if (GetComponent<BurningSkyWallClinging>().wasCharacterFacingRight() == _character.IsFacingRight) {
				_character.Flip();
			} 

			return;
		}
	}
}
