﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterType : MonoBehaviour
{
	/// The type of character that this enemy is.
	public enum EnemyType { Ranged, Melee, Flying }

	public int threatLevel = 0;

	/// storage for enemy type
	[Information("Here you can set the type of enemy this character is. This is specifically used for cloak of thorns and other similar abilities.", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
	public EnemyType enemyType = EnemyType.Ranged;

	//Helper method to quickly check if a character is ranged
	public bool isCharacterRanged() {
		return enemyType.Equals(EnemyType.Ranged);
	}

	//Helper method to quickly check if a character is melee
	public bool isCharacterMelee() {
		return enemyType.Equals(EnemyType.Melee);
	}

	//Helper method to quickly check if a character is flying
	public bool isCharacterFlying() {
		return enemyType.Equals(EnemyType.Flying);
	}
}
