﻿using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyHorizontalMovement : CharacterHorizontalMovement {
	//Feedbacks for the various levels of landing sounds, depending on how long the player has been in the air
	//Intensity is retreived from the "ClimbingSoundEffect" script attached to the main character. 
	public MMFeedbacks lightLandingSoundFeedback;
	public MMFeedbacks mediumLandingSoundFeedback;
	public MMFeedbacks heavyLandingSoundFeedback;

	//The number to get past to trigger the medium landing sound feedback. 
	public float mediumLandingThreshold = 0.4f;

	//The number to get past to trigger the heavy landing sound feedback.
	public float heavyLandingThreshold = 0.7f;

	//Value to check if the player was moving left or right after some delay. This is done to prevent the issue with the animation playing the intro after user switches side when they are running.
	public int horizontalMovementDelayedValue;
	public float horizontalMovementCheckDelay = 0.1f;

	// animation parameters
	protected const string _horizontalMovementParameterName = "HorizontalMovement";
	protected int _horizontalMovementAnimationParameter;

	private float horizontalMovementCheckDelayInternal;

	// Update is called once per frame
	void Update() {
		if (_horizontalMovement != 0) {
			horizontalMovementDelayedValue = (int) _horizontalMovement;
		}

		if (_horizontalMovement == 0 && horizontalMovementCheckDelayInternal <= 0) {
			horizontalMovementDelayedValue = 0;
			horizontalMovementCheckDelayInternal = horizontalMovementCheckDelay;
		} else if (_horizontalMovement == 0) {
			horizontalMovementCheckDelayInternal -= Time.deltaTime;
		} else {
			horizontalMovementCheckDelayInternal = horizontalMovementCheckDelay;
		}
	}

	/// <summary>
	/// Called at the very start of the ability's cycle, and intended to be overridden, looks for input and calls
	/// methods if conditions are met
	/// </summary>
	protected override void HandleInput() {
		if (_character.MovementState.CurrentState != CharacterStates.MovementStates.VineSwinging) {
			base.HandleInput();
		} else {
			_horizontalMovement = 0;
			// If the value of the horizontal axis is positive, the character must face right.
			if (_horizontalInput > InputThreshold) { 
				if (!_character.IsFacingRight) {
					_character.Flip();
				}
			}
			// If it's negative, then we're facing left
			else if (_horizontalInput < -InputThreshold) {
				if (_character.IsFacingRight) {
					_character.Flip();
				}
			}
		}
	}

	/// <summary>
	/// Every frame, checks if we just hit the ground, and if yes, changes the state and triggers a particle effect
	/// </summary>
	protected override void CheckJustGotGrounded() {
		// if the character just got grounded
		if (_movement.CurrentState == CharacterStates.MovementStates.Dashing) {
			return;
		}

		if (_controller.State.JustGotGrounded) {
			if (_controller.State.ColliderResized) {
				_movement.ChangeState(CharacterStates.MovementStates.Crouching);
			}
			else {
				_movement.ChangeState(CharacterStates.MovementStates.Idle);
			}

			_controller.SlowFall(0f);

			if (Time.time - _lastTimeGrounded > MinimumAirTimeBeforeFeedback) {
				float lastTimeGroundedAdjustedForTime = Time.time - _lastTimeGrounded;

				if (lastTimeGroundedAdjustedForTime >= heavyLandingThreshold) {
					heavyLandingSoundFeedback.PlayFeedbacks();
				} else if (lastTimeGroundedAdjustedForTime >= mediumLandingThreshold) {
					mediumLandingSoundFeedback.PlayFeedbacks();
				} else {
					lightLandingSoundFeedback.PlayFeedbacks();
				}
			}

		}
		if (_controller.State.IsGrounded) {
			_lastTimeGrounded = Time.time;
		}
	}

	protected override void InitializeAnimatorParameters() {
		base.InitializeAnimatorParameters();
		RegisterAnimatorParameter(_horizontalMovementParameterName, AnimatorControllerParameterType.Int, out _horizontalMovementAnimationParameter);
	}

	public override void UpdateAnimator() {
		base.UpdateAnimator();
		MMAnimatorExtensions.UpdateAnimatorInteger(_animator, _horizontalMovementAnimationParameter, horizontalMovementDelayedValue, _character._animatorParameters);
	}
}
