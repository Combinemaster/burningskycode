﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedForm : CharacterAbility
{
	public float chargedFormDuration = 2.0f;
	public int chargedFormExtraDamage = 5;
	public GameObject chargedFormActiveSprite;

	[ReadOnly]
	public bool chargedFormActive;

	private float durationInternal;

	protected override void Start() {
		base.Start();
		durationInternal = 0;
	}

	/// <summary>
	/// Called at the very start of the ability's cycle, and intended to be overridden, looks for input and calls methods if conditions are met
	/// </summary>
	protected override void HandleInput() {
		//If statement here to bind it to a specific key. for now it's bound to nothing since that system hasn't been developed yet.
		/*
		if (_inputManager.Spell1 == MMInput.ButtonStates.ButtonDown) {
			chargedFormActive = true;
			durationInternal = chargedFormDuration;
			chargedFormActiveSprite.SetActive(true);
		}
		*/
	}

	public override void ProcessAbility() {
		//Process the ability here, if the player has pressed the required key in handle input. the method should start counting down, while this duration is above 0, the ability should remain active. 
		if (durationInternal > 0) {
			durationInternal -= Time.deltaTime;
		}
		else {
			durationInternal = 0;
			chargedFormActive = false;
			chargedFormActiveSprite.SetActive(false);
		}
	}

	public override void ResetAbility() {
		durationInternal = 0;
		base.ResetAbility();
	}
}
