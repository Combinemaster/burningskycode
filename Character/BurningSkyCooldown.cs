﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BurningSkyCooldown : MMCooldown
{
    public float pauseOnAbilityStop = 1f;
    protected float abilityEndTimestamp = 0f;
	[ReadOnly]
	public bool cooldownReset = true;
	[ReadOnly]
	public bool cooldownFullySpent = false;

	public override void Stop()
    {
        if (CooldownState == CooldownStates.Consuming)
        {
            abilityEndTimestamp = Time.time;
            CooldownState = CooldownStates.PauseOnEmpty;
		}
		cooldownReset = true;
		if (CurrentDurationLeft <= 0) {
			cooldownFullySpent = true;
		}
	}

	public override void Start() {
		if (Ready()) {
			CooldownState = CooldownStates.Consuming;
			cooldownReset = false;
			cooldownFullySpent = false;
		}
	}

	// Update is called once per frame
	public override void Update()
    {
        if (Unlimited)
        {
            return;
        }

        switch (CooldownState)
        {
            case CooldownStates.Idle:
                break;

            case CooldownStates.Consuming:
                CurrentDurationLeft = CurrentDurationLeft - Time.deltaTime;
                if (CurrentDurationLeft <= 0f)
                {
                    CurrentDurationLeft = 0f;
                    abilityEndTimestamp = Time.time;
                    CooldownState = CooldownStates.PauseOnEmpty;
                }
                break;

            case CooldownStates.PauseOnEmpty:
                if (Time.time - abilityEndTimestamp >= pauseOnAbilityStop)
                {
                    CooldownState = CooldownStates.Refilling;
                }
                break;

            case CooldownStates.Refilling:
                CurrentDurationLeft += RefillDuration * Time.deltaTime;
                if (CurrentDurationLeft >= ConsumptionDuration)
                {
                    CurrentDurationLeft = ConsumptionDuration;
                    CooldownState = CooldownStates.Idle;
                }
                break;
        }
    }

	public override bool Ready() {
		if (Unlimited) {
			return true;
		}
		if (CooldownState != CooldownStates.Consuming && CurrentDurationLeft > 0) {
			return true;
		}
		if ((CooldownState == CooldownStates.Refilling) && (CanInterruptRefill)) {
			return true;
		}
		return false;
	}

	public override void Initialization()
    {
        _pauseOnEmptyWFS = new WaitForSeconds(pauseOnAbilityStop);
        CurrentDurationLeft = ConsumptionDuration;
        CooldownState = CooldownStates.Idle;
        abilityEndTimestamp = 0f;
    }
}
