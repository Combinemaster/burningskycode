﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyCharacterJump : CharacterJump
{
	private bool jumpedThisFrame = false; 

	protected override bool EvaluateJumpConditions()
	{
		bool onAOneWayPlatform = (_controller.OneWayPlatformMask.MMContains(_controller.StandingOn.layer)
			|| _controller.MovingOneWayPlatformMask.MMContains(_controller.StandingOn.layer));


		if (!AbilityPermitted  // if the ability is not permitted
			|| !JumpAuthorized // if jumps are not authorized right now
			|| (!_controller.CanGoBackToOriginalSize() && !onAOneWayPlatform)
			|| ((_condition.CurrentState != CharacterStates.CharacterConditions.Normal) // or if we're not in the normal stance
				&& (_condition.CurrentState != CharacterStates.CharacterConditions.ControlledMovement))
			|| (_movement.CurrentState == CharacterStates.MovementStates.Jetpacking) // or if we're jetpacking
			|| (_movement.CurrentState == CharacterStates.MovementStates.Dashing) // or if we're dashing
			|| (_movement.CurrentState == CharacterStates.MovementStates.Pushing) // or if we're pushing                
			|| ((_movement.CurrentState == CharacterStates.MovementStates.WallClinging) && (_characterWallJump != null)) // or if we're wallclinging and can walljump
			|| (_movement.CurrentState == CharacterStates.MovementStates.VineSwinging) //or if we are vine swinging
			|| (_controller.State.IsCollidingAbove && !onAOneWayPlatform)) // or if we're colliding with the ceiling
		{
			return false;
		}

		// if we're in a button activated zone and can interact with it
		if (_characterButtonActivation != null)
		{
			if (_characterButtonActivation.AbilityPermitted
				&& _characterButtonActivation.PreventJumpWhenInZone
				&& _characterButtonActivation.InButtonActivatedZone
				&& !_characterButtonActivation.InButtonAutoActivatedZone)
			{
				return false;
			}
		}

		// if we're crouching and don't have enough space to stand we do nothing and exit
		if ((_movement.CurrentState == CharacterStates.MovementStates.Crouching) || (_movement.CurrentState == CharacterStates.MovementStates.Crawling))
		{
			if (_characterCrouch != null)
			{
				if (_characterCrouch.InATunnel && (_verticalInput >= -_inputManager.Threshold.y))
				{
					return false;
				}
			}
		}

		// if we're not grounded, not on a ladder, and don't have any jumps left, we do nothing and exit
		if ((!_controller.State.IsGrounded)
			&& !EvaluateJumpTimeWindow()
			&& (_movement.CurrentState != CharacterStates.MovementStates.LadderClimbing)
			&& (JumpRestrictions != JumpBehavior.CanJumpAnywhereAnyNumberOfTimes)
			&& (NumberOfJumpsLeft <= 0))
		{
			return false;
		}

		if (_controller.State.IsGrounded
			&& (NumberOfJumpsLeft <= 0))
		{
			return false;
		}

		if (_inputManager != null)
		{
			if (_verticalInput < -_inputManager.Threshold.y && _controller.State.IsGrounded)
			{
				_movement.ChangeState(CharacterStates.MovementStates.Jumping);

				if (JumpDownFromOneWayPlatform())
				{
					return false;
				}
			}

			// if the character is standing on a moving platform and not pressing the down button,
			if (_controller.State.IsGrounded)
			{
				JumpFromMovingPlatform();
			}
		}

		return true;
	}

	/// <summary>
	/// At the beginning of each cycle we check if we've just pressed or released the jump button
	/// </summary>
	protected override void HandleInput() {
		if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonDown && !jumpedThisFrame) {
			jumpedThisFrame = true;
			JumpStart();
		}
		if (_inputManager.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonUp) {
			jumpedThisFrame = false;
			JumpStop();
		}
	}

	public override void ProcessAbility() {
		JumpHappenedThisFrame = false;

		if (!AbilityPermitted) { return; }

		//Added a extra check. If the previous state was vine swinging DO NOT add any extra Y force. VineStrike.cs controls the jump strength fully
		if (_character.MovementState.PreviousState == CharacterStates.MovementStates.VineSwinging) { return; }

		// if we just got grounded, we reset our number of jumps
		if (_controller.State.JustGotGrounded) {
			NumberOfJumpsLeft = NumberOfJumps;
			_doubleJumping = false;
		}

		// we store the last timestamp at which the character was grounded
		if (_controller.State.IsGrounded) {
			_lastTimeGrounded = Time.time;
		}

		// If the user releases the jump button and the character is jumping up and enough time since the initial jump has passed, then we make it stop jumping by applying a force down.
		if ((_jumpButtonPressTime != 0)
			&& (Time.time - _jumpButtonPressTime >= JumpMinimumAirTime)
			&& (_controller.Speed.y > Mathf.Sqrt(Mathf.Abs(_controller.Parameters.Gravity)))
			&& (_jumpButtonReleased)
			&& (!_jumpButtonPressed
				|| (_movement.CurrentState == CharacterStates.MovementStates.Jetpacking))) {
			_jumpButtonReleased = false;
			if (JumpIsProportionalToThePressTime) {
				_jumpButtonPressTime = 0;
				if (JumpReleaseForceFactor == 0f) {
					_controller.SetVerticalForce(0f);
				}
				else {
					_controller.AddVerticalForce(-_controller.Speed.y / JumpReleaseForceFactor);
				}
			}
		}

		UpdateController();
	}

	void Update()
	{
		/*
		 * Temporarily removing the whole "options" thing for jumping on PC, the default way is uncomfortable and thanks to the new mechnics we have keyboard controls will take a while
		 * to figure out
		 */
		 /*
		int dropDownOption = GameObject.FindGameObjectWithTag("OptionsManager").GetComponent<OptionsManager>().dropDownOption;
		if (dropDownOption == 1)
		{
			if (_verticalInput < -_inputManager.Threshold.y && _controller.State.IsGrounded)
			{
				_movement.ChangeState(CharacterStates.MovementStates.Jumping);

				JumpDownFromOneWayPlatform();
			}
		}
		*/

		if (!_inputManager.isGamepadEnabled) {
			//Defualting to just pressing the down key for now.
			if (_verticalInput < 0 && _controller.State.IsGrounded) {
				_movement.ChangeState(CharacterStates.MovementStates.Jumping);

				JumpDownFromOneWayPlatform();
			}
		}
	}
}
