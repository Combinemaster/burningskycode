﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkyCharacter : Character
{
	/// Added this attribute so gravity is not applied to this character;
	public bool ignoreGravity;

	protected override void Initialization() {
		// we initialize our state machines
		MovementState = new MMStateMachine<CharacterStates.MovementStates>(gameObject, SendStateChangeEvents);
		ConditionState = new MMStateMachine<CharacterStates.CharacterConditions>(gameObject, SendStateChangeEvents);

		if (InitialFacingDirection == FacingDirections.Left) {
			IsFacingRight = false;
		}
		else {
			IsFacingRight = true;
		}

		// instantiate camera target
		if (CameraTarget == null) {
			CameraTarget = new GameObject();
		}
		CameraTarget.transform.SetParent(this.transform);
		CameraTarget.transform.localPosition = Vector3.zero;
		CameraTarget.name = "CameraTarget";
		_cameraTargetInitialPosition = CameraTarget.transform.localPosition;

		// we get the current input manager
		SetInputManager();
		// we get the main camera
		if (Camera.main != null) {
			SceneCamera = Camera.main.GetComponent<CameraController>();
		}
		// we store our components for further use 
		CharacterState = new CharacterStates();
		_spriteRenderer = GetComponent<SpriteRenderer>();
		_controller = GetComponent<CorgiController>();
		_characterAbilities = GetComponents<CharacterAbility>();
		_aiBrain = GetComponent<AIBrain>();
		_health = GetComponent<Health>();
		_damageOnTouch = GetComponent<DamageOnTouch>();
		CanFlip = true;
		AssignAnimator();

		if (!ignoreGravity) {
			_originalGravity = _controller.Parameters.Gravity;
		}

		ForceSpawnDirection();
	}
}
