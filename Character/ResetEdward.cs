﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetEdward : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		gameObject.GetComponent<Health>().OnDeath += onPlayerDeath;

	}

	public void onPlayerDeath() {
		gameObject.GetComponent<BurningSkyCharacterTimeControl>().reset();
	}
}
