﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningSkySimpleObjectPooler : MMSimpleObjectPooler
{
	public List<GameObject> getPooledGameObjects() {
		return _pooledGameObjects;
	}
}
