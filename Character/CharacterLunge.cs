﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An override of the character dash, allowing a NPC to lunge at a target via a curve than a straight line. 
/// </summary>
public class CharacterLunge : CharacterDash {
	//The direction that the NPC should lunge at.
	protected Vector2 targetVector;

	protected override void ComputeDashDirection() {
		if (targetVector.y < 0f) {
			targetVector.y = 0.2f;
		}

		_dashDirection = targetVector;
	}

	public void setTargetVector(Vector2 targetVector) {
		//To fix later, ticket 336
		/*
		if (gameObject.GetComponent<Character>().IsFacingRight) {
			targetVector.x += 5.0f;
		} else {
			targetVector.x -= 5.0f;
		}
		*/

		this.targetVector = targetVector.normalized;
	}

	public bool canLunge()
	{
		return (_cooldownTimeStamp <= Time.time);
	}
}
