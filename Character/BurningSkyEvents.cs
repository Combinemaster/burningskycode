﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BurningSky
{
	public struct EnemyEvent
	{

		public EnemyEventType eventName;
		public int enemyId;

		public EnemyEvent(EnemyEventType newName, int enemyId)
		{
			eventName = newName;
			this.enemyId = enemyId;
		}
	}

	public enum EnemyEventType
	{
		EnemyDied
	}

}
