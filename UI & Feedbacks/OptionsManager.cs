﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    public GameObject optionsPanel;

    [ReadOnly]
    public int dropDownOption;

	[ReadOnly]
	public bool doubleTapToDashEnabled;

    public Dropdown dropDownOptionUI;
	public Toggle doubleTapToDashOptionUI;

    private const string DROPDOWN_OPTION_NAME = "dropdown_option";
    private const string DOUBLETAP_OPTION_NAME = "doubleTapToDash_option";

    public void openOptionsMenu()
    {
        optionsPanel.SetActive(true);
    }

    public void closeOptionsMenu()
    {
        optionsPanel.SetActive(false);
    }

    public void saveOptions()
    {
        dropDownOption = dropDownOptionUI.value;
		doubleTapToDashEnabled = doubleTapToDashOptionUI.isOn;
        PlayerPrefs.SetInt(DROPDOWN_OPTION_NAME, dropDownOption);
        PlayerPrefs.SetInt(DOUBLETAP_OPTION_NAME, doubleTapToDashEnabled ? 1 : 0);
        PlayerPrefs.Save();
        closeOptionsMenu();
	}
    private void Start()
    {
        dropDownOption = PlayerPrefs.GetInt(DROPDOWN_OPTION_NAME);
        doubleTapToDashEnabled = PlayerPrefs.GetInt(DOUBLETAP_OPTION_NAME) == 1 ? true : false ;
        dropDownOptionUI.value = dropDownOption;
        doubleTapToDashOptionUI.isOn = doubleTapToDashEnabled;
    }
}