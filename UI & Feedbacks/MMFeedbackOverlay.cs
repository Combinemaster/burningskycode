﻿using MoreMountains.Feedbacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("")]
[FeedbackPath("Camera/Image")]
[FeedbackHelp("This feedback lets the developer show some sort of full-screen overlay, such as for when time slows down or for when the players health is low.")]
public class MMFeedbackOverlay : MMFeedback {
	[Header("Overlay")]
	/// the overlay image to display
	public Sprite overlayToDisplay;
	public float imageTransparency;

	//variable to store the newly created overlay object. 
	private Image overlayObject;

	public bool fadeInImage;
	public bool destroyOverlayOnFinish;
	
	protected override void CustomPlayFeedback(Vector3 position, float attenuation = 1) {
		GameObject overlayContainer = GameObject.FindGameObjectWithTag("OverlayContainer");
		if (overlayContainer != null && overlayObject == null) {
			GameObject temporaryOverlayObject = new GameObject();
			// we set the image's position
			temporaryOverlayObject.transform.position = overlayContainer.transform.position;
			temporaryOverlayObject.transform.SetParent(overlayContainer.transform);
			// add add several components that will cover the entire screen.
			RectTransform tempRectTransform = temporaryOverlayObject.AddComponent<RectTransform>();
			tempRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 0);
			tempRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0, 0);
			tempRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 0);
			tempRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 0);
			tempRectTransform.anchorMin = new Vector2(0, 0);
			tempRectTransform.anchorMax = new Vector2(1, 1);
			tempRectTransform.anchoredPosition = new Vector2(0.5f, 0.5f);
			tempRectTransform.localScale = new Vector3(1, 1, 1);

			temporaryOverlayObject.AddComponent<CanvasRenderer>();
			overlayObject = temporaryOverlayObject.AddComponent<Image>() as Image;
			overlayObject.sprite = overlayToDisplay;

			overlayObject.transform.SetParent(overlayContainer.transform);
		}

		if (fadeInImage)
		{
			overlayObject.canvasRenderer.SetAlpha(0.0f);
			overlayObject.CrossFadeAlpha(imageTransparency, 0.5f, true);
		}
	}

	protected override void CustomStopFeedback(Vector3 position, float attenuation = 1.0f) {
		StartCoroutine(Stop());
	}

	IEnumerator Stop()
	{
		if (overlayObject != null)
		{
			if (fadeInImage) {
				overlayObject.CrossFadeAlpha(0.0f, 0.5f, true);
				yield return new WaitForSeconds(0.5f);
			}
			if (destroyOverlayOnFinish) 
			{
				Destroy(overlayObject.gameObject);
				overlayObject = null;
			}
		}
	}
}
