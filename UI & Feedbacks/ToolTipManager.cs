﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipManager : MonoBehaviour, MMEventListener<CorgiEngineEvent> {
	public GameObject toolTipParentObject;
	public float keepTooltipVisibleForSeconds;

	private float tooltipVisibleCounter;

	public void OnMMEvent(CorgiEngineEvent eventType) {
		if (eventType.EventType == CorgiEngineEventTypes.PlayerDeath) {
			tooltipVisibleCounter = keepTooltipVisibleForSeconds;
			toolTipParentObject.GetComponent<Image>().enabled = true;
		}
	}

	public void Update() {
		if (tooltipVisibleCounter <= 0) {
			toolTipParentObject.GetComponent<Image>().enabled = false;
		} else {
			tooltipVisibleCounter -= Time.deltaTime;
		}
	}

	protected virtual void OnEnable() {
		this.MMEventStartListening<CorgiEngineEvent>();
	}

	/// <summary>
	/// On disable we stop listening for events
	/// </summary>
	protected virtual void OnDisable() {
		this.MMEventStopListening<CorgiEngineEvent>();
	}
}
