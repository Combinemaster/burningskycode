﻿using MoreMountains.CorgiEngine;
using UnityEngine;
using UnityEngine.UI;

public class DebugManager : MonoBehaviour
{
    public GameObject consoleOutput;
    public GameObject consoleLinePrefab;
    public InputField inputField;
    private bool consoleIsOpen = false;
    public GameObject consoleContainer;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            if (consoleIsOpen)
            {
                consoleIsOpen = false;
                consoleContainer.SetActive(consoleIsOpen);
                inputField.text = "";
            }
            else
            {
                consoleIsOpen = true;
                consoleContainer.SetActive(consoleIsOpen);
            }
        }
        if (consoleIsOpen && inputField.text != "" && Input.GetKey(KeyCode.Return))
        {
            switch (inputField.text)
            {
                case "god":
                    Health localHealthRef = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
                    if (localHealthRef.Invulnerable)
                    {
                        localHealthRef.Invulnerable = false;
                        sendToConsole("God mode Disabled");
                    }
                    else
                    {
                        localHealthRef.Invulnerable = true;
                        sendToConsole("God mode Enabled");
                    }
                    break;
				case "disablemusic":
					SoundManager localGameManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SoundManager>();
					if (localGameManagerRef.Settings.MusicOn) {
						localGameManagerRef.MusicOff();
						sendToConsole("music off");
					} else {
						localGameManagerRef.MusicOn();
						sendToConsole("music on");
					}
					break;
                default:
                    sendToConsole(inputField.text);
                    break;
            }
        }
    }
    private void sendToConsole(string message)
    {
        GameObject newLine = Instantiate(consoleLinePrefab, consoleOutput.transform);
        newLine.GetComponent<Text>().text = message;
        inputField.text = "";
    }
}